function [DI0j,exp_dt] = compute_delay_demo(Info,xr,yr,zr,Nacq,tx_r)

f0=Info.f0;
fs=Info.fs;
c=Info.c;

xt=Info.receivers.pos_mm.x;
yt=Info.receivers.pos_mm.y;
zt=Info.receivers.pos_mm.z;

Nel=length(tx_r);
[XXr,YYr,ZZr] = ndgrid(xr,yr,zr);
ddr = zeros(size(XXr,1),size(XXr,2),size(XXr,3),Nel);   

for i = 1:Nel  
    dr = (sqrt((XXr-xt(tx_r(i))).^2+(YYr-yt(tx_r(i))).^2+(ZZr-zt(tx_r(i))).^2));
    ddr(:,:,:,i)=dr;
end      


d=ddr; % distances bewteen each transducer and each pixel

clear ddr ZZr zt zr YYr XXr dr dc

tt=Info.acq_time;
DI=d/c*fs -tt(1)*fs+1; %convert to indice
clear d
DI( DI > Nacq-1 ) = Nacq-1;
DI( DI <= 0 ) = 1;
DI0j = round(DI(:,:,:,:));DI0j(DI0j==0)=1; %integer part
dt = (DI(:,:,:,:)-DI0j)/fs; 
clear DI
exp_dt=exp(1i*2*pi*f0.*dt); %interpolation of the non integer part