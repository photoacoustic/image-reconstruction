clear all
mexcuda BF_main_cGPU_nimg.cu % to do once 
InfoPA.fs=31.25; % sampling frequency
InfoPA.f0=8; % central frequency
InfoPA.t0=0.55; % delay between laser shoot and start acquisition
InfoPA.c = 1.5; % speed of sound
InfoPA.Zimg = Info.Zimg; % start depth for acquisition
Nacq_PA = Info.PAnumSamples;

x= -4:.09:4; Nx = length(x);InfoPA.X = x;InfoUS.X = x; % grid     
y=-4:.09:4; Ny = length(y); InfoPA.Y = y;InfoUS.Y = y;
z=35:.09:40; Nz = length(z); InfoPA.Z = z;InfoUS.Z = z;

InfoPA.receivers.pos_mm.x=Info.Trans.ElementPos(:,1); %transducers positions
InfoPA.receivers.pos_mm.y=Info.Trans.ElementPos(:,2);
InfoPA.receivers.pos_mm.z=Info.Trans.ElementPos(:,3);  
Nel = length(InfoPA.receivers.pos_mm.z);
% time vector
InfoPA.acq_time =(0:((Nacq_PA)-1))/InfoPA.fs + InfoPA.Zimg(1)/1.5-InfoPA.t0;


% compute lut and convert to GPU
[DI0j,exp] =compute_delay_demo(InfoPA,x,y,z,Nacq_PA,1:Nel); 

Np = length(x)*length(y)*length(z);

DI0j_t = reshape(DI0j,[Np,Nel]);
exp_t = reshape(exp,[Np,Nel]);



RFPA_bf = hilbert(RFPA(:,:,1));
tic
RFPA_GPU=gpuArray(RFPA_bf);
DI0j_GPU=gpuArray(DI0j_t);
exp_GPU=gpuArray(exp_t);
[img_real,img_imag] = BF_main_cGPU_nimg(RFPA_GPU,DI0j_GPU,exp_GPU);
img_real=gather(img_real);
img_imag=gather(img_imag);
ImgPA=complex(img_real,img_imag);
toc
ImgPA = abs(reshape(ImgPA,[Nx Ny Nz]));

%%
figure(11);
subplot(311);
imagesc(y,z,squeeze(max(ImgPA,[],1))');axis image;colormap('hot')
subplot(312);
imagesc(x,z,squeeze(max(ImgPA,[],2))');axis image;colormap('hot')
subplot(313);
imagesc(x,y,squeeze(max(ImgPA,[],3)));axis image;colormap('hot')
%%
addpath(genpath('C:\Users\Guillaume\Desktop\matlab_code'))
InfoPA.mode = 'PA'

tic
ImgPA = BF_classique(RFPA_bf,DI0j_t,exp_t,InfoPA);
toc
ImgPA = abs(reshape(ImgPA,[Nx Ny Nz]));