%% Beamforming matlab %%
%RF: one acquisition time x nb élement
%integer part of delay, Nb pixel x nb élement
%float part of delay, complex because of the interpolation, Nb pixel x nb élement
function img = BF_classique(RF,DI0j,exp,Info)   
    Nz = length(Info.Z);
    Nx = length(Info.X);
    Ny = length(Info.Y);
    img = zeros(Nz*Nx*Ny,1);
    for j = 1:length(Info.receivers.pos_mm.x)
%         disp(j)
        A0 = RF(:,j);                     
        img=img + A0(DI0j(:,j),:).*exp(:,j); % 
    end
end