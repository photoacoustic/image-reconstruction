                                     #include "mex.h"
#include "gpu/mxGPUArray.h"

/*
 * Device code: perform the beamforming
 */
void __global__ computImg(double const * const rf_real,
                         double const * const rf_imag,
                         double const * const di_round,                        
                         double const * const dt_floor_real,
                         double const * const dt_floor_imag,
                         double * const img_real, 
                         double * const img_imag,
                         int const np,
                         int const nacq,
                         double const N,
                         int nel)
{

    int el;
    int indice;
    int pix;
    int npix = 0;
    int const i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < N) {
        
                npix = i - i/(np*nel) * (np*nel);
                el = (i) / np ;
                pix = i - (np * el) + np * (el /(nel));
                indice=(int)di_round[npix]+nacq*el;
                /*value_real=rf_real[indice-1];
                value_imag=rf_imag[indice-1];
                approx_real=dt_floor_real[i];
                approx_imag=dt_floor_imag[i];*/
                
                img_real[pix]=img_real[pix]+(rf_real[indice-1]*dt_floor_real[npix]-rf_imag[indice-1]*dt_floor_imag[npix]);// exp(2*pi*f0*dt_floor[cc])
                img_imag[pix]=img_imag[pix]+(rf_real[indice-1]*dt_floor_imag[npix]+rf_imag[indice-1]*dt_floor_real[npix]); 
        
    }

}

/*
 * Host code: define variable 
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, mxArray const *prhs[])
{
    /* Declare all variables.*/
    mxGPUArray const *rf;
    mxGPUArray const *rf_real;
    double const *d_rf_real;
    mxGPUArray const *rf_imag;
    double const *d_rf_imag; 

    mxGPUArray const *di_round;
    double const *d_di_round;

    mxGPUArray const *dt_floor;
    mxGPUArray const *dt_floor_real;
    double const *d_dt_floor_real;
    mxGPUArray const *dt_floor_imag;
    double const *d_dt_floor_imag;
    
    mxGPUArray *img_real;
    double *d_img_real;
    mxGPUArray *img_imag;
    double *d_img_imag; 

    int nel;
    double N;
    int nimg;
    mwSize npimg;
    mwSize np;
    mwSize nacq;
    
   
    char const * const errId = "parallel:gpu:mexGPUExample:InvalidInput";
    char const * const errMsg = "Invalid input to MEX file.";

    /* Choose a reasonably sized number of threads for the block. */
    int const threadsPerBlock = 1024;
    int blocksPerGrid;

    /* Initialize the MathWorks GPU API. */
    mxInitGPU();

    if ((nrhs!=3) || !(mxIsGPUArray(prhs[0]))) {
        mexErrMsgIdAndTxt(errId, errMsg);
    }
    /* Initialize arrays and pointers from input */
    rf = mxGPUCreateFromMxArray(prhs[0]);  
    rf_real = mxGPUCopyReal(rf);
    d_rf_real = (double const *)(mxGPUGetDataReadOnly(rf_real));

    rf_imag = mxGPUCopyImag(rf);
    d_rf_imag = (double const *)(mxGPUGetDataReadOnly(rf_imag));

    di_round = mxGPUCreateFromMxArray(prhs[1]);
    d_di_round = (double const *)(mxGPUGetDataReadOnly(di_round));

    dt_floor = mxGPUCreateFromMxArray(prhs[2]);
    dt_floor_real = mxGPUCopyReal(dt_floor);
    d_dt_floor_real = (double const *)(mxGPUGetDataReadOnly(dt_floor_real));
 
    dt_floor_imag = mxGPUCopyImag(dt_floor);
    d_dt_floor_imag = (double const *)(mxGPUGetDataReadOnly(dt_floor_imag));

    /* Some variables */
    np=*mxGPUGetDimensions(dt_floor_real);
    nacq=*mxGPUGetDimensions(rf) ;
    nel = int(mxGPUGetNumberOfElements(dt_floor_real))/np;
    nimg = int(mxGPUGetNumberOfElements(rf))/(nacq*nel);
    npimg = int(np) * nimg;
    /* Initialize outputs */
    img_real = mxGPUCreateGPUArray(1,
                        &npimg,
                        mxDOUBLE_CLASS,
                        mxREAL,
                        MX_GPU_INITIALIZE_VALUES);
    d_img_real = (double *)(mxGPUGetData(img_real));
    img_imag = mxGPUCreateGPUArray(1,
                        &npimg,
                        mxDOUBLE_CLASS,
                        mxREAL,
                        MX_GPU_INITIALIZE_VALUES);
    d_img_imag = (double *)(mxGPUGetData(img_imag));
    /*
     * Call the kernel using the CUDA runtime API. We are using a 1-d grid here,
     * and it would be possible for the number of elements to be too large for
     * the grid. For this example we are not guarding against this possibility.
     */
    N = (np)*(nel)*(nimg);
    blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;
    computImg<<<blocksPerGrid, threadsPerBlock>>>(d_rf_real, d_rf_imag,
                                                  d_di_round,
                                                  d_dt_floor_real,d_dt_floor_imag,
                                                  d_img_real,d_img_imag, 
                                                  (int)(np),(int)(nacq), N, nel);

    /* Wrap the result up as a MATLAB gpuArray for return. */
    plhs[0] = mxGPUCreateMxArrayOnGPU(img_real);
    plhs[1] = mxGPUCreateMxArrayOnGPU(img_imag);


    /*
     * The mxGPUArray pointers are host-side structures that refer to device
     * data. These must be destroyed before leaving the MEX function.
       
     */
    
    mxGPUDestroyGPUArray(rf);
    mxGPUDestroyGPUArray(rf_real);
    mxGPUDestroyGPUArray(rf_imag);
    mxGPUDestroyGPUArray(di_round);
    mxGPUDestroyGPUArray(dt_floor);
    mxGPUDestroyGPUArray(dt_floor_real);
    mxGPUDestroyGPUArray(dt_floor_imag);  
    mxGPUDestroyGPUArray(img_real);
    mxGPUDestroyGPUArray(img_imag);
    
}