# Image Reconstruction
This repository contains image reconstruction codes from photoacoustic datasets.

## Bossy's data structure

Load your RF data and create beamforming structure: 

```matlab
beamforming.receivers=receivers;
beamforming.mode='photoacoustic';
beamforming.c=c; % mm/us
beamforming.sampling_freq_MHz=fs;
beamforming.t0_pulse_pt=t0_pulse*fs;
beamforming.acquisition_delay_pt=t0_acq*fs;
beamforming.acquisition_duration_pt=size(RF,1);
beamforming.number_of_elements=size(RF,2);
beamforming.x_pixel_mm
beamforming.y_pixel_mm
beamforming.z_pixel_mm
```

- `receivers`: 
  - `receivers.pos_mm.x` is an array containing x coordinates of the detector elements (same for y and z)  
  - `receivers.N` is the number of detector elements
  - `receivers.elevation_focus_mm` is more or less in our case the expected center of the field of view.
- `mode`: photoacoustic for our purpose (as opposed to pure ultrasound imaging)
- `c`: speed of sound in mm/µs
- `sampling_freq_MHz` 
-  `t0_pulse_pt`: time at which nanosecond pulse was shot, in samples
- `acquisition_delay_pt`: time at which the acquisition starts, in samples
- `acquisition_duration_pt`: duration of the acquisition, in samples
- `number_of_elements`: number of detector elements (virtual in our case)  in ONE direction (nx~=ny not supported yet)
- `beamforming.x_pixel_mm`: x coordinates of reconstruction grid voxels (same for y and z)

## Create LUT

## Beamform image



