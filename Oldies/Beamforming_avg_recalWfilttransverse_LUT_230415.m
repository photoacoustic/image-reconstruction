clear all
% close all
%% Load data
load('Ph3last_rot_2015421.mat')

% load('C:\Users\Thomas\Desktop\Th�se\Manips\Mars 2015\Randomw50_100um_rot_2015327.mat')
% load('C:\Users\Thomas\Desktop\Th�se\Manips\Copie manip leaf aout 2014\100um_phantomleaf_rotating_2bis_140827.mat')
f_oscillo = 1e9;

%% load look up table si d�j� construite (� refaire si on change les params)
% load('LUT_hamming.mat','LUT_hamming','beamforming')

%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg); %nb de points par pulse/image
trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);
figure; plot(ttl_sig(:,1,1)); hold all; plot(trig_out_aix(:,1,1))

Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));
test_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));

for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
        I=find(AAlign==max(AAlign));
        Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
        
        % si pas de trig out aix, Dec_trig=0
        if max(trig_out_aix(:,k,kk))<0.5*max(trig_out_aix(:))
          Dec_trig(k,kk)=0;  
          test_trig(k,kk)=1;
        end
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:,end));

figure, subplot(121), plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix((Dec_trig(1,1)+1):end,1,1))
subplot(122), imagesc(Dec_trig), colorbar
%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));
PD_sig =reshape(PD_sig, n, n_avg,n_it);
 
center=1635;
% center=1020;%_leaf_aout
for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        nrj_pulse(k,kk)=sum(PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));
        sig_photo(:,k,kk)=PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)); 
    end
end

figure; imagesc(sig_photo(:,:,1));
% return
%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for kk=1:size(Align,3)
    for k=1:size(Align,2)
        AAlign=xcorr(Align(:,k,kk),ref,'coeff');
        I=find(AAlign==max(AAlign));
        Dec_pulse(k,kk)=I-size(Align,1);
    end
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo

%%
pitch=0.33*1e-3;
PA_sig1=PA_sig(151:(end-350),:,:,:); %on coupe le d�but et la fin des signaux car oscillations+saturation d�gueu (origine?)
clear PA_sig

%%
image_width = 20e-3;
offset_width=0e-3;
image_depth =20e-3;
offset_depth = 15e-3;
interpfact=1;
c= 1475;
t0=0;
fs=60e6*interpfact;
BF_check=0; %0->J�r�me style, 1->Manu style

% n�cessaire pour Jerome
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/fs/interpfact));% number of pixel on the other side
n=m;

Bp_image=zeros(m, n,size(PA_sig1,4),'single');
Dec2=((Dec_trig)*fs/f_oscillo); %convertit le temps oscillo en temps aixplorer

for ik= 34%:size(PA_sig1,4)
    
    %% interpolation (jamais utilis�e en fait)
    if interpfact>1
        PA_sig0=zeros([size(PA_sig1,1)*interpfact,size(PA_sig1,2),size(PA_sig1,3)],'single');
        for i=1:size(PA_sig1,2)
            for j=1:size(PA_sig1,3)
                PA_sig0(:,i,j)= single(interp(double(squeeze(PA_sig1(:,i,j,ik))),interpfact));
            end
        end        
    else
        % d�finition signaux � utiliser pour l'image 
        PA_sig0=single(squeeze(PA_sig1(:,:,:,ik)));
    end
    
  %% filtrage passe-haut des signaux dans l'espace des voies pour supprimer les pics de bruit et affiner le recalage des signaux entre eux
    seuil=0.05;
    [b a] =butter(3,seuil,'high');
    x0=double(PA_sig0);
%     figure, subplot(221), imagesc(x0(:,:,1))
%     subplot(223), plot(x0(:,64,1))
    
    for k=1:size(PA_sig1,3)
        x0(:,:,k)= (single(filtfilt(b,a,x0(:,:,k)')))';
                x0= (single(filtfilt(b,a,x0')))';

    end
    
%     subplot(222), imagesc(x0(:,:,1))
%     subplot(224), plot(x0(:,64,1))
%   return

    %% Normalisation + Recalage des signaux (pour le delay entre ttl et trig_aix) + Filtrage passe-bas (standard) des signaux 
    
    [b a] =butter(3,[1e6 8e6]/(fs/2));
    for k=1:size(PA_sig1 ,3);
%         PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,Dec2(k,ik)+150*interpfact); %les 150 correspondent aux points supprim�s plus haut
                PA_sig0 = DelaySignal(PA_sig0,1,150*interpfact); %les 150 correspondent aux points supprim�s plus haut
%         PA_sig0 = single(filtfilt(b,a,double(PA_sig0 )));

        PA_sig0 (:,:,k)= single(filtfilt(b,a,double(PA_sig0 (:,:,k))))./nrj_pulse(k,ik);
        % on filtre et recale aussi x0 car on doit l'utiliser plus loin
        x0 (:,:,k) = DelaySignal(x0(:,:,k),1,Dec2(k,ik)+150*interpfact); 
        x0 (:,:,k)= single(filtfilt(b,a,double(x0 (:,:,k))))./nrj_pulse(k,ik);
%     end
%     PA_sig0=PA_sig0(:,:,3:end,:);
%     x0=x0(:,:,3:end,:);
return
    
    %% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
    % (Dec_pulse n'est pas utilis� finalement...?)
    
    
    if ik==1 %ok pour psf, mais �trange pour rot, on pourrait avoir des variations dues au speckle ...?
        % --> du coup pas besoin de tout filtrer pour obtenir le bon x0 plus haut
        Refx=mean(double(squeeze(x0((500*interpfact):(1000*interpfact),32:3:96,:))),3); %attention � bien tout prendre, plot pour checker si besoin
    end
    
       % calcul decalage sur signal filtr� dans l'espace des voies
    for i=1:size(PA_sig0,3)
        cc = xcorr2(double(squeeze(x0((500*interpfact):(1000*interpfact),32:3:96,i))),Refx);
        cc2=interp(cc(:),10);
        [max_cc, imax] = max(abs(cc2));
        [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
        corr_offset(i,:) = [ (ypeak-size(Refx,1)*10 +9) (xpeak-size(Refx,2)) ];
    end
    Dec=corr_offset(:,1);
    if ik==1
        figure, plot(Dec)
    end
    
    for k=1:size(PA_sig0 ,3);
        PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,t0-Dec(k)/10);
    end
    return
    %%
    PA_sig0=squeeze(mean(PA_sig0,3));
%     PA_sig0=squeeze(PA_sig0(:,:,1));

    NbElemts=size(PA_sig0,2);
    ik
    %% Beamforming J�r�me's style
    if BF_check==0
    x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
    y_array = - offset_depth*ones(1,NbElemts);
    sizeT = size(PA_sig0,1)-1;
    [N, M] = meshgrid( 1:n, 1:m );
    xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
    yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
    clear N M SS
    
    for i = 1:length(x_array)
        x_sensor = x_array(i) ;
        y_sensor = y_array(i) ;
        x = x_sensor - xr;
        y = y_sensor - yr;
        s = sqrt( x.^2 + y.^2 );
        ss = round( s*fs/c +  1 );
        ss( ss > sizeT ) = sizeT;
        ss( ss <= 0 ) = 1;
        SS(:,:,i)=ss;
    end
    
    sigMat=squeeze(double(PA_sig0)/4);
    bp_image = zeros(m, n);
    tic
    for i = 1:length(x_array)
        A0 = -diff(sigMat(:,i));
        s1 = A0(squeeze(SS(:,:,i)));
        bp_image = bp_image + s1;
    end 
    Bp_image(:,:,ik) =single(bp_image);
    end
    
    %%
    if BF_check==1
        %               BuildLUT_exp
        %               return
        if ik==1
            load('LUT_hamming_10mupix.mat')
        end
        
       temp=diff(PA_sig0,1,1);
        temp=-[temp;zeros(1,size(temp,2))];
        
        image_bf=BF_compute_beamformed_data_exp(temp,LUT_hamming);
        
        
        if ik==1
            Bp_image=zeros(size(image_bf,1),size(image_bf,2),size(PA_sig1,4),'single');
    end
    Bp_image(:,:,ik) =single(image_bf);
     end
end
end
 return
% save ('C:\Users\Thomas\Desktop\Th�se\Manips\Mars 2015\BF_Randomw50_100um_rot_2015327_delay&sum.mat','nrj_pulse','Bp_image','BF_check','-v7.3')

%%

h=1:10;
Image_Mean=mean(Bp_image(:,:,h),3);
figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean);
% figure; imagesc(Image_Mean);
colormap jet
colorbar

% figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,abs(Image_Mean))
% figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,abs(Image_Mean))
% 
% axis equal tight
% colormap gray
% title('mean')
% colorbar

Image_Var=var(Bp_image(:,:,h),[],3);

axis equal tight
figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,sqrt(Image_Var))
% figure; imagesc(sqrt(Image_Var))

colormap jet
title('std')
colorbar

