clear all
close all

addpath(genpath('..\Data_PA\'))
%%
load('Ph9last_rot_2015429.mat')
f_oscillo = 1e9;
pitch=0.33*1e-3;
NbElemts=size(PA_sig,2);

%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement
% de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg);
trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);
figure(1),plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix(:,1,1))
title('shift between trig out laser and trig out ultrasound machine')
Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));

for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
        I=find(AAlign==max(AAlign));
        Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:)); % a checker

figure(2), subplot(121), plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix((Dec_trig(1)+1):end,1,1))
subplot(122), imagesc(Dec_trig),

%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));
PD_sig =reshape(PD_sig, n, n_avg,n_it);

center=1637;
for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        nrj_pulse(k,kk)=sum(PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));
        sig_photo(:,k,kk)=PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk));
    end
end

figure(3); imagesc(sig_photo(:,:,1));

%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for kk=1:size(Align,3)
    for k=1:size(Align,2)
        AAlign=xcorr(Align(:,k,kk),ref,'coeff');
        I=find(AAlign==max(AAlign));
        Dec_pulse(k,kk)=I-size(Align,1);
    end
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo

%% cut beginning and end of signal because saturated signals because of the machine
PA_sig1=PA_sig(151:(end-350),:,:,:);
% clear PA_sig


%%
c= 1475;
image_width = 20e-3;%NbElemts*pitch;
offset_width=0e-3;
image_depth =20e-3;
offset_depth = 15e-3;
interpfact=1;
t0=0; % not used here, a voir
fs=60e6*interpfact;
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/(fs/interpfact)));% number of pixel on the other side
n=m;
Bp_image=zeros(m, n,size(PA_sig1,4),'single');
Dec2=((Dec_trig)*fs/f_oscillo);

%% signals processing and image reconstruction
for ik= 1:size(PA_sig1,4) % nb of uncorrelated speckle illuminations
    
    disp(['processing image reconstruction on ' num2str(ik) 'th speckle realisation'])
    
    if interpfact>1 % never used so far
        PA_sig0=zeros([size(PA_sig1,1)*interpfact,size(PA_sig1,2),size(PA_sig1,3)],'single');
        for i=1:size(PA_sig1,2)
            for j=1:size(PA_sig1,3)
                PA_sig0(:,i,j)= single(interp(double(squeeze(PA_sig1(:,i,j,ik))),interpfact));
            end
        end
    else
        PA_sig0=single(squeeze(PA_sig1(:,:,:,ik)));
    end

    %%  Normalisation + Recalage des signaux (pour le delay Dec2 entre ttl et trig_aix)
    % + Filtrage passe-bas (standard) des signaux
    [b a] =butter(3,[1e6 6e6]/(fs/2));
    for k=1:size(PA_sig0 ,3);
        PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,Dec2(k,ik)+150*interpfact);
        PA_sig0 (:,:,k)= single(filtfilt(b,a,double(PA_sig0 (:,:,k))))./nrj_pulse(k,ik);
        % check for bursts degueulasses
        %bd(k,ik)=max(PA_sig0(:,64,k));
    end
    PA_sig0=PA_sig0(:,:,2:end,:); % why? --> car sinon burst degueu on first signals (voir bd)
    %figure; imagesc(squeeze(PA_sig0(:,64,:,1)))
    %figure(8), imagesc(bd(1:end,:))
    

    %% on coupe eventuellement sur la voie 86 car un peu degueu
    figure(4), subplot(121), imagesc(PA_sig0(:,:,1))
    PA_sig00=PA_sig0;
    for k=1:size(PA_sig0 ,3);
        for kk=1:size(PA_sig0 ,4);
            PA_sig00(150:550,86,k,kk)=mean(PA_sig0(150:550,[85 87],k,kk),2);
        end
    end
    PA_sig0=PA_sig00;
    clear PA_sig00
    subplot(122), imagesc(PA_sig0(:,:,1))

    %% methode manu: on enl�ve la moyenne des signaux pour couper les pics
    % � bricoler si beaucoup de signaux quasi "horizontaux"??
    figure(5), subplot(121), imagesc(PA_sig0(:,:,1))
    for k=1:size(PA_sig0,3)
        for j=1:size(PA_sig0,2)
            PA_sig2(:,j,k)= PA_sig0(:,j,k)-squeeze(mean(PA_sig0(:,[1:42,87:128],k),2)); % a checker
        end
    end
    subplot(122), imagesc(PA_sig2(:,:,1))
    
    %% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
    % (Dec_pulse n'est pas utilis� finalement...?)
    if ik==1
        Ref=mean(double(squeeze(PA_sig2((500*interpfact):(900*interpfact),32:3:96,:))),3);
    end
    for i=1:size(PA_sig2,3)
        cc = xcorr2(double(squeeze(PA_sig2((500*interpfact):(900*interpfact),32:3:96,i))),Ref);
        cc2=interp(cc(:),10);
        [max_cc, imax] = max(abs(cc2));
        [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
        corr_offset(i,:) = [ (ypeak-size(Ref,1)*10 +9) (xpeak-size(Ref,2)) ];
    end
    Dec=corr_offset(:,1);
    if ik==1
        figure(6); plot(Dec), title('jitter calculated by sigs cross corr, in tenths of pixel')
    end
    for k=1:size(PA_sig2 ,3);
        PA_sig2 (:,:,k) = DelaySignal(PA_sig2(:,:,k),1,-Dec(k)/10);
    end
    
    %% average 25 (24) signals with same speckle illumination
    PA_sig2=squeeze(mean(PA_sig2,3));
% PA_sig_cleaned(:,:,ik)=PA_sig2;
%     end
% PA_sig_cleaned=mean(PA_sig_cleaned,3);
% save ('data_sample3_optica\Ph3last_rot_2015421_6MHz_cleanedAndAvg.mat','PA_sig_cleaned')
% return

    %% beamforming
    if ik==1
        x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
        y_array = - offset_depth*ones(1,NbElemts);
        sizeT = size(PA_sig0,1)-1;
        [N, M] = meshgrid( 1:n, 1:m );
        xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
        yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
        clear N M SS
        
        for i = 1:length(x_array)
            x_sensor = x_array(i) ;
            y_sensor = y_array(i) ;
            x = x_sensor - xr;
            y = y_sensor - yr;
            s = sqrt( x.^2 + y.^2 );
            ss = round( s*fs/c +  1 );
            ss( ss > sizeT ) = sizeT;
            ss( ss <= 0 ) = 1;
            SS(:,:,i)=ss;
        end
    end
   
    sigMat=squeeze(double(PA_sig2)/4);
    bp_image= zeros(m, n);
    
    for i =1:length(x_array)
        A0 = -diff(sigMat(:,i));
        s1 = A0(squeeze(SS(:,:,i))); %create a matrix of size(SS), and s1(i,j)=A0(SS(i,j))
        bp_image= bp_image+s1;
    end 
    
    Bp_image(:,:,ik) =single(bp_image);
    figure(7), imagesc(bp_image), axis xy image
end

save ('data_sample3_optica\BF_Ph9last_rot_2015429_dns_6MHz.mat','nrj_pulse','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')
return

%% check images, then mean and variance images
h=1:100;

Image_Mean=mean(Bp_image(:,:,h),3);
Image_Var=var(Bp_image(:,:,h),[],3);
figure(9), 
subplot(121),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean),
colorbar, axis image,title('mean')
subplot(122), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var),
axis image,title('std'),colorbar
colormap jet

%%
% Bp_image_new=Bp_image;
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum.mat','Bp_image')
% Bp_image_old=Bp_image;

for k=h
    figure(666),
    subplot(221),imagesc(Bp_image_old(300:500,300:500,k)), axis xy image, title(['old ' num2str(k)]), colorbar
    subplot(222),imagesc(Bp_image_new(300:500,300:500,k)), axis xy image, title(['new ' num2str(k)]), colorbar
    if k>1
    subplot(223),imagesc(Bp_image_old(300:500,300:500,k)-Bp_image_old(300:500,300:500,k-1)), 
    axis xy image, title('old(k)-old(k-1)'), colorbar
    end
    subplot(224),imagesc(Bp_image_new(300:500,300:500,k)-Bp_image_old(300:500,300:500,k)), 
    axis xy image, title('new(k)-old(k)'), colorbar
%     coef(k)=mean2(Bp_image_new(100:300,100:300,k)-Bp_image_old(100:300,100:300,k));
    pause%(0.5)
end
figure(12000)
plot(coef)
%%
% Bp_image=Bp_image_new;

% Image_Mean_old=mean(Bp_image_old(:,:,h),3);
% Image_Var_old=var(Bp_image_old(:,:,h),[],3);
% Image_Mean_new=mean(Bp_image_new(:,:,h),3);
% Image_Var_new=var(Bp_image_new(:,:,h),[],3);

figure(9), 
subplot(211),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean_old),
colorbar, axis image,title('old mean')
subplot(212), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var_old),
axis image,title('old std'),colorbar
colormap jet

figure(10), 
subplot(211),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean_new),
colorbar, axis image,title('new mean')
subplot(212), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var_new),
axis image,title('new std'),colorbar
colormap jet