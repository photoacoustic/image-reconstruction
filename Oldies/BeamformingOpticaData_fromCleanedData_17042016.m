clear all
close all

addpath(genpath('..\Data_PA\'))
load('Ph3last_rot_2015421_6MHz_cleanedAndAvg.mat')
fs_downsamp=fs/3;
PA_sig_downsamp=PA_sig_cleaned(1:3:end,:,:);
%% image reconstruction


for ik= 1:size(PA_sig_cleaned,3) % nb of uncorrelated speckle illuminations
    
    disp(['processing image reconstruction on ' num2str(ik) 'th speckle realisation'])

    if ik==1
        x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
        y_array = - offset_depth*ones(1,NbElemts);
        sizeT = size(PA_sig_downsamp,1)-1;
        [N, M] = meshgrid( 1:n, 1:m );
        xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
        yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
        clear N M SS
        
        for i = 1:length(x_array)
            x_sensor = x_array(i) ;
            y_sensor = y_array(i) ;
            x = x_sensor - xr;
            y = y_sensor - yr;
            s = sqrt( x.^2 + y.^2 );
            ss = round( s*fs_downsamp/c +  1 );
            ss( ss > sizeT ) = sizeT;
            ss( ss <= 0 ) = 1;
            SS(:,:,i)=ss;
        end
    end
   
    sigMat=squeeze(double(PA_sig_downsamp(:,:,ik))/4);
    bp_image= zeros(m, n);
    
    for i =1:2:length(x_array)
        
        A0 = -diff(sigMat(:,i));
        s1 = A0(squeeze(SS(:,:,i))); %create a matrix of size(SS), and s1(i,j)=A0(SS(i,j))
        bp_image= bp_image+s1;
    end 
    
    Bp_image(:,:,ik) =single(bp_image);
    figure(7), imagesc(bp_image), axis xy image
end

% save ('data_sample2_optica\newBFdata\BF_Ph3last_rot_2015421_6MHz_fromCleanedData_64Elts.mat','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')
return

%% check images, then mean and variance images
h=1:100;

Image_Mean=mean(Bp_image(:,:,h),3);
Image_Var=var(Bp_image(:,:,h),[],3);
figure(9), 
subplot(121),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean),
colorbar, axis image,title('mean')
subplot(122), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var),
axis image,title('var'),colorbar
colormap jet

%%
% Bp_image_new=Bp_image;
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum.mat','Bp_image')
% Bp_image_old=Bp_image;

for k=h
    figure(666),
    subplot(221),imagesc(Bp_image_old(300:500,300:500,k)), axis xy image, title(['old ' num2str(k)]), colorbar
    subplot(222),imagesc(Bp_image_new(300:500,300:500,k)), axis xy image, title(['new ' num2str(k)]), colorbar
    if k>1
    subplot(223),imagesc(Bp_image_old(300:500,300:500,k)-Bp_image_old(300:500,300:500,k-1)), 
    axis xy image, title('old(k)-old(k-1)'), colorbar
    end
    subplot(224),imagesc(Bp_image_new(300:500,300:500,k)-Bp_image_old(300:500,300:500,k)), 
    axis xy image, title('new(k)-old(k)'), colorbar
%     coef(k)=mean2(Bp_image_new(100:300,100:300,k)-Bp_image_old(100:300,100:300,k));
    pause%(0.5)
end
figure(12000)
plot(coef)
%%
% Bp_image=Bp_image_new;

% Image_Mean_old=mean(Bp_image_old(:,:,h),3);
% Image_Var_old=var(Bp_image_old(:,:,h),[],3);
% Image_Mean_new=mean(Bp_image_new(:,:,h),3);
% Image_Var_new=var(Bp_image_new(:,:,h),[],3);

figure(9), 
subplot(211),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean_old),
colorbar, axis image,title('old mean')
subplot(212), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var_old),
axis image,title('old std'),colorbar
colormap jet

figure(10), 
subplot(211),imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean_new),
colorbar, axis image,title('new mean')
subplot(212), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Var_new),
axis image,title('new std'),colorbar
colormap jet