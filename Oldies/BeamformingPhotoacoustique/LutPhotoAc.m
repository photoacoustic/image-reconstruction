function lutR=LutPhotoAc(p,x,z)

nx=length(x);
nz=length(z);                  

tic
lutR = cell(nz,nx);
for ix=1:nx
    for iz=1:nz % a priori possibilit� de condenser en 1 ligne les 2 suivantes
        r=retard(p,x(ix),z(iz));      % calculate delays    
        lutR{iz,ix}=r;         % garde dans un cell
    end
end
toc

function r = retard(p,x0,z0)

x = (0:p.Bnx-1)*p.Bdx; % barette �chographique
r = round((sqrt(z0*z0 + (x-x0).*(x-x0))./p.c - p.Rret)*p.Rfech); % retard en nombre de points sur chacun des piezos

ap = abs((x-x0)/z0) < 1/(2*p.DF) ;         % p.DF = F/D. permet de d�terminer les piezos pris en compte (fen�tre rectangulaire ici)
ind = r>1 & r<p.Rnt & ap>0;                % limits (r=1 if data does not exist)
for i=1:p.Bnx                              % position dans le vecteur des donnes "1D"
    r(i) = r(i)+(i-1)*p.Rnt;
end
r = r(ind);

