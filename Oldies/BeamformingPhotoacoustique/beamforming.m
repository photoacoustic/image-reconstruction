close all

RawSignal = data;        % donn�es r�cup�r�es par l'�chographe dont le T0 n'est pas le bon (pas celui du laser)
TLaser = 370;                      % temps en nombre de points du tir du laser (nouveau T0)
[A,B] = size(RawSignal);
RawSignal = RawSignal(TLaser:A,:);

%% structure p des param�tres pour le beamforming

p.c     = 1.5;                     % vitesse du son (mm/�s)
p.Bdx   = 0.7;                     % pitch (mm)
p.Rfech = 36;                      % frequence d'echantillonage (MHz)
p.Rret  = 0;                       % retard correspondant au 1er echantillon (�s)
p.DF    = 1;                       % F number (D/F)
[p.Rnt p.Bnx] = size(RawSignal);   % taille des signaux et nombre de voies

%% caract�ristiques de l'image beamform�e

freq = 4;                          % fr�quence du signal (MHz) ou plus grand pour avoir un pas spatial de reconstruction plus petit
z = 5:p.c/freq:70;                  % z axis for beamforming mm
x = (0:p.Bnx-1)*p.Bdx;             % x axis for beamforming mm
lutR=LutPhotoAc(p,x,z);
bfdata=bf(RawSignal,lutR,x,z);     % beamforming

t = (0:(p.Rnt-1))/p.Rfech;

figure(1);
imagesc(1:128,t,RawSignal);
colormap(gray);

figure(2);
imagesc(x,z,abs(hilbert(bfdata)))
axis equal tight;
xlabel('largeur (en mm)');
ylabel('profonfeur (en mm)');
title('image beamform�e');

% figure(3);
% imagesc(x,z,abs(bfdata))
% axis equal tight;
% xlabel('largeur (en mm)');
% ylabel('profonfeur (en mm)');
% title('image beamform�e (sans transform�e de Hilbert)');