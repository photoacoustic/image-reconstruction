function bfdata = bf(D,lutR,x,z)
tic
nx = length(x);
nz = length(z);
bfdata = zeros(nz,nx);      % init beamforming matrix                  

for iz=1:nz
    for ix=1:nx
        r = lutR{iz,ix};
        bfdata(iz,ix) = sum(D(r));    % delay and sum       
    end
end
toc


