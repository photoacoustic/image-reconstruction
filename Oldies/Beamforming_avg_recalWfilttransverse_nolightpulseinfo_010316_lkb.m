clear all
close all
%% Load data
load('D:\Thomas_Data\Fevrier2016\X_ink2_2016226.mat')

f_oscillo = 1e9;

%%
pitch=0.33*1e-3;
PA_sig=reshape(PA_sig,size(PA_sig,1),size(PA_sig,2),size(PA_sig,3)*size(PA_sig,4));
PA_sigsig=PA_sig(:,:,1:5000);
PA_sig1=PA_sigsig(151:(end-350),:,:); %on coupe le d�but et la fin des signaux car oscillations+saturation d�gueu (origine?)
clear PA_sig

%%
image_width = 20e-3;
offset_width=0e-3;
image_depth =20e-3;
offset_depth = 15e-3;
interpfact=1;
c= 1550;
t0=0;
fs=60e6*interpfact;
BF_check=0; %0->J�r�me style, 1->Manu style

% n�cessaire pour Jerome
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/fs/interpfact));% number of pixel on the other side
n=m;

Bp_image=zeros(m, n,10,'single');
% Dec2=((Dec_trig)*fs/f_oscillo); %convertit le temps oscillo en temps aixplorer

    %% Normalisation + Recalage des signaux (pour le delay entre ttl et trig_aix) + Filtrage passe-bas (standard) des signaux 
%     PA_sig0=x0;
    [b,a] =butter(3,[1e6 8e6]/(fs/2));
    for k=1:size(PA_sig1,3)
        
%         PA_sig00=single(squeeze(PA_sig1(:,:,k)));
        PA_sig1(:,:,k)  = DelaySignal(single(PA_sig1(:,:,k)),1,150*interpfact); %les 150 correspondent aux points supprim�s plus haut
%         PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,Dec2(k,ik)+150*interpfact); %les 150 correspondent aux points supprim�s plus haut
        PA_sig1(:,:,k)= single(filtfilt(b,a,double(PA_sig1(:,:,k))));%./nrj_pulse(k,ik);
        % on filtre et recale aussi x0 car on doit l'utiliser plus loin
%         x0 (:,:,k) = DelaySignal(x0(:,:,k),1,Dec2(k,ik)+150*interpfact); 
%         x0 (:,:,k)= single(filtfilt(b,a,double(x0 (:,:,k))))./nrj_pulse(k,ik);
    end
%     PA_sig0=PA_sig0(:,:,3:end,:);
%     x0=x0(:,:,3:end,:);

  %% filtrage passe-haut des signaux dans l'espace des voies pour supprimer les pics de bruit et affiner le recalage des signaux entre eux
  % sinon essayer d'enlever la moyenne des signaux a chaque signal pour
  % enlever ces m�mes pics
  seuil=0.05;
    [b a] =butter(3,seuil,'high');
    PA_sig1=double(PA_sig1);
%     figure, subplot(221), imagesc(x0(:,:,1))
%     subplot(223), plot(x0(:,64,1))
    for k=1:size(PA_sig1,3)
        PA_sig1(:,:,k)= (single(filtfilt(b,a,PA_sig1(:,:,k)')))';
    end
%     subplot(222), imagesc(x0(:,:,1))
%     subplot(224), plot(x0(:,64,1))
  



    %% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
    % (Dec_pulse n'est pas utilis� finalement...?)
    
    
%     if ik==1 %ok pour psf, mais �trange pour rot, on pourrait avoir des variations dues au speckle ...?
        % --> du coup pas besoin de tout filtrer pour obtenir le bon x0 plus haut
        Refx=mean(double(squeeze(PA_sig1((500*interpfact):(1000*interpfact),32:3:96,1:100))),3); %attention � bien tout prendre, plot pour checker si besoin
%     end
    
       % calcul decalage sur signal filtr� dans l'espace des voies
    for i=1:size(PA_sig1,3)
        cc = xcorr2(double(squeeze(PA_sig1((500*interpfact):(1000*interpfact),32:3:96,i))),Refx);
        cc2=interp(cc(:),10);
        [max_cc, imax] = max(abs(cc2));
        [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
        corr_offset(i,:) = [ (ypeak-size(Refx,1)*10 +9) (xpeak-size(Refx,2)) ];
    end
    Dec=corr_offset(:,1);
%     if ik==1
    Dec(find(abs(Dec)>100))=0;

        figure, plot(Dec)
%     end
    for k=1:size(PA_sig1 ,3);
        PA_sig1 (:,:,k) = DelaySignal(PA_sig1(:,:,k),1,t0-Dec(k)/10);
    end
    
    
    %%   
for ik= 2501:size(PA_sig1,3)
        PA_sig0=single(squeeze(PA_sig1(:,:,ik)));
     NbElemts=size(PA_sig0,2);

%     PA_sig00=squeeze(mean(PA_sig0(:,:,1:10),3));
%     PA_sig00=squeeze(PA_sig0(:,:,1));
% PA_sig0=x0;
    ik
    %% Beamforming J�r�me's style
    if BF_check==0
    x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
    y_array = - offset_depth*ones(1,NbElemts);
    sizeT = size(PA_sig0,1)-1;
    [N, M] = meshgrid( 1:n, 1:m );
    xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
    yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
    clear N M SS
    
    for i = 1:length(x_array)
        x_sensor = x_array(i) ;
        y_sensor = y_array(i) ;
        x = x_sensor - xr;
        y = y_sensor - yr;
        s = sqrt( x.^2 + y.^2 );
        ss = round( s*fs/c +  1 );
        ss( ss > sizeT ) = sizeT;
        ss( ss <= 0 ) = 1;
        SS(:,:,i)=ss;
    end
    
    sigMat=squeeze(double(PA_sig0)/4);
    bp_image = zeros(m, n);
    tic
    for i = 1:length(x_array)
        A0 = -diff(sigMat(:,i));
        s1 = A0(squeeze(SS(:,:,i)));
        bp_image = bp_image + s1;
    end 
    Bp_image(:,:,ik-2500) =single(bp_image);
    end
    
  
figure(1), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3, Bp_image(:,:,ik-2500) ),
end

% return
% save ('D:\Thomas_Data\Fevrier2016\BF_Xink2_2501to5000img.mat','Bp_image','BF_check','-v7.3')

%%
X=301:600;
Y=100:350;
figure(2),subplot(221),imagesc(Bp_image(:,:,1)), axis xy, axis image,title('image 1')
%%
h=1:50;
Mean=mean(Bp_image(Y,X,h),3);
Var=var(Bp_image(Y,X,h),[],3);
Var=Var-mean2(Var(1:50,1:50));
Var=abs(Var);
%%
% figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean);
% load('C:\Users\BoiteTomLKB\Desktop\MeanAndVar_X_ink2_201632.mat')
Y=351:440;
X=351:450;
m=Image_Mean(Y,X);
v=Image_Var(Y,X);
% v=abs(v-mean2(Image_Var(1:100,1:100)));
figure(2), subplot(221); imagesc(m);%190:250,300:600
axis xy,axis image,title('mean')
figure(2), subplot(222); imagesc(abs(m));%190:250,300:600
axis xy,axis image,title('abs(mean)')
subplot(223), imagesc(sqrt(v))
axis image,colormap jet,title('std'),axis xy

xi=[1,100];
yi=[36,58]; %0.2258 slope
c_mean=improfile(abs(m),xi,yi);
c_var=improfile(v,xi,yi);
subplot(224), plot_norm(1:length(c_mean),c_mean,'b'), hold on, plot_norm(1:length(c_mean),c_var,'r'), hold off
subplot(221), hold on, plot(xi,yi,'color','w'), hold off
% subplot(223), hold on, plot(xi,yi,'color','w'), hold off

%%
for k=75%:length(X)
    k
    mean_temp=abs(m);
    mean_temp(:,k)=80;
    subplot(221), imagesc(mean_temp)
    axis image,colormap jet,title('abs(mean)'),axis xy
    
    std_temp=sqrt(v);
    std_temp(:,k)=30;
    subplot(222), imagesc(std_temp)
    axis image,colormap jet,title('std'),axis xy
    
% cum4_temp=abs(cum4);
% cum4_temp(:,k)=9e5;
% cum4_temp(:,120)=30;
% subplot(223), imagesc(cum4_temp)
% axis image,colormap jet,title('4th order cum'),axis xy
%     subplot(2,2,[3,4]), plot_norm(1:length(Y),abs(hilbert(m(:,k))),'b'); hold on,plot_norm(1:length(Y),abs(hilbert(m(:,k))).^2,'k')
%     plot_norm(1:length(Y),abs(m(:,k)),'g');
%     plot_norm(1:length(Y),v(:,k),'r'); hold off
%     legend('mean (envelope)','mean.^2 (envelope)','mean (abs)','sqrt(var)')
subplot(2,2,4), plot_norm(hh,abs(hilbert(m(hh,120))),'b'); hold on,plot_norm(hh,abs(hilbert(m(hh,120))).^2,'k')
plot_norm(hh,abs(m(hh,120)),'g');
plot_norm(hh,sqrt(v(hh,120)),'r');
% plot_norm(hh,cum4(hh,120),'m')
% plot_norm(hh,abs(hilbert(m(hh,120))).^4,'c')
hold off
legend('abs(hilbert(mean))','abs(hilbert(mean)).^2','mean (abs)','sqrt(var)')
axis tight
    pause
    
end
%%
k=[75 88]
mean_temp=abs(m);
mean_temp(:,k)=85;
mean_temp(:,120)=85;
subplot(221), imagesc(mean_temp)
axis image,colormap jet,title('abs(mean)'),axis xy, colorbar

std_temp=sqrt(v);
std_temp(:,k)=30;
std_temp(:,120)=30;
subplot(222), imagesc(std_temp)
axis image,colormap jet,title('standard deviation'),axis xy, colorbar


var_temp=v;
var_temp(:,k)=900;
var_temp(:,120)=900;
subplot(223), imagesc(var_temp,[0 600])
axis image,colormap jet,title('variance'),axis xy, colorbar

cum4_temp=mom4(Y,X);
cum4_temp(:,k)=9e5;
cum4_temp(:,120)=9e5;
subplot(223), imagesc(cum4_temp,[0 7e5])
axis image,colormap jet,title('4th order cum'),axis xy

hh=40:100;
% subplot(2,2,3), plot_norm(hh,abs(hilbert(m(hh,k))),'b'); hold on,plot_norm(hh,abs(hilbert(m(hh,k))).^2,'k')
% plot_norm(hh,abs(m(hh,k)),'g');
% plot_norm(hh,v(hh,k),'r'); hold off
% legend('mean (envelope)','mean.^2 (envelope)','mean (abs)','sqrt(var)','location','northwest')
% axis tight

subplot(2,2,4), plot_norm(hh,abs(hilbert(m(hh,120))),'b'); hold on,plot_norm(hh,abs(hilbert(m(hh,120))).^2,'k')
plot_norm(hh,abs(m(hh,120)),'g');
plot_norm(hh,v(hh,120),'r'); hold off
legend('abs(hilbert(mean))','abs(hilbert(mean)).^2','abs(mean)','variance')
axis tight


subplot(224), imagesc(mean_temp.^4,[0 2e6])
axis image,colormap jet,title('abs(mean).^2'),axis xy, colorbar
%% 
cum4=mom4(Y,X)-3*v.^2;



