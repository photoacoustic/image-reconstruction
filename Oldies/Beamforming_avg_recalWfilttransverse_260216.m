clear all
close all
%% Load data
% load('D:\Thomas_Data\Mars2016\2pts_ink_fluctest3_2016331.mat')
load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\Ph3last_rot_2015421.mat')

%%
f_oscillo = 1e9;
pitch=0.33*1e-3;
m=mean(abs(PA_sig(:,64,:)),1);

min_ind=1;
max_ind=100;%find(m==0,1)-1;
figure(1)%,subplot(211)
plot(squeeze(m(min_ind:max_ind)))
% mm=max(sig_photo,[],1);
% subplot(212)
% plot(mm)

%%
PA_sig=PA_sig(:,:,min_ind:max_ind);
PA_sig=single(PA_sig(151:(end-350),:,:)); %on coupe le d�but et la fin des signaux car oscillations+saturation d�gueu (origine?)
% clear PA_sig

%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg); %nb de points par pulse/image
trig_out_aix =reshape(trig_out_aix, n, n_avg*n_it);
trig_out_aix=trig_out_aix(:,min_ind:max_ind);
ttl_sig =reshape(ttl_sig, n, n_avg*n_it);
ttl_sig=ttl_sig(:,min_ind:max_ind);

figure; plot(ttl_sig(:,1)); hold all; plot(trig_out_aix(:,1))

Dec_trig=zeros(size(ttl_sig,2),1);
test_trig=zeros(size(ttl_sig,2),1);

for k=1:size(ttl_sig,2)
    AAlign=xcorr(trig_out_aix(:,k),ttl_sig(:,k),'coeff');
    I=find(AAlign==max(AAlign));
    Dec_trig(k)=min(I-size(trig_out_aix,1));
    % si pas de trig out aix, Dec_trig=0
    if max(trig_out_aix(:,k))<0.5*max(trig_out_aix(:))
        Dec_trig(k)=0;
        test_trig(k)=1;
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:,end));

figure, subplot(121), plot(ttl_sig(:,1)), hold all, plot(trig_out_aix((Dec_trig(1)+1):end,1,1))
subplot(122), plot(Dec_trig),
%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2));
PD_sig =reshape(PD_sig, n, n_avg*n_it);
PD_sig=PD_sig(:,min_ind:max_ind);

center=1630;
for k=1:size(PD_sig,2)
    nrj_pulse(k)=sum(PD_sig(center+(-25:25),k)-mean(PD_sig(3001:4000,k)));
    sig_photo(:,k)=PD_sig(center+(-25:25),k)-mean(PD_sig(3001:4000,k));
end

figure; imagesc(sig_photo);
% return
%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for k=1:size(Align,2)
    AAlign=xcorr(Align(:,k),ref,'coeff');
    I=find(AAlign==max(AAlign));
    Dec_pulse(k)=I-size(Align,1);
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo



%%
c= 1480;

image_width = 20e-3;
offset_width=0e-3;
image_depth =20e-3;
offset_depth =15e-3;
interpfact=1;
t0=0;
fs=60e6*interpfact;
BF_check=0; %0->J�r�me style, 1->Manu style
NbElemts=size(PA_sig,2);

% n�cessaire pour Jerome
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/fs/interpfact));% number of pixel on the other side
n=m;
Bp_image=zeros(m, n,100,'single');
Dec2=((Dec_trig)*fs/f_oscillo); %convertit le temps oscillo en temps aixplorer


%% Normalisation + Recalage des signaux (pour le delay entre ttl et trig_aix) + Filtrage passe-bas (standard) des signaux
%     PA_sig0=x0;
% [b,a] =butter(3,[1e6 8e6]/(fs/2));
[b,a] =butter(3,[1e6 8e6]/(fs/2));

for k=1:size(PA_sig,3)
    PA_sig(:,:,k) = DelaySignal(PA_sig(:,:,k),1,Dec2(k)+150*interpfact); %les 150 correspondent aux points supprim�s plus haut
    PA_sig(:,:,k)= single(filtfilt(b,a,double(PA_sig(:,:,k))))./nrj_pulse(k);
    % on filtre et recale aussi x0 car on doit l'utiliser plus loin
    %         x0 (:,:,k) = DelaySignal(x0(:,:,k),1,Dec2(k,ik)+150*interpfact);
    %         x0 (:,:,k)= single(filtfilt(b,a,double(x0 (:,:,k))))./nrj_pulse(k,ik);
end
%     PA_sig=PA_sig(:,:,3:end,:);
%     x0=x0(:,:,3:end,:);

 %% filtrage passe-haut des signaux dans l'espace des voies pour supprimer les pics de bruit et affiner le recalage des signaux entre eux
% seuil=0.01;
% [b a] =butter(3,seuil,'high');
% figure, subplot(121), imagesc(PA_sig(:,:,1))
% for k=1:size(PA_sig,3)
%     PA_sig1(:,:,k)= (single(filtfilt(b,a,double(PA_sig(:,:,k))')))';
% end
% subplot(122), imagesc(PA_sig1(:,:,1))
% return
%% methode manu: on enl�ve la moyenne des signaux pour couper les pics
% � bricoler si beaucoup de signaux quasi "horizontaux"??
figure, subplot(121), imagesc(PA_sig(:,:,1))
for k=1:size(PA_sig,3)
    for j=1:size(PA_sig,2)
    PA_sig2(:,j,k)= PA_sig(:,j,k)-squeeze(mean(PA_sig(:,[1:42,87:128],k),2));
    end
end
subplot(122), imagesc(PA_sig2(:,:,1))

%% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
% (Dec_pulse n'est pas utilis� finalement...?)


%     if ik==1 %ok pour psf, mais �trange pour rot, on pourrait avoir des variations dues au speckle ...?
% --> du coup pas besoin de tout filtrer pour obtenir le bon x0 plus haut
Refx=mean(double(squeeze(PA_sig2((600*interpfact):(1100*interpfact),32:3:96,:))),3); %attention � bien tout prendre, plot pour checker si besoin
%     end

% calcul decalage sur signal filtr� dans l'espace des voies
for i=1:size(PA_sig,3)
    cc = xcorr2(double(squeeze(PA_sig2((600*interpfact):(1100*interpfact),32:3:96,i))),Refx);
    cc2=interp(cc(:),10);
    [max_cc, imax] = max(abs(cc2));
    [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
    corr_offset(i,:) = [ (ypeak-size(Refx,1)*10 +9) (xpeak-size(Refx,2)) ];
end
Dec=corr_offset(:,1);
figure, plot(Dec)

for k=1:size(PA_sig,3);
    PA_sig3(:,:,k) = DelaySignal(PA_sig2(:,:,k),1,t0-Dec(k)/10);
end


%%
% figure(666)
% for k=1:1000
%     imagesc(PA_sig3(:,:,k))
%     title(num2str(k))
%     pause(0.3)
% end
% 
% varsig=var(PA_sig3,[],3);
%%
clear PA_sig PA_sig2
for ik= 1:size(PA_sig3,3)
  
    ik
    
    %Beamforming J�r�me's style
        x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
        y_array = - offset_depth*ones(1,NbElemts);
        sizeT = size(PA_sig3,1)-1;
        [N, M] = meshgrid( 1:n, 1:m );
        xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
        yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
        clear N M SS
        
        for i = 1:length(x_array)
            x_sensor = x_array(i) ;
            y_sensor = y_array(i) ;
            x = x_sensor - xr;
            y = y_sensor - yr;
            s = sqrt( x.^2 + y.^2 );
            ss = round( s*fs/c +  1 );
            ss( ss > sizeT ) = sizeT;
            ss( ss <= 0 ) = 1;
            SS(:,:,i)=ss;
        end
        
        sigMat=squeeze(double(PA_sig3(:,:,ik))/4);
        bp_image = zeros(m, n);
        tic
        for i = 1:length(x_array)
            A0 = -diff(sigMat(:,i));
            s1 = A0(squeeze(SS(:,:,i)));
            bp_image = bp_image + s1;
        end
        Bp_image(:,:,ik) =single(bp_image);
end

figure(1), imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3, Bp_image(:,:,ik) ),axis image


% save ('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum_lowPassFilt8MHzLarge.mat','nrj_pulse','Bp_image','BF_check','-v7.3')
return
 %%
% clear all
% close all
% load('D:\Thomas_Data\Fevrier2016\BF_2pts_ink1_img1to2500_2016315.mat')
% Bp_image_selec(:,:,1:500)=Bp_image(:,:,2001:2500);
% load('D:\Thomas_Data\Fevrier2016\BF_2pts_ink1_img2501to5000_2016315.mat')
% Bp_image_selec(:,:,501:1000)=Bp_image(:,:,1:500);
% clear Bp_image

% for PSF1
% figure,plot(max(squeeze(Bp_image(:,250,:)),[],1))
% for kk=[509,531,592,844,875]
% Bp_image(:,:,kk)=Bp_image(:,:,530);
% end
Bp_image(:,:,51)=Bp_image(:,:,50);
h=1:100;
Y=1:405;
X=1:405;
Image_Mean=mean(Bp_image,3);
Image_Var=var(Bp_image,1,3);

figure;
subplot(121), imagesc(Image_Mean(Y,X));colormap jet,axis image,colorbar
subplot(122); imagesc(sqrt(Image_Var(Y,X))),colormap jet,axis image
title('std')
colorbar
%%
load('D:\Thomas_Data\Mars2016\BF_X_ink_static1_img1to2500_2016329.mat')
Bp_image2=Bp_image;
load('D:\Thomas_Data\Mars2016\BF_X_ink_fluctest1_2016331.mat')
Bp_image2(:,:,2501:5000)=Bp_image;

% Y=301:500;
% X=301:500;

%%
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum.mat')
% Bp_old=Bp_image;
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum_lowPassFilt8MHzLarge.mat')
% Bp_new=Bp_image;

for k=49:size(Bp_image,3)
    figure(666)
    subplot(121),
    imagesc(Bp_old(300:500,300:500,k)), axis xy, axis image
     subplot(122),imagesc(Bp_new(300:500,300:500,k)) , axis xy, axis image
    title(num2str(k))
    pause%(0.5)
    
% Var(:,:,1+floor(k/500))=var(Bp_image2(:,:,k:(k+499)),1,3);
% Mean(:,:,1+floor(k/500))=mean(Bp_image2(:,:,k:(k+499)),3);
end

%%
% Y=301:500;
% X=301:500;
figure(1)
for k=1:10
subplot(211), imagesc(abs(Mean(:,:,k))), axis image, axis xy, colormap jet
subplot(212), imagesc(sqrt(Var(:,:,k))), axis image, axis xy,
k
    pause
end
%%
Y=301:450;
X=301:550;
figure(1)
for k=1000%:20:size(Bp_image,3)
    for j=k:k+19
        figure(1)
        if j==k+19
            title('PA_img-mean_image')
        end
        subplot(4,5,j-k+1), imagesc(Bp_image(Y,X,j)-Image_Mean(Y,X)), axis image, axis xy, colormap jet
        
        figure(2)
         if j==k+19
            title('PA_img')
        end
        subplot(4,5,j-k+1), imagesc(Bp_image(Y,X,j)), axis image, axis xy, colormap jet
        
    end
    
    
%     pause
end

%%
figure, subplot(121), imagesc(PA_sig(:,:,10))   
subplot(122); imagesc(PA_sig(:,:,82)),colormap jet

%% higher order cumulants and moments
% Bp_image1000to2000=Bp_image(:,:,1000:2000);
%
Y=301:450;
X=301:550;
load('D:\Thomas_Data\Fevrier2016\BF_X_ink2_201632.mat')
BP_img_LargeDataSet(:,:,1:2500)=Bp_image(Y,X,:);
clear Bp_image
 load('D:\Thomas_Data\Fevrier2016\BF_X_ink3_201632.mat')
BP_img_LargeDataSet(:,:,2501:4000)=Bp_image(Y,X,:);
clear Bp_image
load('D:\Thomas_Data\Fevrier2016\BF_X_ink4_201632.mat')
BP_img_LargeDataSet(:,:,4001:8000)=Bp_image(Y,X,:);
clear Bp_image
 %%
mom2=moment(BP_img_LargeDataSet(:,:,:),2,3);
mom3=moment(BP_img_LargeDataSet(:,:,:),3,3);
mom4=moment(BP_img_LargeDataSet(:,:,:),4,3);
mom5=moment(BP_img_LargeDataSet(:,:,:),5,3);
mom6=moment(BP_img_LargeDataSet(:,:,:),6,3);

% moy1000to2000=mean(Bp_image1000to2000,3);

cum2=mom2;
cum3=mom3;
cum4=mom4-3*cum2.^2;
cum5=mom5-10*cum3.*cum2;
cum6=mom6-15*cum4.*cum2-10*cum3.^2-15*cum2.^3;
%%
% Y=301:450;
% X=301:550;
Y=41:100;
X=41:120;
% figure(1),  imagesc(abs(Image_Mean(Y,X)).^2), axis image, axis xy
figure(2),
subplot(251), imagesc(abs(cum2(Y,X))), axis image, axis xy,title('2nd order cumulant')
subplot(252), imagesc(abs(cum3(Y,X))), axis image, axis xy,title('3rd order cumulant')
subplot(253), imagesc(abs(cum4(Y,X))), axis image, axis xy,title('4th order cumulant')
subplot(254), imagesc(abs(cum5(Y,X))), axis image, axis xy,title('5th order cumulant')
subplot(255), imagesc(abs(cum6(Y,X))), axis image, axis xy,title('6th order cumulant')

subplot(256), imagesc(abs(mom2(Y,X))), axis image, axis xy,title('2nd order moment')
subplot(257), imagesc(abs(mom3(Y,X))), axis image, axis xy,title('3rd order moment')
subplot(258), imagesc(abs(mom4(Y,X))), axis image, axis xy,title('4th order moment')
subplot(259), imagesc(abs(mom5(Y,X))), axis image, axis xy,title('5th order moment')
subplot(2,5,10), imagesc(abs(mom6(Y,X))), axis image, axis xy,title('6th order moment')



%% movie with all frames
figure(10)
% Y=301:450;
% X=301:550;
Y=341:400;
X=341:420;
% myVideo = VideoWriter('X_ink2_201632_framemovie_1000to2000.avi');
% myVideo.FrameRate = 5;  % Default 30
% myVideo.Quality = 30;    % Default 75
% open(myVideo);
for k=1:size(Bp_image,3)
    k
    %subplot(211), imagesc(Bp_image(Y,X,k),[-110 150]),axis image, axis xy, colormap jet, colorbar, title(num2str(k))
    %subplot(212), imagesc(Bp_image(410:510,300:440,k)),axis image, axis xy, colormap jet, colorbar, title(num2str(k))
%     a=imread(['D:\Thomas_Data\Fevrier2016\X_ink2_201632_framemovie\X_ink2_201632_img' num2str(k) '.png']);
%       writeVideo(myVideo,a);
[maxx ind]=max(max(Bp_image(410:510,300:440,k)));
[i(k) j(k)]=find(Bp_image(410:510,300:440,k)==maxx,1);
% saveas(gcf,['D:\Thomas_Data\Fevrier2016\X_ink2_201632_framemovie\X_ink2_201632_img' num2str(k) '.png'])
%         pause(0.01)
end
% close(myVideo);
    
    
    

