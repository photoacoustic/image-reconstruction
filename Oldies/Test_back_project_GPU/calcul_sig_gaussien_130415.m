function [sig2] = calcul_sig_gaussien_130415(Rmicro,laser_width,t_start,t_end,fs,c,x_trans_3D,y_trans_3D,z_trans_3D,x_micro1,y_micro1,z_micro1 )
 
t= single((t_start:t_end)*1/fs);
       
Distance = sqrt((x_trans_3D-x_micro1).^2+(y_trans_3D-y_micro1).^2+(z_trans_3D-z_micro1).^2);
                    
        sig = zeros(size(t),'single');
                   
            %% signal micro
           for k = 1:length(Distance)  
           sig1= -(t-Distance(k)/c).*8*log(2)*2*pi*Rmicro^2/(laser_width.^2).*exp(-(t-Distance(k)/c).^2/(laser_width.^2/(4*log(2))))/Distance(k);
            sig=sig+sig1;
           end
                        
        sig2=single(sig/length(Distance));

end

