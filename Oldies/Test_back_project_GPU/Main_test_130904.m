clear all
close all
clc
 matlabpool(3)

%% Position barette
N_trans=5e3;
x_trans_3D = linspace(-64.5*0.3e-3,64.5*0.3e-3,N_trans);
y_trans_3D = 1e-2*ones(1,N_trans);
z_trans_3D = zeros(1,N_trans);

c=1500;
fs=1e8;
laser_width=100e-9;

x_micro=0;
y_micro=0;
z_micro=0;

N_micro= length(x_micro);
Rmicro=1;
%% Generation signaux

t_start=1;
t_end=ceil((3e-2/c)*fs);
Lt=t_end-t_start+1;

SigMat =zeros(Lt,N_micro,'single');
              
  
    tic 
    
    for j=1:N_micro
parfor k=1:N_trans
   
        

   SigMat(:,k,j)=calcul_sig_gaussien_130415(Rmicro,laser_width,t_start,t_end,fs,c,x_trans_3D(k),y_trans_3D(k),z_trans_3D(k),x_micro(j),y_micro(j),z_micro(j) );
      
    end
    
end
     toc
     
     figure; imagesc(SigMat)
     
     %% Recon
     
     n=500;
     image_width= 1e-3;
     
     t=(2:Lt)/fs;
     
     pos_sensor(:,1)=x_trans_3D;
     pos_sensor(:,2)=y_trans_3D;
     pos_sensor(:,3)=z_trans_3D;
     pos_sensor=single(pos_sensor);
     
     startupcl;
     
     tic
     
           Recon = backprojects3d_cl( -diff(SigMat), n, pos_sensor, c, 1, t', fs, image_width ) ;
       
           toc
           
           cleanupcl;
           
           figure; imagesc(Recon);
           
           %%
           
           
            matlabpool close