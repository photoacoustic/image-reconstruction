clear; close all; clc

addpath('D:\Data_jerome\corelib');

startupcl;

%% load data
load('D:\Data_jerome\28MHz_crazy_scan\Manip_kidney_papier_oct2012\Kidney_IR_dye_cross_sut_121030\data_28MHz\scan_50um_usphere_discreet_700nm_crazy28MHz_posi_120ang_21posi_735172724972_part1.mat')

%%
sigMat=single(sigMat);

load Sens_flip;

Sens_flip=1./Sens_flip;
Sens_flip(127)=0;
Amp_corr(:,:,1)=repmat(Sens_flip,size(sigMat,1),1);

hh=500;
h=[1:hh:size(sigMat,3) size(sigMat,3)];
Amp_corrn=repmat(Amp_corr,[1,1,hh+1]);
Amp_corrfin=repmat(Amp_corr,[1,1,(h(end)-h(end-1)+1)]);
clear Amp_corr


Filter = [2e6 40e6];
Order = 3;
fs=125e6;


signal_3D_array=zeros(size(sigMat),'single');
for i=1:(length(h)-1)
    [t signal_3D_array1 ] =  Flip_amp120625(sigMat(:,:,h(i):h(i+1)),Filter,Order);
    %return
    %signal_correction_amp_elect_120515(sigMat(:,:,h(i):h(i+1)));
    if i<(length(h)-1)
    signal_3D_array(:,:,h(i):h(i+1))= - single( signal_3D_array1.*Amp_corrn ); % signe moins pour compenser
    else
        signal_3D_array(:,:,h(i):h(i+1))= - single( signal_3D_array1.*Amp_corrfin ); % signe moins pour compenser
     
    end
    
    clear signal_3D_array1
end
clear sigMat Amp_corrfin Amp_corrn

signal_3D_array=signal_3D_array(2:end,:,1:length(Scan_para.Phi));

%%
fs=125e6;
%t=((1:1:size(signal_3D_array,1)))/fs;

clear tt
tt1=repmat(t(1:(length(t)))',1,size(signal_3D_array,2));
tt(:,:,2)=tt1;
tt=repmat(tt(:,:,2),[1,1,size(signal_3D_array,3)]);
clear tt1

%%

signal_3D_array_full = zeros(size(signal_3D_array));
signal_3D_array_full(1:(length(t)-1),:,:) = -(signal_3D_array(2:length(t),:,:)-signal_3D_array(1:(length(t)-1),:,:))./(t(2)-t(1));

signal_3D_array_fullt=single(signal_3D_array_full.*tt);

signal_3D_array_full=single(signal_3D_array_full);


clear signal_3D_array


%% speed of sound

COR = 7.5e-3 ;
image_width_1pos = 6e-3%127*3e-4 ;
n1=round(image_width_1pos/(1500/fs));
pitch=70e-6;
pos_sensor(:,1)=linspace(-(63*pitch+pitch/2),(63*pitch+pitch/2),128);
pos_sensor(:,2)=-COR*ones(128,1);
pos_sensor(:,3)=zeros(128,1);

pos_sensor=single(pos_sensor);

%L=1:10;
L=I_selec;

C=1460:2:1520;
clear mimag

mimag=zeros(length(C),length(L));

figure(11);
ii=1;
for c_ii = C
    
    i_L=1;
    for i=L
        
        Recon = backprojects3d_cl( squeeze(signal_3D_array_fullt(:, :, i)), n1, pos_sensor, c_ii, 1, t', fs, image_width_1pos ) ;
        
        
        Recon=flipud(Recon);
        limits_1pos=linspace(-image_width_1pos/2,image_width_1pos/2,n1);
        
        figure(31);
        imagesc(limits_1pos*1e3,limits_1pos*1e3, Recon  ); axis xy equal ; colormap(bone);
        xlabel('z (mm)')
        ylabel('depth (mm)')
        title(num2str(c_ii)); pause(0.1);
        
        mimag(ii,i_L) = max( max( ( Recon) )) ;
        %C(ii)=c_ii;
        
        figure(32); imagesc(1:length(L),C,mimag./max(mimag(:)));
        
        pause(0.1)
        
        i_L=i_L+1;
       
    end
    
     ii=ii+1;
    
end ;

  figure; plot(C,mimag./max(mimag(:)));
figure; plot(C,sum(mimag,2));
%% bilan

c=1488;


%% visulasation for 1 position
COR = 7.5e-3 ;
image_width_1pos = 6e-3;%127*3e-4 ;
n1=round(image_width_1pos/(c/fs));
image_width_1pos = n1*(c/fs);

pitch=70e-6;
pos_sensor1(:,1)=linspace(-(63*pitch+pitch/2),(63*pitch+pitch/2),128);
pos_sensor1(:,2)=-COR*ones(128,1);
pos_sensor1(:,3)=zeros(128,1);
pos_sensor1=single(pos_sensor1);

% positioning

Recon_positioning = backprojects3d_cl( squeeze(signal_3D_array_fullt(:, :, I_selec(40))), n1, pos_sensor1, c, 1, t', fs, image_width_1pos ) ;

%round(size(signal_3D_array,3)/2)
Recon_positioning=flipud(Recon_positioning);
limits_1pos=linspace(-image_width_1pos/2,image_width_1pos/2,n1);


figure;
imagesc(1:128,1:128, Recon_positioning); axis xy image; colormap bone; colorbar;


figure; imagesc(limits_1pos*1e3,limits_1pos*1e3+COR*1e3,Recon_positioning); axis xy image ; colormap(bone);colorbar;
xlabel('z (mm)')
ylabel('depth (mm)')

%% recontruire toutes les positions et prendre valeur du max et position en x,y

clear xmic ymic MMM

limits_1pos=linspace(-image_width_1pos/2,image_width_1pos/2,n1);

for i=1:size(signal_3D_array_fullt,3)
    
    Recon_positioning = backprojects3d_cl( squeeze(signal_3D_array_fullt(:, :, i)), n1, pos_sensor1, c, 1, t', fs, image_width_1pos ) ;
    
    Recon_positioning=flipud(Recon_positioning);
    limits_1pos=linspace(-image_width_1pos/2,image_width_1pos/2,n1);
    
    
    MMM(i)= max(Recon_positioning(:));
    I=find(Recon_positioning(:)==MMM(i));
    [ymic(i) xmic(i)]=ind2sub(size(Recon_positioning),I);
    
    
    % imagesc(limits_1pos*1e3,limits_1pos*1e3+COR*1e3, Recon_positioning); axis xy image; colormap bone; colorbar;
    figure(401)
    imagesc( Recon_positioning); axis xy image; colormap bone; colorbar;
    figure(402)
    plot(xmic); hold on; plot([1 i],[1 1]*mean(xmic(1:i)),'b--'); plot(ymic,'r'); plot(MMM*1e-4,'k');
    
   legend('xmic','mean','ymic','max')
    hold off
    
    %
    %     hold on; plot([1 1]*(-1.5), [-10 10],'--')
    %     plot([1 1]*1.5, [-10 10],'--')
    drawnow
    pause(0.1)
end

%% selection projection avec bonne ampli

I_selec=find(MMM>max(MMM*0.5));
mean(xmic(I_selec))
std(xmic(I_selec))
figure(403)
plot(xmic(I_selec)); hold on; plot(ymic(I_selec),'r'); plot(MMM(I_selec)*1e-5,'k');

plot([1 length(I_selec)],[1 1]*mean(xmic(I_selec)),'b--')
hold off


%% Parametres en 2D

z_recon2D=limits_1pos(round(mean(xmic(I_selec))));
element_number_2D=round((z_recon2D+127*pitch/2)/pitch)+1

%%%
facteur_voxel=1;
image_width=Scan_para.N_posi*Scan_para.Step_posi%-0.5e-3;
n=round(image_width/(facteur_voxel*c/fs));
image_width=n*(c/fs);
I_selec=1:size(signal_3D_array_fullt,3);
maxi =  length(I_selec) ;
maxih = 60000;  % round( maxi/2 ) ;
if maxih<=maxi
    mm=[maxih:maxih:maxi maxi];
    
else
    mm=maxi;
end
%%%


% %% find radius
clear pos_sensor

R_tomo=7.8e-3+linspace(-1e-4, 1e-4, 51 ) ;  %19.28e-3;
L_tomo=(-200e-6:10e-6:200e-6);

mimag2=zeros(length(R_tomo),length(L_tomo));
minimag2=zeros(length(R_tomo),length(L_tomo));
for kk= 1:length( L_tomo )
    
    for jj = 1 : length( R_tomo )
        
        % Recon_bp2D=zeros([n,n]);
        
        Recon_posi.R= R_tomo(jj) ;
        center_Real=Scan_para.y_center-L_tomo(kk);
        
        r_posi=  sqrt(( (Scan_para.Y_real(I_selec)*1e-3)'-center_Real).^2+Recon_posi.R^2);
        angle_posi = sign((Scan_para.Y_real(I_selec)*1e-3)'-center_Real).*acos( Recon_posi.R./ r_posi ) ;
        pos_sensor(:,1)=cos( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
        pos_sensor(:,2)=sin( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
        
        pos_sensor(:,3)=zeros(size(pos_sensor(:,2)));
        
        P=single(pos_sensor);
        S=squeeze(signal_3D_array_full(:,element_number_2D,I_selec));
        
        Recon_cl = backprojects3d_cl( S( :, 1 : mm(1) ), n, P( 1 : mm(1), : ), c, 1, t', fs, image_width ) ;
        
        if length(mm)>1
            for i=2:length(mm)
                paues(0.1)
                Recon_cl = Recon_cl + backprojects3d_cl( S( :, mm(i-1)+1: mm(i) ), n, P( mm(i-1)+1: mm(i), : ), c, 1, t', fs, image_width ) ;
            end
            
        end
        
        Recon_bp2D = Recon_cl;
        
        mimag2(jj,kk) = max( Recon_bp2D(:)  )  ;
        minimag2(jj,kk) = min( Recon_bp2D(:)  )  ;
        
        figure(121);
        imagesc(Recon_bp2D); axis xy image ; colormap(bone);colorbar;
        pause(0.1);
        
        
    end
    figure(122);
    close(122)
    figure(122);
    subplot(1,3,1)
    imagesc(R_tomo,L_tomo(1:kk),mimag2(:,1:kk)'); colorbar;
    subplot(1,3,2)
    imagesc(R_tomo,L_tomo(1:kk),-minimag2(:,1:kk)'); colorbar;
    subplot(1,3,3)
    imagesc(R_tomo,L_tomo(1:kk),-(mimag2(:,1:kk)./minimag2(:,1:kk))'); colorbar;
    drawnow;
    pause(0.01)
    
    
end

T1=mimag2%-(mimag2.^2)./minimag2;
I2D=find(T1==max(T1(:)));
[R2D L2D]=ind2sub(size(T1),I2D);
R_tomo(R2D)*1e3
L_tomo(L2D)*1e6
figure; plot( mimag2')
%% bilan

R_tomo2=7.27e-3;
L_tomo2=500e-6;
% 
% 
% 
% %%
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %% reconstruction find radius with alpha and beta=0
% F_D_beamform=0.5;
% 
% facteur_voxel=1;
% pitch=70e-6;
% 
% %
% zz_observation=z_recon2D % +(-0.15e-3:(facteur_voxel*2*c/fs):0.15e-3);
% 
% %%% backprojection
% image_width=Scan_para.N_posi*Scan_para.Step_posi-0.5e-3;
% n=round(image_width/(facteur_voxel*c/fs));
% image_width=n*(c/fs);
% 
% % %% find radius
% clear pos_sensor
% %
% clear mimag3
% %
% R_tomo=R_tomo2+linspace(-5e-5, 5e-5, 51 ) ;  %19.28e-3;
% L_tomo= L_tomo2 +(-200e-6:10e-6:200e-6);%
% Alpha_tomo=0%linspace(-1,1,21)*pi/180;
% Betha_tomo=0%linspace(-10,10,21)*pi/180;
% 
% 
% mimag3=zeros(length(R_tomo),length(L_tomo),length(Alpha_tomo),length(Betha_tomo));
% 
% 
% minimag3=zeros(length(R_tomo),length(L_tomo),length(Alpha_tomo),length(Betha_tomo));
% 
% maxi = 128 * length(I_selec) ;
% maxih = 65000;  % round( maxi/2 ) ;
% if maxih<=maxi
%     mm=[maxih:maxih:maxi maxi];
%     
% else
%     mm=maxi;
% end
% 
% for aa= 1:length( Alpha_tomo )
%     
%     for bb= 1:length(  Betha_tomo )
%         
%         for kk= 1:length( L_tomo )
%             
%             
%             for jj = 1 : length( R_tomo )
%                 
%                 % 3D projection
%                 Recon_bp3D=zeros([n,n,length(zz_observation)]);
%                 
%                 zzzz=linspace(-(63*pitch+pitch/2),(63*pitch+pitch/2),128);
%                 Zd=(zzzz-zzzz(element_number_2D))*cos(Alpha_tomo(aa))*cos(Betha_tomo(bb));
%                 
%                 pos_sensor=zeros(128,3,length(I_selec));
%                 
%                 
%                 for i_d = 1:128
%                     
%                     Recon_posi.R= R_tomo(jj) + Zd(i_d)*tan(Alpha_tomo(aa));
%                     center_Real=Scan_para.y_center-L_tomo(kk)+ Zd(i_d)*tan(Betha_tomo(bb));
%                     
%                     r_posi=  sqrt(( (Scan_para.Y_real(I_selec)*1e-3)'-center_Real).^2+Recon_posi.R^2);
%                     angle_posi = sign((Scan_para.Y_real(I_selec)*1e-3)'-center_Real).*acos( Recon_posi.R./ r_posi ) ;
%                     pos_sensor(i_d,1,:)=cos( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
%                     pos_sensor(i_d,2,:)=sin( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
%                     
%                 end
%                 
%                 
%                 P = zeros(  maxi, 3, 'single' ) ;
%                 
%                 
%                 Signal_3D_grow_aper_full=single( signal_3D_array_fullt(:,:,I_selec));
%             
%                 S = reshape( squeeze( Signal_3D_grow_aper_full ), size( Signal_3D_grow_aper_full, 1 ), 128 * size( Signal_3D_grow_aper_full, 3 ) ) ;
%                 
%                 
%                 
%                 tic;
%                 for i_z=1: length(zz_observation)
%                     % i_z,
%                     
%                     
%                     
%                     %  [ Signal_3D_grow_aper_full]=growing_apperture_signals120608(F_D_beamform, zz_observation(i_z),t,c,pitch,signal_3D_array_full);
%                     %Signal_3D_grow_aper_full=single(Signal_3D_grow_aper_full);
%                     
%                     
%                     pos_sensor(:,3,:) = single( (Zd+zzzz(element_number_2D)-zz_observation(i_z))'*ones(1,size(pos_sensor,3)) ) ;
%                     pos_sensor = single( pos_sensor ) ;
%                     
%                     for ii=1: size(pos_sensor,3)
%                         P((1:128)+(ii-1)*128,:) = pos_sensor(:,:,ii);
%                     end
%                     
%                     
%                     %P=P(1:facteur_proj:end,:);
%                     
%                     clear Signal_3D_grow_aper_full
%                     
%                     Recon_cl = backprojects3d_cl( S( :, 1 : mm(1) ), n, P( 1 : mm(1), : ), c, 1, t', fs, image_width ) ;
%                     
%                     if length(mm)>2
%                         for i=2:length(mm)
%                             Recon_cl = Recon_cl + backprojects3d_cl( S( :, mm(i-1)+1: mm(i) ), n, P( mm(i-1)+1: mm(i), : ), c, 1, t', fs, image_width ) ;
%                         end
%                         
%                     end
%                     
%                     Recon_bp3D(:,:,i_z) = Recon_cl;
%                     
%                     
%                     %                     tic
%                     figure(12);
%                     imagesc(Recon_bp3D(:,:,i_z)); axis xy image ; colormap(bone);colorbar;
%                     pause(0.1);
%                     %                   toc
%                     %
%                     
%                     
%                     
%                     
%                 end
%                 toc
%                 clear S
%                 
%                 mimag3(jj,kk,aa,bb)=max(Recon_bp3D(:));
%                 
%                 minimag3(jj,kk,aa,bb)=min(Recon_bp3D(:));
%                 %       figure(32);
%                 %      plot(R_tomo(1:jj),mimag3,'*-r');
%                 %      hold off;
%                 %      drawnow;
%                 %      pause(0.01)
%                 %                 I=find( mimag3==max( mimag3(:)));
%                 %                 [am,bm,jm,km]=ind2sub(size(mimag3),I);
%                 %                 resu = [max( mimag3(:))*1e-6 Alpha_tomo(am)*180/pi Betha_tomo(bm)*180/pi R_tomo(jm)*1e3 L_tomo(km)*1e6],
%                 %                 pause(1)
%                 
%                 
%                 
%                 figure(32)
%                 close(32)
%                 figure(32)
%                 subplot(1,3,1)
%                 imagesc(R_tomo,L_tomo(1:kk),squeeze(mimag3(:,1:kk,:,:))');
%                 colorbar
%                 subplot(1,3,2)
%                 imagesc(R_tomo,L_tomo(1:kk),-squeeze(minimag3(:,1:kk,:,:))');
%                 colorbar
%                 subplot(1,3,3)
%                 imagesc(R_tomo,L_tomo(1:kk),-(squeeze(mimag3(:,1:kk,:,:))./squeeze(minimag3(:,1:kk,:,:)))');
%                 colorbar
%                 drawnow;
%                 %pause(1)
%             end
%             
%         end
%         
%     end
%     
% end
% 
% %save test_deconv1 R_tomo L_tomo Alpha_tomo Betha_tomo mimag3 zz_observation
% 
% figure;  imagesc(R_tomo,L_tomo(1:kk),-(squeeze(mimag3(:,1:kk,:,:).^2)./squeeze(minimag3(:,1:kk,:,:)))');
% 
% figure; imagesc(R_tomo,L_tomo(1:kk),mimag3')
% 
% %% bilan
% 
% R_tomo3=7.256e-3;
% L_tomo3=530e-6;%%
% 
% 
% 
% %% Alpha and betha
% 
% 
% F_D_beamform=0.5;
% 
% facteur_voxel=1;
% pitch=70e-6;
% 
% %
% zz_observation=z_recon2D +(-0.3e-3:(facteur_voxel*2*c/fs):0.3e-3);
% 
% %%% backprojection
% image_width=Scan_para.N_posi*Scan_para.Step_posi;
% n=round(image_width/(facteur_voxel*c/fs));
% image_width=n*(c/fs);
% 
% % %% find radius
% clear pos_sensor
% %
% clear mimag3
% %
% R_tomo=R_tomo3;%7.4e-3+linspace(-20e-5, 20e-5, 11 ) ;  %19.28e-3;
% L_tomo=L_tomo3;%(-200e-6:50e-6:200e-6);
% Alpha_tomo=linspace(-2,2,21)*pi/180;
% Betha_tomo=-4*pi/180+linspace(-2,2,11)*pi/180;
% 
% 
% mimag3=zeros(length(R_tomo),length(L_tomo),length(Alpha_tomo),length(Betha_tomo));
% 
% minimag3=zeros(length(R_tomo),length(L_tomo),length(Alpha_tomo),length(Betha_tomo));
% 
% maxi = 128 * length(I_selec) ;
% maxih = 65000;  % round( maxi/2 ) ;
% if maxih<=maxi
%     mm=[maxih:maxih:maxi maxi];
%     
% else
%     mm=maxi;
% end
% 
% for aa= 1:length( Alpha_tomo )
%     
%     for bb= 1:length(  Betha_tomo )
%         
%         for kk= 1:length( L_tomo )
%             
%             
%             for jj = 1 : length( R_tomo )
%                 
%                 % 3D projection
%                 Recon_bp3D=zeros([n,n,length(zz_observation)]);
%                 
%                 zzzz=linspace(-(63*pitch+pitch/2),(63*pitch+pitch/2),128);
%                 Zd=(zzzz-zzzz(element_number_2D))*cos(Alpha_tomo(aa))*cos(Betha_tomo(bb));
%                 
%                 pos_sensor=zeros(128,3,length(I_selec));
%                 
%                 
%                 for i_d = 1:128
%                     
%                     Recon_posi.R= R_tomo(jj) + Zd(i_d)*tan(Alpha_tomo(aa));
%                     center_Real=Scan_para.y_center-L_tomo(kk)+ Zd(i_d)*tan(Betha_tomo(bb));
%                     
%                     r_posi=  sqrt(( (Scan_para.Y_real(I_selec)*1e-3)'-center_Real).^2+Recon_posi.R^2);
%                     angle_posi = sign((Scan_para.Y_real(I_selec)*1e-3)'-center_Real).*acos( Recon_posi.R./ r_posi ) ;
%                     pos_sensor(i_d,1,:)=cos( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
%                     pos_sensor(i_d,2,:)=sin( angle_posi+Scan_para.Phi_posi_real(I_selec)'*pi/180).* r_posi;
%                     
%                 end
%                 
%                 
%                 P = zeros(  maxi, 3, 'single' ) ;
%                 
%                 
%                 Signal_3D_grow_aper_full=single( signal_3D_array_fullt(:,:,I_selec));
%             
%                 S = reshape( squeeze( Signal_3D_grow_aper_full ), size( Signal_3D_grow_aper_full, 1 ), 128 * size( Signal_3D_grow_aper_full, 3 ) ) ;
%                 
%                 
%                 
%                 tic;
%                 for i_z=1: length(zz_observation)
%                     % i_z,
%                     
%                     
%                     
%                     %  [ Signal_3D_grow_aper_full]=growing_apperture_signals120608(F_D_beamform, zz_observation(i_z),t,c,pitch,signal_3D_array_full);
%                     %Signal_3D_grow_aper_full=single(Signal_3D_grow_aper_full);
%                     
%                     
%                     pos_sensor(:,3,:) = single( (Zd+zzzz(element_number_2D)-zz_observation(i_z))'*ones(1,size(pos_sensor,3)) ) ;
%                     pos_sensor = single( pos_sensor ) ;
%                     
%                     for ii=1: size(pos_sensor,3)
%                         P((1:128)+(ii-1)*128,:) = pos_sensor(:,:,ii);
%                     end
%                     
%                     
%                     %P=P(1:facteur_proj:end,:);
%                     
%                     clear Signal_3D_grow_aper_full
%                     
%                     Recon_cl = backprojects3d_cl( S( :, 1 : mm(1) ), n, P( 1 : mm(1), : ), c, 1, t', fs, image_width ) ;
%                     
%                     if length(mm)>2
%                         for i=2:length(mm)
%                             Recon_cl = Recon_cl + backprojects3d_cl( S( :, mm(i-1)+1: mm(i) ), n, P( mm(i-1)+1: mm(i), : ), c, 1, t', fs, image_width ) ;
%                         end
%                         
%                     end
%                     
%                     Recon_bp3D(:,:,i_z) = Recon_cl;
%                     
%                     
%                     %                     tic
%                     figure(12);
%                     imagesc(Recon_bp3D(:,:,i_z)); axis xy image ; colormap(bone);colorbar;
%                     pause(0.1);
%                     %                   toc
%                     %
%                     
%                     
%                     
%                     
%                 end
%                 toc
%                 clear S
%                 
%                 mimag3(jj,kk,aa,bb)=max(Recon_bp3D(:));
%                 
%                 minimag3(jj,kk,aa,bb)=min(Recon_bp3D(:));
%                 %       figure(32);
%                 %      plot(R_tomo(1:jj),mimag3,'*-r');
%                 %      hold off;
%                 %      drawnow;
%                 %      pause(0.01)
%                 %                 I=find( mimag3==max( mimag3(:)));
%                 %                 [am,bm,jm,km]=ind2sub(size(mimag3),I);
%                 %                 resu = [max( mimag3(:))*1e-6 Alpha_tomo(am)*180/pi Betha_tomo(bm)*180/pi R_tomo(jm)*1e3 L_tomo(km)*1e6],
%                 %                 pause(1)
%                 
%                 
%                 
%                 figure(32)
%                 subplot(1,3,1)
%                 imagesc(Alpha_tomo(1:aa)*180/pi,Betha_tomo*180/pi,squeeze(mimag3(1,1,1:aa,:))');
%                 colorbar
%                 subplot(1,3,2)
%                 imagesc(Alpha_tomo(1:aa)*180/pi,Betha_tomo*180/pi,-squeeze(minimag3(1,1,1:aa,:))');
%                 colorbar
%                 subplot(1,3,3)
%                 imagesc(Alpha_tomo(1:aa)*180/pi,Betha_tomo*180/pi,-(squeeze(mimag3(1,1,1:aa,:))./squeeze(minimag3(1,1,1:aa,:)))');
%                 colorbar
%                 drawnow;
%                 %pause(1)
%             end
%             
%         end
%         
%     end
%     
% end
% 
% save test_para_micro2 R_tomo L_tomo Alpha_tomo Betha_tomo mimag3 zz_observation minimag3
% 
% figure;  imagesc(Alpha_tomo(1:aa)*180/pi,Betha_tomo*180/pi,-(squeeze(mimag3(1,1,1:aa,:).^2)./squeeze(minimag3(1,1,1:aa,:)))');
% 
% 
% %% bilan
% 
% R_tomo3f=7.256e-3;
% L_tomo3f=530e-6;%%
% Alpha_tomo3f=-0.2*pi/180;
% Betha_tomo3f=-4.4*pi/180;%
% z_recon2D=1.0285e-004;
% element_number_2D=round((z_recon2D+127*pitch/2)/pitch)+1
% 
% 
% %% 3D reconstruct
% 
% 
% F_D_beamform=0.5;
% 
% facteur_voxel=1;
% pitch=70e-6;
% 
% %
% zz_observation=z_recon2D  +(-1e-3:(facteur_voxel*2*c/fs):1e-3);
% 
% %%% backprojection
% image_width=1e-3;%Scan_para.N_posi*Scan_para.Step_posi+1e-3;
% n=round(image_width/(facteur_voxel*c/fs));
% image_width=n*(c/fs);
% 
% % %% find radius
% clear pos_sensor
% %
% clear mimag3
% %
% R_tomo=R_tomo3f;
% L_tomo=L_tomo3f;
% Alpha_tomo=Alpha_tomo3f;
% Betha_tomo=Betha_tomo3f;
% 
% 
% maxi = 128 * size( signal_3D_array_full, 3 ) ;
% maxih = 65000;  % round( maxi/2 ) ;
% if maxih<=maxi
%     mm=[maxih:maxih:maxi maxi];
%     
% else
%     mm=maxi;
% end
% 
%                 
%                 % 3D projection
%                 Recon_bp3D=zeros([n,n,length(zz_observation)]);
%                 
%                 zzzz=linspace(-(63*pitch+pitch/2),(63*pitch+pitch/2),128);
%                 Zd=(zzzz-zzzz(element_number_2D))*cos(Alpha_tomo)*cos(Betha_tomo);
%                 
%                 pos_sensor=zeros(128,3,size( signal_3D_array_full, 3 ));
%                 
%                 
%                 for i_d = 1:128
%                     
%                     Recon_posi.R= R_tomo + Zd(i_d)*tan(Alpha_tomo);
%                     center_Real=Scan_para.y_center-L_tomo+ Zd(i_d)*tan(Betha_tomo);
%                     
%                     r_posi=  sqrt(( (Scan_para.Y_real(1:length(Scan_para.Y))*1e-3)'-center_Real).^2+Recon_posi.R^2);
%                     angle_posi = sign((Scan_para.Y_real(1:length(Scan_para.Y))*1e-3)'-center_Real).*acos( Recon_posi.R./ r_posi ) ;
%                     pos_sensor(i_d,1,:)=cos( angle_posi+Scan_para.Phi_posi_real(1:length(Scan_para.Y))'*pi/180).* r_posi;
%                     pos_sensor(i_d,2,:)=sin( angle_posi+Scan_para.Phi_posi_real(1:length(Scan_para.Y))'*pi/180).* r_posi;
%                     
%                 end
%                 
%                 
%                 P = zeros(  maxi, 3, 'single' ) ;
%                 
%                 
%              %   Signal_3D_grow_aper_full=single( signal_3D_array_fullt(:,:,I_selec));
%             
%                  
%                 
%                 
%                 tic;
%                 for i_z=1: length(zz_observation)
%                     % i_z,
%                     
%                     
%                     
%                      [ Signal_3D_grow_aper_full]=growing_apperture_signals120608(F_D_beamform, zz_observation(i_z),t,c,pitch,signal_3D_array_full);
%                     Signal_3D_grow_aper_full=single(Signal_3D_grow_aper_full);
%                     
%                      S = reshape( squeeze( Signal_3D_grow_aper_full ), size( Signal_3D_grow_aper_full, 1 ), 128 * size( Signal_3D_grow_aper_full, 3 ) ) ;
%               
%                     pos_sensor(:,3,:) = single( (Zd+zzzz(element_number_2D)-zz_observation(i_z))'*ones(1,size(pos_sensor,3)) ) ;
%                     pos_sensor = single( pos_sensor ) ;
%                     
%                     for ii=1: size(pos_sensor,3)
%                         P((1:128)+(ii-1)*128,:) = pos_sensor(:,:,ii);
%                     end
%                     
%                     
%                  
%                     
%                     clear Signal_3D_grow_aper_full
%                     
%                     Recon_cl = backprojects3d_cl( S( :, 1 : mm(1) ), n, P( 1 : mm(1), : ), c, 1, t', fs, image_width ) ;
%                     
%                     if length(mm)>2
%                         for i=2:length(mm)
%                             Recon_cl = Recon_cl + backprojects3d_cl( S( :, mm(i-1)+1: mm(i) ), n, P( mm(i-1)+1: mm(i), : ), c, 1, t', fs, image_width ) ;
%                         end
%                         
%                     end
%                     
%                     Recon_bp3D(:,:,i_z) = Recon_cl;
%                     
%                     
%                     %                     tic
%                     figure(12);
%                     imagesc(Recon_bp3D(:,:,i_z)); axis xy image ; colormap(bone);colorbar;
%                     pause(0.1);
%                     %                   toc
%                     %
%                     clear S
%                     
%                     
%                     
%                 end
%                 toc
%                 
%                
% 
%  return
% %%
% F_D_beamform=0.5;
% %
% zz_observation=-1.5e-3:(2*c/fs):-0.5e-3;
% 
% R_tomo=18.99e-3;
% Recon_posi.R=R_tomo;
% 
% maxi = 128 * size( signal_3D_array_full, 3 ) ;
% maxih = 65000;  % round( maxi/2 ) ;
% if maxih<=maxi
%     mm=[65000:65000:maxi maxi]
%     
% else
%     mm=maxi;
% end
% 
% r_posi=  sqrt(( (Scan_para.Y_real(1:length(Scan_para.Y))*1e-3)'-Scan_para.y_center).^2+Recon_posi.R^2);
% angle_posi = sign((Scan_para.Y_real(1:length(Scan_para.Y))*1e-3)').*acos( Recon_posi.R./ r_posi ) ;
% Recon_posi.X=cos( angle_posi+Scan_para.Phi_posi_real(1:length(Scan_para.Phi))'*pi/180).* r_posi;
% Recon_posi.Y=sin( angle_posi+Scan_para.Phi_posi_real(1:length(Scan_para.Phi))'*pi/180).* r_posi;
% 
% 
% %%% backprojection
% image_width=Scan_para.N_posi*Scan_para.Step_posi+2e-3;
% n=round(image_width/(c/fs));
% image_width=n*(c/fs);
% 
% 
% % 3D projection
% Recon_bp3D=zeros([n,n,length(zz_observation)]);
% 
% 
% pos_sensor(:,1,:)=ones(128,1)*(Recon_posi.X)';
% pos_sensor(:,2,:)=ones(128,1)*(Recon_posi.Y)';
% pos_sensor = single( pos_sensor ) ;
% P = zeros(  maxi, 3, 'single' ) ;
% 
% 
% for i_z=1: length(zz_observation)
%     i_z,
%     
%     tic;
%     [ Signal_3D_grow_aper_full]=growing_apperture_signals120516(F_D_beamform,zz_observation(i_z),t,c,signal_3D_array_full);
%     Signal_3D_grow_aper_full=single(Signal_3D_grow_aper_full);
%     pos_sensor(:,3,:) = single( (linspace(-(63*0.3+0.15)*1e-3,(63*0.3+0.15)*1e-3,128)-zz_observation(i_z))'*ones(1,length(Recon_posi.X)) ) ;
%     for ii=1: length(Recon_posi.X)
%         P((1:128)+(ii-1)*128,:) = pos_sensor(:,:,ii);
%     end
%     
%     S = reshape( squeeze( Signal_3D_grow_aper_full ), size( Signal_3D_grow_aper_full, 1 ), 128 * size( Signal_3D_grow_aper_full, 3 ) ) ;
%     
%     clear Signal_3D_grow_aper_full
%     
%     Recon_cl = backprojects3d_cl( S( :, 1 : mm(1) ), n, P( 1 : mm(1), : ), c, 1, t', fs, image_width ) ;
%     
%     if length(mm)>2
%         for i=2:length(mm)
%             Recon_cl = Recon_cl + backprojects3d_cl( S( :, mm(i-1)+1: mm(i) ), n, P( mm(i-1)+1: mm(i), : ), c, 1, t', fs, image_width ) ;
%         end
%         
%     end
%     
%     Recon_bp3D(:,:,i_z) = Recon_cl;
%     
%     clear S
%     
%     toc
%     
% end
% 
% save Recon_micros_2 Recon_bp3D n image_width F_D_beamform zz_observation c R_tomo
% 
% 
% 
% return
% 
% 
% 
% 
% %%
% limits=linspace(-image_width/2,image_width/2,n);
% figure
% for i_z=1: length(zz_observation)
%     i_z,
%     %figure
%     imagesc(limits*1e3,limits*1e3,Recon_bp3D(:,:,i_z));
%     axis xy image
%     caxis([min(Recon_bp3D(:)) max(Recon_bp3D(:))])
%     colormap(bone);colorbar;
%     grid
%     title(num2str(zz_observation(i_z)*1e3))
%     %      hold on;
%     %      for ii=1:length(n_1posi)
%     %      plot([limits(1) limits(end)]*1e3,[1 1]*y_sensor(ii)*1e3,'*m--')
%     %      plot([1 1]*y_sensor(ii)*1e3,[limits(1) limits(end)]*1e3,'*m--')
%     %      plot(y_sensor(ii).*1e3*cos(Phi),y_sensor(ii).*1e3*sin(Phi),'.m')
%     %
%     %      end
%     %      hold off
%     
%     pause%(0.1);
%     
% end
% %%
% %%
% limits=linspace(-image_width/2,image_width/2,n);
% figure
% for i_z=1: length(zz_observation)
%     i_z,
%     %figure
%     imagesc(Recon_bp3D(:,:,i_z));
%     axis xy image
%     %caxis([min(Recon_bp3D(:)) max(Recon_bp3D(:))])
%     colormap(bone);colorbar;
%     grid
%     title(num2str(zz_observation(i_z)*1e3))
%     %      hold on;
%     %      for ii=1:length(n_1posi)
%     %      plot([limits(1) limits(end)]*1e3,[1 1]*y_sensor(ii)*1e3,'*m--')
%     %      plot([1 1]*y_sensor(ii)*1e3,[limits(1) limits(end)]*1e3,'*m--')
%     %      plot(y_sensor(ii).*1e3*cos(Phi),y_sensor(ii).*1e3*sin(Phi),'.m')
%     %
%     %      end
%     %      hold off
%     
%     pause%(0.1);
%     
% end
% 
% %%
% 
% figure; isosurface(limits*1e3,limits*1e3,zz_observation*1e3,Recon_bp3D/max(Recon_bp3D(:)),0.6)
% 
% %      hold on;
% %      for ii=1:length(n_1posi)
% % plot3(y_sensor(ii).*1e3*cos(Phi),y_sensor(ii).*1e3*sin(Phi),zeros(size(Phi)),'.m')
% %      end
% axis equal
% xlim([limits(1) limits(end)]*1e3)
% ylim([limits(1) limits(end)]*1e3)
% zlim([zz_observation(1) zz_observation(end)]*1e3)
% 
% grid
