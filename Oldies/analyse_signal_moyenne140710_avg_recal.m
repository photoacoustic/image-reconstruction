
clear all
%close all
% clc;
 addpath('C:\Users\Tobias\Desktop\Thomas\Juin 2014\superres')

%%
load('D:\Thomas_Data\Aout 2014\50um_PSF_2_140827.mat','PD_sig','ttl_sig','trig_out_aix','PA_sig','n_avg','n_it')
% PA_sig2=PA_sig(:,:,:,1:85);
% PD_sig2=PD_sig(:,1:85);
% trig_out_aix2=trig_out_aix(:,1:85);
% ttl_sig2=ttl_sig(:,1:85);
% load('D:\Thomas_Data\Juillet 2014\100um200um_phantom2_rotating_1to57_140730.mat','PD_sig','ttl_sig','trig_out_aix','PA_sig','n_avg','n_it')
% PA_sig=cat(4,PA_sig2,PA_sig(:,:,:,1:57));
% PD_sig=cat(2,PD_sig2,PD_sig(:,1:57));
% trig_out_aix=cat(2,trig_out_aix2,trig_out_aix(:,1:57));
% ttl_sig=cat(2,ttl_sig2,ttl_sig(:,1:57));
% n_it=85+57;
% clear PA_sig2 PD_sig2 trig_out_aix2 ttl_sig2 
f_oscillo = 1e9;
%%

n= length(trig_out_aix)/(n_avg);

trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);

 figure; plot(ttl_sig(:,1,1)); hold all; plot(trig_out_aix(:,1,1))

 Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));
 
 for kk=1:size(ttl_sig,3)
 for k=1:size(ttl_sig,2)
     
 AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
 
I=find(AAlign==max(AAlign));
Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
 end
 end
  
figure; plot(ttl_sig(:,1,1)); hold all; plot(trig_out_aix((Dec_trig(1,1)+1):end,1,1))

%  figure; plot(Dec_trig*60e6/f_oscillo); hold all; plot(round(Dec_trig*60e6/f_oscillo),'x')
% % 
%  figure; hist(Dec_trig*60e6/f_oscillo)
%  
%%
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));

PD_sig =reshape(PD_sig, n, n_avg,n_it);

 for kk=1:size(ttl_sig,3)
 for k=1:size(ttl_sig,2)
nrj_pulse(k,kk)=sum(PD_sig(1020+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));

sig_photo(:,k,kk)=PD_sig(1020+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk));
 end
 end
 
 
figure; imagesc(sig_photo(:,:,1));

%%
 Align=double(squeeze(sig_photo));
%figure; imagesc(Align)
ref=Align(:,1);
 for kk=1:size(Align,3)
 for k=1:size(Align,2)

AAlign=xcorr(Align(:,k,kk),ref,'coeff');
I=find(AAlign==max(AAlign));
Dec_pulse(k,kk)=I-size(Align,1);
 end
 end

Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
% figure; plot(Dec_pulse(:));
% 
% figure; plot(round(Dec_pulse*60e6/f_pulse));

clear Align sig_photo
%%


pitch=0.33*1e-3;
 
     PA_sig1=PA_sig(151:(end-350),:,:,:);
     
     clear PA_sig
     
     %%
     
image_width = 10e-3%%NbElemts*pitch;
offset_width=0%-0.7e-3;
image_depth =8e-3;
offset_depth =20e-3;
     
     
     
interpfact=1;
c= 1476;     
fs=60e6*interpfact;
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/(fs/interpfact)));% number of pixel on the other side
n=m;
     
     Bp_image=zeros(m, n,size(PA_sig1,4),'single');
     
      Dec2=((Dec_trig)*fs/f_oscillo);
      %Dec2=round(-(Dec_trig)*fs/f_oscillo);
%   Dec2=max(-Dec2(:))+Dec2;
% ind_Dec=max(round((Dec_trig(:))*fs/f_oscillo))+150*interpfact +10*interpfact; % 10 pour le decalage
%      
     
for ik= 1:size(PA_sig1,4)
     
     

 if interpfact>1
PA_sig0=zeros([size(PA_sig1,1)*interpfact,size(PA_sig1,2),size(PA_sig1,3)],'single');
for i=1:size(PA_sig1,2)
for j=1:size(PA_sig1,3)
  
   PA_sig0(:,i,j)= single(interp(double(squeeze(PA_sig1(:,i,j,ik))),interpfact));
    
end
end


 else
     PA_sig0=single(squeeze(PA_sig1(:,:,:,ik)));
     
 end
%clear PA_sig1
%      
 % sampling rate
   
       
%%

[b a] =butter(3,[1e6 6e6]/(fs/2));


% 
% PA_sig0 = cat(1,zeros(ind_Dec,size(PA_sig1,2),size(PA_sig1,3)),PA_sig0); % rajout de 0 au d�but

%150*interpfact

 for k=1:size(PA_sig1 ,3);
     
 %PA_sig0 ((1:(end-Dec2(k,ik))),:,k) = PA_sig0((Dec2(k,ik)+1):end,:,k);
  PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,Dec2(k,ik)+150*interpfact);
PA_sig0 (:,:,k)= single(filtfilt(b,a,double(PA_sig0 (:,:,k))))./nrj_pulse(k,ik);
 end
% 

  PA_sig0=PA_sig0(:,:,3:end,:);
  % figure; imagesc(squeeze(PA_sig0(:,64,:,1)))
   
  % return
   
   %figure; plot(fs/f_oscillo*Dec_trig(:,1))
  % figure; plot(mean(squeeze(PA_sig0(:,64,:,2)),2))
%%

if ik==1
Ref=mean(double(squeeze(PA_sig0((600*interpfact):(1000*interpfact),32:3:96,:))),3);
%Ref=mean(double(squeeze(PA_sig0((600*interpfact):(1000*interpfact),32:3:96,1))),3);

% return
end


for i=1:size(PA_sig0,3)
% i,
% tic
cc = xcorr2(double(squeeze(PA_sig0((600*interpfact):(1000*interpfact),32:3:96,i))),Ref);
cc2=interp(cc(:),10);

[max_cc, imax] = max(abs(cc2));
[ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
corr_offset(i,:) = [ (ypeak-size(Ref,1)*10 +9) (xpeak-size(Ref,2)) ];

%return
% toc
end 
% 
% 
 Dec=corr_offset(:,1);
 if ik==1
 figure; plot(Dec)
 pause(1);
 end
%return

Dec(Dec>10)=0;
Dec(Dec<-10)=0;

 for k=1:size(PA_sig0 ,3);
     
 %PA_sig0 ((1:(end-Dec2(k,ik))),:,k) = PA_sig0((Dec2(k,ik)+1):end,:,k);
  PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,-Dec(k)/10);

 end
 
 
 
% figure; imagesc(squeeze(PA_sig0(:,64,:,1)))
 
 %return
% Dec0=Dec;
% % 
% % 
% Align=double(squeeze(PA_sig0((600*interpfact):(1000*interpfact),64,:)));
% %figure; imagesc(Align(:,:,1))
% if ik==1
% ref=Align(:,1);
% end
% 
% for k=1:size(PA_sig0,3);
% 
% AAlign=xcorr(Align(:,k),ref,'coeff');
% I=find(AAlign==max(AAlign));
% Dec(k)=I-size(Align,1);
% end
% clear Align
% 
% 
%  figure(61); plot(Dec(:)); hold all; plot(Dec0,'*-'); hold off
% 
%  pause;
% return

%%
% Dec=10*interpfact+Dec;
% 
%  for k=1:size(PA_sig0 ,3);
% PA_sig0((1:(end-Dec(k))),:,k) = PA_sig0((Dec(k)+1):end,:,k);
% 
%  end
%  


% figure; imagesc(squeeze(PA_sig0(:,64,:)))

%return


   %%
 
% 
% ind_Dec=round(mean((Dec_trig(:)+0*Dec_pulse(:))*fs/f_oscillo))+150;
% 
% PA_sig0 = cat(1,zeros(ind_Dec,size(PA_sig,2),size(PA_sig,3),size(PA_sig,4)),PA_sig0); % rajout de 0 au d�but
% 
% 
%    for kk=1:size(PA_sig ,4);
%  for k=1:size(PA_sig ,3);
% % PA_sig0 ((1:(end-Dec2(k))),:,k,kk) = single(PA_sig0((Dec2(k)+1):end,:,k,kk))./nrj_pulse(k,kk);
% % % 
% PA_sig0 (:,:,k,kk) = single(PA_sig0(:,:,k,kk))./nrj_pulse(k,kk);
% 
% 
%  end
% % 
%    end
%   PA_sig0=PA_sig0(:,:,3:end,:);
%    figure; imagesc(squeeze(PA_sig0(1:1000,64,:,1)))
%    figure; plot(fs/f_oscillo*Dec_trig(:,1))
%    figure; plot(mean(squeeze(PA_sig(:,64,:,2)),2))

%%
PA_sig0=squeeze(mean(PA_sig0,3));


%PA_sig0=PA_sig0((10*interpfact):end,:);

%%


NbElemts=size(PA_sig0,2);


%%


x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
y_array = - offset_depth*ones(1,NbElemts);
sizeT = size(PA_sig0,1)-1;

[N, M] = meshgrid( 1:n, 1:m );
xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;

clear N M SS

      for i = 1:length(x_array)
        
        x_sensor = x_array(i) ;
        y_sensor = y_array(i) ;
        
        x = x_sensor - xr;
        y = y_sensor - yr;
        s = sqrt( x.^2 + y.^2 );
        ss = round( s*fs/c +  1 );
        ss( ss > sizeT ) = sizeT;
        ss( ss <= 0 ) = 1;
        
        SS(:,:,i)=ss;
      end
    

%%


    
    ik,
  sigMat=squeeze(double(PA_sig0)/4);

    
    bp_image = zeros(m, n);
    
    tic
    for i = 1:length(x_array)
        
        %A0 = sigMat(:,i);
          A0 = -diff(sigMat(:,i));
        s1 = A0(squeeze(SS(:,:,i)));
        bp_image = bp_image + s1;
        
    end ;
    
    
    Bp_image(:,:,ik) =single(bp_image);
    
%     figure(1);
%     %subplot(2,ceil(length(diameter)/2),i_k);
%     imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,bp_image);
%     %colorbar;
%     pause(0.01)
%     axis equal tight
%     %xlabel('x (mm)')
%     %ylabel('y (mm)')
%     %caxis([-1 1]*30)
%     drawnow
%     title(num2str(k))
% %     
%     % figure(100+ k);
%     % %subplot(2,ceil(length(diameter)/2),i_k);
%     % imagesc(sigMat(150:end,29:end));
%      colorbar;
    % title(num2str(k))
    
    % i_k=i_k+1;
    %return
end

 save ('D:\Thomas_Data\Aout 2014\images_50um_PSF_2_140827.mat','nrj_pulse','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')

% return







%%
% A=nrj_pulse;
% %hist(A,100)
% 
% mA=median(A);
% figure; plot(abs((A-mA)/mA))
%  I=find( abs((A-mA)/mA) <(0.005));%I=find(A>(mA-std(A)) & A<(mA+std(A)));
%  
%  h=I;
h=1:10%0;%24;
Image_Mean=mean(Bp_image(:,:,h),3);
figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,Image_Mean);
%print -dpng opto_200um_mean_color_minus_1
colorbar


figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,abs(Image_Mean))
axis equal tight
colormap gray
title('mean')
colorbar
%print -dpng opto_200um_mean_1



Image_Var=var(Bp_image(:,:,h),[],3);
   
   axis equal tight
   figure; imagesc((xr(1,:)+offset_width)*1e3-offset_width,(yr(:,1)-y_array(1))*1e3,sqrt(Image_Var))
   colormap jet
   title('std')
   colorbar
   %print -dpng opto_200um_var_1


