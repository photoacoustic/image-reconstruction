clear all
close all
addpath(genpath('..\Data_PA\'))
%%
load('psfRecoveredRawData.mat')
% X is a (# of time samples)*128, should be positive
% X=double(PA_sig_cleaned);
% figure(1),
% subplot(131),imagesc(X)
% for k=1:size(X,2)
%     XX(:,k)=X(:,k).*(X(:,k)==max(X(:,k),[],1));
%     XXX(:,k)=double(X(:,k)==max(X(:,k),[],1));
% end
% subplot(132),imagesc(XX)
% subplot(133),imagesc(XXX)

f_oscillo = 1e9;
pitch=0.33*1e-3;
NbElemts=size(Xrec,2);
c= 1475;
image_width = 20e-3;
offset_width=0e-3;
image_depth =20e-3;
offset_depth = 15e-3;
interpfact=4;
fs=60e6*interpfact;
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/(fs)));% number of pixel on the other side
n=m;
Object=zeros(m,n);

%% Object reconstruction from X matrix

if interpfact>1 % never used so far
    X_interp=zeros([size(Xrec,1)*interpfact,size(Xrec,2)],'single');
    for i=1:size(Xrec,2)
        X_interp(:,i)= single(interp(double(squeeze(Xrec(:,i))),interpfact));
    end
end
Xrec=X_interp;
x_array =linspace(-((NbElemts-1)/2)*pitch,((NbElemts-1)/2)*pitch,NbElemts)-offset_width;
y_array = - offset_depth*ones(1,NbElemts);
sizeT = size(Xrec,1)-1;
[N, M] = meshgrid( 1:n, 1:m );
xr = ( N - (n+1)/2 ) * ( image_width/(n-1) ) ;
yr = ( (m+1)/2 - M ) * ( image_depth/(m-1) ) ;
clear N M SS

for i = 1:length(x_array)
    x_sensor = x_array(i) ;
    y_sensor = y_array(i) ;
    x = x_sensor - xr;
    y = y_sensor - yr;
    s = sqrt( x.^2 + y.^2 );
    ss = round( s*fs/c +  1 );
    ss( ss > sizeT ) = sizeT;
    ss( ss <= 0 ) = 1;
    SS(:,:,i)=ss;
end

for i =1:length(x_array)
%     A0=-diff(X(:,i));
    A0=Xrec(:,i);
    s1 = A0(squeeze(SS(:,:,i))); %create a matrix of size(SS), and s1(i,j)=X(SS(i,j))
    Object = Object+s1;
end

figure(3), imagesc(Object), axis xy image

% save ('data_leaf2015\BF_Phblueleaf_rot_2015520_dns_6MHz.mat','nrj_pulse','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')

