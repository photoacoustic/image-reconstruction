clear all
close all

addpath(genpath('..\Data_PA\'))
%%
load('Ph3last_static_2015421.mat')
f_oscillo = 1e9;
pitch=0.33*1e-3;
NbElemts=size(PA_sig,2);

%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement
% de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg);
trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);
figure(1),plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix(:,1,1))
title('shift between trig out laser and trig out ultrasound machine')
Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));

for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
        I=find(AAlign==max(AAlign));
        Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:)); % a checker

figure(2), subplot(121), plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix((Dec_trig(1)+1):end,1,1))
subplot(122), imagesc(Dec_trig),

%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));
PD_sig =reshape(PD_sig, n, n_avg,n_it);

center=1637;
for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        nrj_pulse(k,kk)=sum(PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));
        sig_photo(:,k,kk)=PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk));
    end
end

figure(3); imagesc(sig_photo(:,:,1));

%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for kk=1:size(Align,3)
    for k=1:size(Align,2)
        AAlign=xcorr(Align(:,k,kk),ref,'coeff');
        I=find(AAlign==max(AAlign));
        Dec_pulse(k,kk)=I-size(Align,1);
    end
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo

%% cut beginning and end of signal because saturated signals because of the machine
PA_sig1=PA_sig(151:(end-350),:,:,:);
% clear PA_sig


%%
c= 1475;
image_width = 20e-3;%NbElemts*pitch;
offset_width=0e-3;
image_depth =20e-3;
offset_depth = 15e-3;
interpfact=1;
t0=0; % not used here, a voir
fs=60e6*interpfact;
%n=round(image_width/pitch *4) ; % number of pixel on one side
m= round(image_depth/(c/(fs/interpfact)));% number of pixel on the other side
n=m;
Bp_image=zeros(m, n,size(PA_sig1,4),'single');
Dec2=((Dec_trig)*fs/f_oscillo);

%% signals processing 
for ik= 1:size(PA_sig1,4) % nb of uncorrelated speckle illuminations
    
    disp(['processing data cleaning on ' num2str(ik) 'th speckle realisation'])
    
    if interpfact>1 % never used so far
        PA_sig0=zeros([size(PA_sig1,1)*interpfact,size(PA_sig1,2),size(PA_sig1,3)],'single');
        for i=1:size(PA_sig1,2)
            for j=1:size(PA_sig1,3)
                PA_sig0(:,i,j)= single(interp(double(squeeze(PA_sig1(:,i,j,ik))),interpfact));
            end
        end
    else
        PA_sig0=single(squeeze(PA_sig1(:,:,:,ik)));
    end
    
    %%  Normalisation + Recalage des signaux (pour le delay Dec2 entre ttl et trig_aix)
    % + Filtrage passe-bas (standard) des signaux
    [b a] =butter(3,[1e6 6e6]/(fs/2));
    for k=1:size(PA_sig0 ,3);
        PA_sig0 (:,:,k) = DelaySignal(PA_sig0(:,:,k),1,Dec2(k,ik)+150*interpfact);
        PA_sig0 (:,:,k)= single(filtfilt(b,a,double(PA_sig0 (:,:,k))))./nrj_pulse(k,ik);
        % check for bursts degueulasses
        %bd(k,ik)=max(PA_sig0(:,64,k));
    end
    PA_sig0=PA_sig0(:,:,2:end,:); % why? --> car sinon burst degueu on first signals (voir bd)
    %figure; imagesc(squeeze(PA_sig0(:,64,:,1)))
    %figure(8), imagesc(bd(1:end,:))
    
    
    %% on coupe eventuellement sur la voie 86 car un peu degueu
    figure(4), subplot(121), imagesc(PA_sig0(:,:,1))
    PA_sig00=PA_sig0;
    for k=1:size(PA_sig0 ,3);
        for kk=1:size(PA_sig0 ,4);
            PA_sig00(150:550,86,k,kk)=mean(PA_sig0(150:550,[85 87],k,kk),2);
        end
    end
    PA_sig0=PA_sig00;
    clear PA_sig00
    subplot(122), imagesc(PA_sig0(:,:,1))
    
    %% methode manu: on enl�ve la moyenne des signaux pour couper les pics
    % � bricoler si beaucoup de signaux quasi "horizontaux"??
    figure(5), subplot(121), imagesc(PA_sig0(:,:,1))
    for k=1:size(PA_sig0,3)
        for j=1:size(PA_sig0,2)
            PA_sig2(:,j,k)= PA_sig0(:,j,k)-squeeze(mean(PA_sig0(:,[1:42,87:128],k),2)); % a checker
        end
    end
    subplot(122), imagesc(PA_sig2(:,:,1))
    
    %% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
    % (Dec_pulse n'est pas utilis� finalement...?)
    if ik==1
        Ref=mean(double(squeeze(PA_sig2((500*interpfact):(900*interpfact),32:3:96,:))),3);
    end
    for i=1:size(PA_sig2,3)
        cc = xcorr2(double(squeeze(PA_sig2((500*interpfact):(900*interpfact),32:3:96,i))),Ref);
        cc2=interp(cc(:),10);
        [max_cc, imax] = max(abs(cc2));
        [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
        corr_offset(i,:) = [ (ypeak-size(Ref,1)*10 +9) (xpeak-size(Ref,2)) ];
    end
    Dec=corr_offset(:,1);
    if ik==1
        figure(6); plot(Dec), title('jitter calculated by sigs cross corr, in tenths of pixel')
    end
    for k=1:size(PA_sig2 ,3);
        PA_sig2 (:,:,k) = DelaySignal(PA_sig2(:,:,k),1,-Dec(k)/10);
    end
    
    %% average 25 (24) signals with same speckle illumination
    PA_sig2=squeeze(mean(PA_sig2,3));
    PA_sig_cleaned(:,:,ik)=PA_sig2;
end

% PA_sig_cleaned=mean(PA_sig_cleaned,3); % only for PSF
save ('data_sample2_optica\Ph3last_static_2015421_6MHz_cleanedAndAvg.mat','PA_sig_cleaned','NbElemts','pitch','offset_width',...
    'offset_depth','n','m','image_width','image_depth','fs','c')






