clear all

%% Acquisition parameters
t0_acq=12; %us
fs=40; %MHz
acquisition_length=14; %us

%
N_acq=round(acquisition_length*fs);
acq_timebase_us=(t0_acq:1/fs:t0_acq+(N_acq-1)/fs)';


%% Signal definition
f0=5;
BW=0.80;
t0_pulse=0; % center time of the pulse in the acquisition timebase. If >0, means that the pulse central time is AFTER the acquisiton trigger (t=0 by def)

[yi,yq] = gauspuls((acq_timebase_us-t0_pulse),f0,BW);
%plot(acq_timebase_us,yq)


%% Transducer
load('Vermon_L7-4.mat')

%load('Imasonic_3D_probe_2018.mat')
receivers.N=length(receivers.pos_mm.x);

%% sources

sources.pos_mm.x=[0 -1 -1 1 1];
sources.pos_mm.y=[0 0 0 0 0];
sources.pos_mm.z=[25 24 26 24 26];

sources.N=length(sources.pos_mm.x);


%% RF-data structure
RF=zeros(length(acq_timebase_us),receivers.N);



%% medium speed of sound
c=1.5;% mm/us

for k=1:receivers.N
    for n=1:sources.N
        retard=1/c.*sqrt((receivers.pos_mm.x(k)-sources.pos_mm.x(n)).^2+(receivers.pos_mm.y(k)-sources.pos_mm.y(n)).^2+(receivers.pos_mm.z(k)-sources.pos_mm.z(n)).^2);
        [yi,yq] = gauspuls((acq_timebase_us-t0_pulse-retard),f0,BW);
        RF(:,k)=RF(:,k)+yi+1i*yq;
    end
end

if 1
    figure(1)
    clf
    imagesc((1:receivers.N),acq_timebase_us,real(RF))
    colormap(gray)
end


%sources.pos
%%
eval(sprintf('save RFData c receivers RF t0_pulse fs t0_acq;'))
%eval(sprintf('save 20181129_SimulationSonde3D_sample%i receivers RF acq_timebase_us;',sample))
%clear all


