function LUT=BF_compute_LUT(beamforming)

% LUT=BF_computeLUT(beamforming)
%
% LUT has cell fields LUT.delays_pt and LUT.apodization_coeff
%
% beamforming has fields:
%
% beamforming.mode  % 'photoacoustic', 'flat'
% beamforming.apodization % 'hamming', 'none'
% beamforming.c
% beamforming.sampling_freq_MHz
% beamforming.t0_pulse_pt
% beamforming.acquisition_delay_pt
% beamforming.acquisition_duration_pt
% beamforming.F_nb
% beamforming.receivers  % position as column vectors...Historical
% beamforming.x_pixel_mm % position as line vectors.... Historical...
% beamforming.y_pixel_mm
% beamforming.z_pixel_mm


% je corrige l'endroit ou sont arrondi les d�lais...
% j'ajoute une 1/2 largeur d'�l�ment pour positionnnement correct
% x = 0 mm correspond au centre de la barrette. J'ai recentr�e la
% focalisation dynamique au passage pour que �a marche
%
% Emmanuel Bossy, 17/01/2020


c=beamforming.c;
fs=beamforming.sampling_freq_MHz;
receivers=beamforming.receivers;

if ((size(beamforming.x_pixel_mm,2)<size(beamforming.x_pixel_mm,1))|...
        (size(beamforming.y_pixel_mm,2)<size(beamforming.y_pixel_mm,1))|...
        (size(beamforming.z_pixel_mm,2)<size(beamforming.z_pixel_mm,1)))
error('grid pixels should be line vectors')
end
if ((size(receivers.pos_mm.x,1)<size(receivers.pos_mm.x,2))|...
        (size(receivers.pos_mm.y,1)<size(receivers.pos_mm.y,2))|...
        (size(receivers.pos_mm.z,1)<size(receivers.pos_mm.z,2)))
error('transducers positions should be column vectors')
end


LUT.delays_pt=cell(size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2));

for x_idx = 1:size(beamforming.x_pixel_mm,2)
    for y_idx = 1:size(beamforming.y_pixel_mm,2)
        for z_idx = 1:size(beamforming.z_pixel_mm,2)
            
            total_distance_mm=sqrt((receivers.pos_mm.x'-beamforming.x_pixel_mm(x_idx)).^2+...
                (receivers.pos_mm.y'-beamforming.y_pixel_mm(y_idx)).^2+...
                (receivers.pos_mm.z'-beamforming.z_pixel_mm(z_idx)).^2);
            
            xy_distance_mm=sqrt((receivers.pos_mm.x'-beamforming.x_pixel_mm(x_idx)).^2+...
                (receivers.pos_mm.y'-beamforming.y_pixel_mm(y_idx)).^2);
            
            z_distance_mm=abs(receivers.pos_mm.z'-beamforming.z_pixel_mm(z_idx));
            
            delays_us = 1/c*total_distance_mm...
                -beamforming.acquisition_delay_pt/fs...
                +beamforming.t0_pulse_pt/fs;
            
            %% delays computation
            
            switch beamforming.mode
                case 'photoacoustic'
                    LUT.type='photoacoustic';
                case 'flat'
                    LUT.type='flat';
                    delays_us=delays_us+1/c*z_distance_mm;
                case 'US_flat'
                    LUT.type='flat';
                    delays_us=delays_us+1/c*z_distance_mm;
                otherwise
                    error('pb mode');
            end
            
            delays_pt=round(delays_us*fs);
            
            
            %% NA limitation (NA=1 takes all the elements)
            local_NA=xy_distance_mm./total_distance_mm;
            max_local_NA=max(local_NA);
            
            NA_mask=(local_NA<beamforming.NA);
            
            apodization_coeff=1+0*NA_mask;
            
            if beamforming.apodization
                apod_NA=min(beamforming.NA,max_local_NA);
                apodization_coeff=0.54+0.46*cos(local_NA/apod_NA*pi);
            end
            
            apodization_coeff=apodization_coeff.*NA_mask;

            
            idx_active_points= (delays_pt>=1) & (delays_pt<=beamforming.acquisition_duration_pt) & (apodization_coeff>0);
            delays_pt = delays_pt + (0:beamforming.acquisition_duration_pt:(beamforming.number_of_elements-1)*beamforming.acquisition_duration_pt);
            LUT.delays_pt{x_idx,y_idx,z_idx}=delays_pt(idx_active_points);
            LUT.apodization_coeff{x_idx,y_idx,z_idx}=apodization_coeff(idx_active_points);
        end
    end
end

