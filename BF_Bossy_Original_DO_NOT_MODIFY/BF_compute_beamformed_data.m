function bfdata = BF_compute_beamformed_data(data,LUT)

% bfdata = BF_compute_beamformed_data(data,LUT)
%
% bfdata is RF-beamformed data (not enveloppe)
% data.RF may be complex data (modulated...)
%
% No temporal interpolation, closest values are used.
%
% Emmanuel Bossy, 17/01/2020

nx = size(LUT.delays_pt,1);
ny = size(LUT.delays_pt,2);
nz = size(LUT.delays_pt,3);

bfdata = zeros(nx,ny,nz);
for ix=1:nx
    for iy=1:ny
        for iz=1:nz
            bfdata(ix,iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}).*LUT.apodization_coeff{ix,iy,iz});
        end
    end
end


