clear all

load('RFData.mat')
load('Beamforming.mat');

tic 
BF=BF_compute_beamformed_data((RF),LUT);
toc

x=beamforming.x_pixel_mm;
z=beamforming.z_pixel_mm;
imagesc(x,z,squeeze(abs(BF))');
set(gca,'DataAspectRatio',[1 1 1]);
