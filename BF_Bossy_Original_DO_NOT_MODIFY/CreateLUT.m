load('RFData.mat')

%%

beamforming.receivers=receivers;

beamforming.mode='photoacoustic';
beamforming.c=c; % mm/us
beamforming.sampling_freq_MHz=fs;
beamforming.t0_pulse_pt=t0_pulse*fs;
beamforming.acquisition_delay_pt=t0_acq*fs;
beamforming.acquisition_duration_pt=size(RF,1);
beamforming.number_of_elements=size(RF,2);



%%

bf_step_mm=0.05;

xc_mm=0;
yc_mm=0;
zc_mm=25;

FOV_x_mm=6;
FOV_y_mm=0;
FOV_z_mm=8;

beamforming.x_pixel_mm=(xc_mm-FOV_x_mm/2:bf_step_mm:xc_mm+FOV_x_mm/2);
beamforming.y_pixel_mm=(yc_mm-FOV_y_mm/2:bf_step_mm:yc_mm+FOV_y_mm/2);
beamforming.z_pixel_mm=(zc_mm-FOV_z_mm/2:bf_step_mm:zc_mm+FOV_z_mm/2);


beamforming.NA=1;
beamforming.apodization=0;

%
tic
LUT=BF_compute_LUT(beamforming);
toc

save Beamforming beamforming LUT


%%
%