# Meeting Notes - Emmanuel Bossy -Photoacoustic Reconstruction algorithm



t0_acq = time origin

t0_pulse = pulse central time (positive -> pulse after trigger)

-> Useful delay : start of time base, relative to the pulse



Generate signal: dirac(r-t)  => just a 1D delay (not accounting for 1/r attenuation or derivative)

System units : mm/us



LUT = Look-Up Table -> Time delay to look up for during pixel reconstruction

Coordinates: x, y, z



LUT -> each point -> transducers everywhere in space. Rectangular beamforming grid.

1 point -> 1 trajectory -> 128 delays



Beamforming space => sensor origin

Trajectory -> shape + position



Pay attention to relative vs absolute times. What really matters is the time of flight.



Generated signals are complex. Hilbert transform is useful if signals are real.



Low aperture -> Hilbert transform -> In time domain -> Complex beamforming



NA = n sin(theta), where theta is the half angle. The NA defines if a sensor is taken into account or not, and if a point is taken or not for reconstruction.



Apodization: maximum in the center, decreases towards NA edges up to 0. -> Arccos or Hanning windows



Interpolating slows down things -> Here the nearest point is taken

Apodization is done on the largest base. So, a constant PSF, no constant SNR.



Delays in time base of aligned signals -> everything is referenced by the first line
