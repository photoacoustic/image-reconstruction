%{
    Reconstruction of pressure source using data Fabry-Perot scanning
%}

clearvars -except username
folder=makeMyDay(username);

%% Load data from a single h5 file
try
    [file,path] = uigetfile([folder '/*.h5'],'Select data file');
catch
    [file,path] = uigetfile('*.h5','Select data file');
end

if exist('data_mean')==1 % save time if data already in workspace
    disp('Using data from workspace')
    data=data_mean;
    clear data_mean
else
    [~,file,ext] = fileparts(file);
    % Load the associated parameters file
    load([path filesep file '_params.mat'])
    
    % Get info about the h5 file and load the mean and std datasets
    info = h5info([path file ext]);
    data = h5read([path file ext],'/data/mean');
end
data = raw2volts(data,sysinfo); 

layers=split(path,filesep);
day=layers{end-2};
hour=layers{end-1};
processing_folder = [folder filesep hour 'm_processing'];
mkdir(processing_folder)

%%
c= 1475; % sound speed in m/s

samplingRate = acqInfo.SampleRate; % in Hz
pitch=scan.dx; % in m
nx=length(scan.x);
ny=length(scan.y);

imWidthX = scan.FOV; % in m
imWidthY = scan.FOV; % in m
offsetX=0e-3; % in m
offsetY=0e-3; % in m
imDepth =2e-3; % in m
offsetZ = 0e-3; % in m
Nx=round(imWidthX/pitch *4) ; % number of pixels in final image in X direction
Ny=round(imWidthY/pitch *4) ; % number of pixels in final image in Y direction
dz=c/samplingRate;
Nz=round(imDepth/dz); % number of pixels along depth

PAsig=squeeze(data(:,2,:,:));
PAimg=zeros(Nz,Nx,Ny,'single');

%% Bandpass filtering
[b,a] = butter(3,[1e6 50e6]/(samplingRate/2));
PAsig= single(filtfilt(b,a,double(PAsig))); %./nrj_pulse(k,ik); --> normalisation for later

%% beamforming
x = linspace(-.5*(Nx-1)*pitch,.5*(Nx-1)*pitch,Nx)-offsetX;
y = linspace(-.5*(Ny-1)*pitch,.5*(Ny-1)*pitch,Ny)-offsetY;
zSensor = -offsetZ;
T = size(PAsig,1)-1;
[X,Y,Z] = meshgrid(1:Nz);
xr = ( X - (Nz+1)/2 ) * ( imWidthX/(Nz-1) ) ;
% yr = ( Y - (Nz+1)/2 ) * ( imWidthY/(Nz-1) ) ;
zr = ( (Nz+1)/2 - Z ) * ( imDepth/(Nz-1) ) ;
clear X Y Z

for kx = 1:length(x)
    for ky = 1:length(y)
        tic
        xSensor = x(kx) ;
        ySensor = y(ky) ;
        s = sqrt((xr-xSensor).^2 + (xr-ySensor).^2 + (zr-zSensor).^2); % distance matrix
        ss = round( s*samplingRate/c +  1 ); % time-of-flight matrix in samples
        ss( ss > T ) = T; % discard pixels further than total signal duration
        ss( ss <= 0 ) = 1;
        SS(:,:,:,kx,ky)=ss;
        toc
    end
end
return

sigMat=squeeze(double(PAsig));
bp_image= zeros(Nz,n);

for kx =1:length(xArray)
    A0 = -diff(sigMat(:,kx));
    s1 = A0(squeeze(SS(:,:,kx))); % this creates a matrix of size(SS), and s1(i,j)=A0(SS(i,j,kx))
    bp_image= bp_image+s1;
end

PAimg(:,:,ik) =single(bp_image);
figure(7), imagesc(bp_image), axis xy image


% save ('data_leaf2015\BF_Phblueleaf_static_2015520_dns_6MHz.mat','nrj_pulse','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')
return

%% check images, then mean and variance images
h=1:100;

Image_Mean=mean(PAimg(:,:,h),3);
Image_Var=var(PAimg(:,:,h),[],3);
figure(9),
subplot(121),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean),
colorbar, axis image,title('mean')
subplot(122), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var),
axis image,title('std'),colorbar
colormap jet

%%
% Bp_image_new=Bp_image;
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum.mat','Bp_image')
% Bp_image_old=Bp_image;

for k=h
    figure(666),
    subplot(221),imagesc(Bp_image_old(300:500,300:500,k)), axis xy image, title(['old ' num2str(k)]), colorbar
    subplot(222),imagesc(Bp_image_new(300:500,300:500,k)), axis xy image, title(['new ' num2str(k)]), colorbar
    if k>1
        subplot(223),imagesc(Bp_image_old(300:500,300:500,k)-Bp_image_old(300:500,300:500,k-1)),
        axis xy image, title('old(k)-old(k-1)'), colorbar
    end
    subplot(224),imagesc(Bp_image_new(300:500,300:500,k)-Bp_image_old(300:500,300:500,k)),
    axis xy image, title('new(k)-old(k)'), colorbar
    %     coef(k)=mean2(Bp_image_new(100:300,100:300,k)-Bp_image_old(100:300,100:300,k));
    pause%(0.5)
end
figure(12000)
plot(coef)
%%
% Bp_image=Bp_image_new;

% Image_Mean_old=mean(Bp_image_old(:,:,h),3);
% Image_Var_old=var(Bp_image_old(:,:,h),[],3);
% Image_Mean_new=mean(Bp_image_new(:,:,h),3);
% Image_Var_new=var(Bp_image_new(:,:,h),[],3);

figure(9),
subplot(211),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean_old),
colorbar, axis image,title('old mean')
subplot(212), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var_old),
axis image,title('old std'),colorbar
colormap jet

figure(10),
subplot(211),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean_new),
colorbar, axis image,title('new mean')
subplot(212), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var_new),
axis image,title('new std'),colorbar
colormap jet

return

%%%%%%%% JUNK or to be used later %%%%%%%%%%%%%%%
%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement
% de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg);
trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);
figure(1),plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix(:,1,1))
title('shift between trig out laser and trig out ultrasound machine')
Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));

for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
        I=find(AAlign==max(AAlign));
        Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:)); % a checker

figure(2), subplot(121), plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix((Dec_trig(1)+1):end,1,1))
subplot(122), imagesc(Dec_trig),




%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));
PD_sig =reshape(PD_sig, n, n_avg,n_it);

center=1637;
for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        nrj_pulse(k,kk)=sum(PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));
        sig_photo(:,k,kk)=PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk));
    end
end

figure(3); imagesc(sig_photo(:,:,1));
%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for kk=1:size(Align,3)
    for k=1:size(Align,2)
        AAlign=xcorr(Align(:,k,kk),ref,'coeff');
        I=find(AAlign==max(AAlign));
        Dec_pulse(k,kk)=I-size(Align,1);
    end
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo

%% cut beginning and end of signal because saturated signals because of the machine
PAsig1=PAsig(151:(end-350),:,:,:);
% clear PAsig

%% interpolation
interpfact=1; %

dz=c/(samplingRate*interpfact); % check this one

if interpfact>1 % never used so far
    PAsig=zeros([size(PAsig1,1)*interpfact,size(PAsig1,2),size(PAsig1,3)],'single');
    for kx=1:size(PAsig1,2)
        for j=1:size(PAsig1,3)
            PAsig(:,kx,j)= single(interp(double(squeeze(PAsig1(:,kx,j,ik))),interpfact));
        end
    end
else
    PAsig=single(squeeze(PAsig1(:,:,:,ik)));
end

%% delay signal 
PAsig (:,kx,ky) = DelaySignal(PAsig(:,:,k),1,Dec2(k,ik)+150*interpfact);

%% on coupe eventuellement sur la voie 86 car un peu degueu
figure(4), subplot(121), imagesc(PAsig(:,:,1))
PAsig00=PAsig;
for k=1:size(PAsig ,3);
    for kk=1:size(PAsig ,4);
        PAsig00(150:550,86,k,kk)=mean(PAsig(150:550,[85 87],k,kk),2);
    end
end
PAsig=PAsig00;
clear PAsig00
subplot(122), imagesc(PAsig(:,:,1))

%% methode manu: on enl�ve la moyenne des signaux pour couper les pics
% � bricoler si beaucoup de signaux quasi "horizontaux"??
figure(5), subplot(121), imagesc(PAsig(:,:,1))
for k=1:size(PAsig,3)
    for j=1:size(PAsig,2)
        PAsig2(:,j,k)= PAsig(:,j,k)-squeeze(mean(PAsig(:,[1:42,87:128],k),2)); % a checker
    end
end
subplot(122), imagesc(PAsig2(:,:,1))

%% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
% (Dec_pulse n'est pas utilis� finalement...?)
if ik==1
    Ref=mean(double(squeeze(PAsig2((500*interpfact):(900*interpfact),32:3:96,:))),3);
end
for kx=1:size(PAsig2,3)
    cc = xcorr2(double(squeeze(PAsig2((500*interpfact):(900*interpfact),32:3:96,kx))),Ref);
    cc2=interp(cc(:),10);
    [max_cc, imax] = max(abs(cc2));
    [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
    corr_offset(kx,:) = [ (ypeak-size(Ref,1)*10 +9) (xpeak-size(Ref,2)) ];
end
Dec=corr_offset(:,1);
if ik==1
    figure(6); plot(Dec), title('jitter calculated by sigs cross corr, in tenths of pixel')
end
for k=1:size(PAsig2 ,3);
    PAsig2 (:,:,k) = DelaySignal(PAsig2(:,:,k),1,-Dec(k)/10);
end


%%%%%%%% FUNCTIONS %%%%%%%%%
function voltData = raw2volts(rawData,sysInfo)
    voltData = (sysInfo.SampleOffset - rawData)/sysInfo.SampleResolution;
end
