%{
    Reconstruction of pressure source using data Fabry-Perot scanning
%}
addpath(genpath('/Users/thomas/Desktop/ChLabGit/Photoacoustic/Rig/pulse-echo_measurement'))
clearvars -except username p_txy folder
% folder=makeMyDay(username);
folder='/Users/thomas/owncloud_Fresnel/Shared/ChLab/Data_temporary';
saveFlag=0;

<<<<<<< HEAD
%% Load data from a single h5 file
try
    [file,path] = uigetfile([folder '/*.h5'],'Select data file');
catch
    [file,path] = uigetfile('*.h5','Select data file');
end

if exist('data_mean')==1 % save time if data already in workspace
    disp('Using data from workspace')
    data=data_mean;
    clear data_mean
else
    [~,file,ext] = fileparts(file);
    % Load the associated parameters file
    load([path filesep file '_params.mat'])
    
    % Get info about the h5 file and load the mean and std datasets
    info = h5info([path file ext]);
%     data = h5read([path file ext],'/data/mean');
    data = h5read([path file ext],'/data/z1/f1');
end
% data = raw2volts(data,sysinfo);

layers=split(path,filesep);
day=layers{end-2};
hour=layers{end-1};
processing_folder=[fullfile(layers{1:end-4}) filesep username filesep day filesep hour '_processing'];
mkdir(processing_folder)
=======
%% load data
[RFdata,params,filepath] = loadScanFPFile(folder); % from scan_fp
>>>>>>> e58e1554490b43aeb086a3b19ef8ce3799c1c248

%%
c= 1475; % sound speed in m/s

samplingRate = params.gage.acqInfo.SampleRate; % in Hz
pitch=params.scan.dx; % in m
nx=length(params.scan.x);
ny=length(params.scan.y);
Npix=nx*ny;
Nt=size(RFdata.data.data,1);

imWidthX = params.scan.FOV; % in m
imWidthY = params.scan.FOV; % in m
offsetX=0e-3; % in m
offsetY=0e-3; % in m
imDepth =1e-3; % in m
offsetZ = 0e-3; % in m
Nx=round(imWidthX/pitch *4) ; % number of pixels in final image in X direction
Ny=round(imWidthY/pitch *4) ; % number of pixels in final image in Y direction
dz=c/samplingRate;
Nz=round(imDepth/dz); % number of pixels along depth

PAsig=squeeze(RFdata.data.data(:,2,:,:));
% % PAimg=zeros(Nz,Nx,Ny,'single');


%% Bandpass filtering
[b,a] = butter(3,[1e6 100e6]/(samplingRate/2));
PAsig2=double(single(filtfilt(b,a,double(PAsig)))); %./nrj_pulse(k,ik); --> normalisation for later
% see how to get rid of peaks at start and end of noisy sigs

%% Reconstruction
clear PAimg
% PAsig22 = padarray(PAsig2,size(PAsig2,1),0,'post');
PAsig3 = PAsig2;
PAsig3=reshape(PAsig3,Nt,nx,ny);

% beamforming using k-wave function kspacePlaneRecon
% see http://www.k-wave.org/documentation/example_pr_3D_fft_planar_sensor.php for details
% p_txy=PAsig3+p_txy;
p_txy=PAsig3;

% p_xyz = kspacePlaneRecon(p_tyz, dy, dz, dt, c, varargin) % mind the different definitions of xyz
c=1480;%linspace(1401,1550,5);
for kc=1:length(c)
    p_zxy = kspacePlaneRecon(p_txy, params.scan.dx, params.scan.dx,1/params.gage.acqInfo.SampleRate,c(kc), ...
        'DataOrder', 'tyz', 'PosCond', false, 'Plot', false,'Interp','linear');
    maxDepth=ceil((imDepth)/dz); %maxDepth=ceil((imDepth+us.distance)/dz);
    PAimg(:,:,:,kc)=p_zxy;
    figure(11), imagesc(mean(p_zxy(1:maxDepth,:,:),3)), daspect([dz params.scan.dx params.scan.dx])
    %         pause
end

PAimg(maxDepth+1:end,:,:,:,:)=[];

dexhot(PAimg,1,[dz params.scan.dx params.scan.dx])
colormap gray

if saveFlag
    clear data p_txy p_zxy PAsig PAsig2 PAsig3 s2x s2x_pix
    save([processing_folder filesep 'sum_img.mat'])
end

return

%% play with image
PAimg2=abs(hilbert(PAimg));
dexhot(PAimg2,1,[1 scan.dx/dz scan.dx/dz])

PAimg3=imgaussfilt3(PAimg2,1);
PAimg3=uint16(PAimg3*2^15/max(PAimg3(:)));

volumeViewer(permute(PAimg3,[2 3 1]),'ScaleFactors',[scan.dx/dz scan.dx/dz 1]) % use permute, because y is the default depth in the viewer


%% concatenate several images and launch volumeViewer
PAimgtot=[];
hours=['11h09m'; '11h30m'; '12h00m'];
for khour=1:3
    load([folder '_processing' filesep hours(khour,:) '_img.mat'])
    PAimgtot=cat(3,PAimgtot,PAimg);
    dexhot(permute(PAimg,[2 3 1]),1,[scan.dx/dz scan.dx/dz 1]), colormap gray % permute to use add2gif
end
return
dexhot(permute(PAimgtot,[1 3 2]),1,[1 scan.dx/dz scan.dx/dz]), colormap gray % permute to use add2gif
volumeViewer(permute(PAimgtot,[2 3 1]),'ScaleFactors',[scan.dx/dz scan.dx/dz 1]) % use permute, because y is the default depth in the viewer


%% beamforming using k-wave function kspaceFirstOrder3D
% see http://www.k-wave.org/documentation/example_pr_3D_tr_planar_sensor.php for details

% [sensor_data, mem_usage] = kspaceFirstOrder3D(kgrid, medium, source, sensor, varargin)
kgrid = kWaveGrid(scan.nx, scan.dx, scan.ny, scan.dx,size(p_txy,1),dz);
kgrid.dt=1/gage.acqInfo.SampleRate;
kgrid.Nt=size(p_txy,1);
medium.sound_speed=c*ones(scan.nx,scan.nx,size(p_txy,1)); % in m/s
medium.density = 1000 * ones(scan.nx,scan.nx,size(p_txy,1)); % in kg/m^3
source.p0=0;
sensor.time_reversal_boundary_data = permute(p_txy(:,:),[2 1]); % p_xyt, to match sensor requirements
sensor.mask=zeros(scan.nx,scan.nx,size(p_txy,1));
sensor.mask(:,:,1)=1;
p_zxy = kspaceFirstOrder3D(kgrid,medium,source,sensor);










return
%%
%%%%%%%%%%%%%%%%% JUNK %%%%%%%%%%%%%%%%%%
x = linspace(-.5*(Nx-1)*pitch,.5*(Nx-1)*pitch,Nx)-offsetX;
y = linspace(-.5*(Ny-1)*pitch,.5*(Ny-1)*pitch,Ny)-offsetY;
zSensor = -offsetZ;
T = size(PAsig,1)-1;
[X,Y,Z] = meshgrid(1:Nz);
xr = ( X - (Nz+1)/2 ) * ( imWidthX/(Nz-1) ) ;
% yr = ( Y - (Nz+1)/2 ) * ( imWidthY/(Nz-1) ) ;
zr = ( (Nz+1)/2 - Z ) * ( imDepth/(Nz-1) ) ;
clear X Y Z

for kx = 1:length(x)
    for ky = 1:length(y)
        tic
        xSensor = x(kx) ;
        ySensor = y(ky) ;
        s = sqrt((xr-xSensor).^2 + (xr-ySensor).^2 + (zr-zSensor).^2); % distance matrix
        ss = round( s*samplingRate/c +  1 ); % time-of-flight matrix in samples
        ss( ss > T ) = T; % discard pixels further than total signal duration
        ss( ss <= 0 ) = 1;
        SS(:,:,:,kx,ky)=ss;
        toc
    end
end
return

sigMat=squeeze(double(PAsig));
bp_image= zeros(Nz,n);

for kx =1:length(xArray)
    A0 = -diff(sigMat(:,kx));
    s1 = A0(squeeze(SS(:,:,kx))); % this creates a matrix of size(SS), and s1(i,j)=A0(SS(i,j,kx))
    bp_image= bp_image+s1;
end

PAimg(:,:,ik) =single(bp_image);
figure(7), imagesc(bp_image), axis xy image


% save ('data_leaf2015\BF_Phblueleaf_static_2015520_dns_6MHz.mat','nrj_pulse','Bp_image','xr', 'offset_width' ,'yr' ,'y_array' ,'-v7.3')
return

%% check images, then mean and variance images
h=1:100;

Image_Mean=mean(PAimg(:,:,h),3);
Image_Var=var(PAimg(:,:,h),[],3);
figure(9),
subplot(121),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean),
colorbar, axis image,title('mean')
subplot(122), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var),
axis image,title('std'),colorbar
colormap jet

%%
% Bp_image_new=Bp_image;
% load('C:\Users\Owner\Desktop\Data_PA\data_sample2_optica\BF_Ph3last_rot_2015421_delay&sum.mat','Bp_image')
% Bp_image_old=Bp_image;

for k=h
    figure(666),
    subplot(221),imagesc(Bp_image_old(300:500,300:500,k)), axis xy image, title(['old ' num2str(k)]), colorbar
    subplot(222),imagesc(Bp_image_new(300:500,300:500,k)), axis xy image, title(['new ' num2str(k)]), colorbar
    if k>1
        subplot(223),imagesc(Bp_image_old(300:500,300:500,k)-Bp_image_old(300:500,300:500,k-1)),
        axis xy image, title('old(k)-old(k-1)'), colorbar
    end
    subplot(224),imagesc(Bp_image_new(300:500,300:500,k)-Bp_image_old(300:500,300:500,k)),
    axis xy image, title('new(k)-old(k)'), colorbar
    %     coef(k)=mean2(Bp_image_new(100:300,100:300,k)-Bp_image_old(100:300,100:300,k));
    pause%(0.5)
end
figure(12000)
plot(coef)
%%
% Bp_image=Bp_image_new;

% Image_Mean_old=mean(Bp_image_old(:,:,h),3);
% Image_Var_old=var(Bp_image_old(:,:,h),[],3);
% Image_Mean_new=mean(Bp_image_new(:,:,h),3);
% Image_Var_new=var(Bp_image_new(:,:,h),[],3);

figure(9),
subplot(211),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean_old),
colorbar, axis image,title('old mean')
subplot(212), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var_old),
axis image,title('old std'),colorbar
colormap jet

figure(10),
subplot(211),imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Mean_new),
colorbar, axis image,title('new mean')
subplot(212), imagesc((xr(1,:)+offsetX)*1e3-offsetX,(zr(:,1)-y_array(1))*1e3,Image_Var_new),
axis image,title('new std'),colorbar
colormap jet

return

%%%%%%%% JUNK or to be used later %%%%%%%%%%%%%%%
%% calcul du d�calage temporel entre instant du pulse laser (ttl_sig) et d�clenchement
% de l'acquisition sur l'aixplorer (trig_out_aix)
n= length(trig_out_aix)/(n_avg);
trig_out_aix =reshape(trig_out_aix, n, n_avg,n_it);
ttl_sig =reshape(ttl_sig, n, n_avg,n_it);
figure(1),plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix(:,1,1))
title('shift between trig out laser and trig out ultrasound machine')
Dec_trig=zeros(size(ttl_sig,2),size(ttl_sig,3));

for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        AAlign=xcorr(trig_out_aix(:,k,kk),ttl_sig(:,k,kk),'coeff');
        I=find(AAlign==max(AAlign));
        Dec_trig(k,kk)=min(I-size(trig_out_aix,1));
    end
end

%on recale les Dec_trig manquants (=0) � la moy des autres
Dec_trig(find(Dec_trig(:)==0))=mean(Dec_trig(:)); % a checker

figure(2), subplot(121), plot(ttl_sig(:,1,1)), hold all, plot(trig_out_aix((Dec_trig(1)+1):end,1,1))
subplot(122), imagesc(Dec_trig),




%% calcul de l'intensit� des pulses laser et de leur profil temporel
sig_photo=zeros(51,size(ttl_sig,2),size(ttl_sig,3));
PD_sig =reshape(PD_sig, n, n_avg,n_it);

center=1637;
for kk=1:size(ttl_sig,3)
    for k=1:size(ttl_sig,2)
        nrj_pulse(k,kk)=sum(PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk)));
        sig_photo(:,k,kk)=PD_sig(center+(-25:25),k,kk)-mean(PD_sig(3001:4000,k,kk));
    end
end

figure(3); imagesc(sig_photo(:,:,1));
%% calcul du d�calage temp. entre les diff�rents pulses laser (� ttl fix�, l'instant d'�mission r�el du pulse varie un peu)
% n'est pas utilis� en fait??
Align=double(squeeze(sig_photo));
ref=Align(:,1);
for kk=1:size(Align,3)
    for k=1:size(Align,2)
        AAlign=xcorr(Align(:,k,kk),ref,'coeff');
        I=find(AAlign==max(AAlign));
        Dec_pulse(k,kk)=I-size(Align,1);
    end
end
Dec_pulse=mean(-Dec_pulse(:))+Dec_pulse;
clear Align sig_photo

%% cut beginning and end of signal because saturated signals because of the machine
PAsig1=PAsig(151:(end-350),:,:,:);
% clear PAsig

%% interpolation
interpfact=1; %

dz=c/(samplingRate*interpfact); % check this one

if interpfact>1 % never used so far
    PAsig=zeros([size(PAsig1,1)*interpfact,size(PAsig1,2),size(PAsig1,3)],'single');
    for kx=1:size(PAsig1,2)
        for j=1:size(PAsig1,3)
            PAsig(:,kx,j)= single(interp(double(squeeze(PAsig1(:,kx,j,ik))),interpfact));
        end
    end
else
    PAsig=single(squeeze(PAsig1(:,:,:,ik)));
end


%% on coupe eventuellement sur la voie 86 car un peu degueu
figure(4), subplot(121), imagesc(PAsig(:,:,1))
PAsig00=PAsig;
for k=1:size(PAsig ,3);
    for kk=1:size(PAsig ,4);
        PAsig00(150:550,86,k,kk)=mean(PAsig(150:550,[85 87],k,kk),2);
    end
end
PAsig=PAsig00;
clear PAsig00
subplot(122), imagesc(PAsig(:,:,1))

%% methode manu: on enl�ve la moyenne des signaux pour couper les pics
% � bricoler si beaucoup de signaux quasi "horizontaux"??
figure(5), subplot(121), imagesc(PAsig(:,:,1))
for k=1:size(PAsig,3)
    for j=1:size(PAsig,2)
        PAsig2(:,j,k)= PAsig(:,j,k)-squeeze(mean(PAsig(:,[1:42,87:128],k),2)); % a checker
    end
end
subplot(122), imagesc(PAsig2(:,:,1))

%% Recalage des signaux entre eux (pour corriger le jitter, entre autres li�s au jitter sur l'�mission du pulse laser)
% (Dec_pulse n'est pas utilis� finalement...?)
if ik==1
    Ref=mean(double(squeeze(PAsig2((500*interpfact):(900*interpfact),32:3:96,:))),3);
end
for kx=1:size(PAsig2,3)
    cc = xcorr2(double(squeeze(PAsig2((500*interpfact):(900*interpfact),32:3:96,kx))),Ref);
    cc2=interp(cc(:),10);
    [max_cc, imax] = max(abs(cc2));
    [ypeak, xpeak] = ind2sub([size(cc,1)*10 size(cc,2)],imax(1));
    corr_offset(kx,:) = [ (ypeak-size(Ref,1)*10 +9) (xpeak-size(Ref,2)) ];
end
Dec=corr_offset(:,1);
if ik==1
    figure(6); plot(Dec), title('jitter calculated by sigs cross corr, in tenths of pixel')
end
for k=1:size(PAsig2 ,3)
    PAsig2 (:,:,k) = DelaySignal(PAsig2(:,:,k),1,-Dec(k)/10);
end


%%%%%%%% FUNCTIONS %%%%%%%%%
function voltData = raw2volts(rawData,sysInfo)
voltData = (sysInfo.SampleOffset - rawData)/sysInfo.SampleResolution;
end


%%%%%%%% JUNK %%%%%%%%%%

% %% Load data from a single h5 file
% try
%     [file,path] = uigetfile([folder '/*.h5'],'Select data file');
% catch
%     [file,path] = uigetfile('*.h5','Select data file');
% end
%
% if exist('data_mean')==1 % save time if data already in workspace
%     disp('Using data from workspace')
%     data=data_mean;
%     clear data_mean
% else
%     [~,file,ext] = fileparts(file);
%     % Load the associated parameters file
%     load([path filesep file '_params.mat'])
%
%     % Get info about the h5 file and load the mean and std datasets
%     info = h5info([path file ext]);
%     try
%         data = h5read([path file ext],'/data/mean');
%     catch
%         data = h5read([path file ext],'/data');
%     end
% end
% % data = raw2volts(data,sysinfo);
%
% layers=split(path,filesep);
% day=layers{end-2};
% hour=layers{end-1};
% processing_folder=[fullfile(layers{1:end-4}) filesep username filesep day filesep hour '_processing'];
% % mkdir(processing_folder)



%% Cross-correlation to check signal delay
% if 1
%     [wf, ~, ~] = fp_genGaussian(pulse.frequency,pulse.amplitude,15e6,pulse.sampleRate,pulse.DurTarget);
%     wf=double(wf(1:150))-2047;
%     for k=1:Npix
%         s2x(:,k)=flipud(conv(flipud(PAsig2(:,k)),wf,'same')); %first flipud to get cross-correlation, second to get the right time
%     end
%     s2x_pix=reshape(s2x(length(wf)+1:end,:),size(s2x,1)-length(wf),sqrt(Npix),sqrt(Npix));
%     s2xptp=reshape(squeeze(max(s2x(length(wf)+1:end,:),[],1)-min(s2x(length(wf)+1:end,:),[],1)),sqrt(size(s2x,2)),sqrt(size(s2x,2)));
%     figure(77), imagesc(scan.x,scan.y,s2xptp), axis image
%     title('Peak to peak amplitude of correlation signals')
% end

%% re-generate acoustic stimulation signal and switch gate/trigger signal from saved parameters

% wrong when using the TGF4242 generator
% t = (0:(params.gage.acqInfo.Depth-1))/params.gage.acqInfo.SampleRate;
% sig = wavepondWF(params.wf);
% figure(1),plot_norm(sig.time,double(sig.waveform))
% hold on, plot_norm(t,mean(PAsig2(:,:),2)), hold off
% xlabel('time'),axis tight
% legend({'Stimulation','Mean PA sig'})
