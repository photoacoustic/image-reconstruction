%% Load things
clearvars -except username p_txy
folder=makeMyDay(username);
path = 'D:\Data\Jeremy\200820';
folders = {'11h09m','11h30m','12h00m'};
filename = 'sum_img.mat';

us = cell(length(folders),1);
for i=1:length(folders)
    us{i} = load([path filesep folders{i} '_processing' filesep filename]);
end



%% Play with images
PAimgT = [];
PAimg2T = [];
PAimg3T = [];
for i=1:length(us)
    us{i}.PAimg2 = abs(hilbert(us{i}.PAimg));
    us{i}.PAimg3 = imgaussfilt3(us{i}.PAimg2(:,:,:,3),1);
    
    PAimgT = [PAimgT, us{i}.PAimg];
    PAimg2T = [PAimg2T, us{i}.PAimg2];
    PAimg3T = [PAimg3T, us{i}.PAimg3];
end

scan = us{1}.scan;
samplingRate = us{1}.acqInfo.SampleRate; % in Hz
c= 1475; % sound speed in m/s
dz=c/samplingRate;


% dexhot(PAimgT(1:1028,:,:,3),1,[1 scan.dx/dz scan.dx/dz]);
% dexhot(PAimg2T(1:1028,:,:,3),1,[1 scan.dx/dz scan.dx/dz]);
% dexhot(PAimg3T(1:1028,:,:),1,[1 scan.dx/dz scan.dx/dz]);

%%
yplane = 42;

figure(1),clf
    for i=1:length(us)
        subplot(1,length(us),i),cla
            PAimg2D = us{i}.PAimg(1:1028,:,yplane,3);
            zaxis = ((1:size(PAimg2D))-1)*dz;
        xaxis = repmat((scan.x-scan.xOffset)',3,1);
        imagesc(xaxis*1e3,zaxis*1e3,PAimg2D);
        colormap gray

        xlabel('x [mm]')
        if i==1, ylabel('z [mm]'); else, yticks([]); end
    end
    
    
figure(2),clf
    for i=1:length(us)
        subplot(1,length(us),i),cla
            PAimg2D = us{i}.PAimg2(1:1028,:,yplane,3);
            zaxis = ((1:size(PAimg2D))-1)*dz;
        xaxis = repmat((scan.x-scan.xOffset)',3,1);
        imagesc(xaxis*1e3,zaxis*1e3,PAimg2D);
        colormap gray

        xlabel('x [mm]')
        if i==1, ylabel('z [mm]'); else, yticks([]); end
    end
    
    
figure(3),clf
    for i=1:length(us)
        subplot(1,length(us),i),cla
            PAimg2D = us{i}.PAimg3(1:1028,:,yplane);
            zaxis = ((1:size(PAimg2D))-1)*dz;
        xaxis = repmat((scan.x-scan.xOffset)',3,1);
        imagesc(xaxis*1e3,zaxis*1e3,PAimg2D);
        colormap gray

        xlabel('x [mm]')
        if i==1, ylabel('z [mm]'); else, yticks([]); end
    end

    


% volumeViewer(permute(PAimg3T(1:1028,:,:),[2 3 1]),'ScaleFactors',[scan.dx/dz scan.dx/dz 1]) % use permute, because y is the default depth in the viewer



