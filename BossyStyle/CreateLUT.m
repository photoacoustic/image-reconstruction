addpath(genpath('/Users/thomas/Desktop/ChLabGit/Photoacoustic/Rig/pulse-echo_measurement'))
clearvars -except username folder LUT
% folder=makeMyDay(username);
saveFlag=0;

%% load data
if exist('folder')
    [RFdata,params,filepath] = loadScanFPFile(folder); % from scan_fp
else
    [RFdata,params,filepath] = loadScanFPFile; % from scan_fp
end

%% filter and plot 
RF=squeeze(RFdata.data.data(:,2,:,:)); % be careful about channel 
[b,a] = butter(3,[1e6 120e6]/(params.gage.acqInfo.SampleRate/2));
PAsig2=double(single(filtfilt(b,a,double(RF)))); %./nrj_pulse(k,ik); --> normalisation for later
istart=1;%000;
figure, subplot(223), imagesc(PAsig2(istart:end,:))
subplot(224), plot(mean(PAsig2(istart:end,:),2))
subplot(221), imagesc(RF(istart:end,:))
subplot(222), plot(mean(RF(istart:end,:),2))

%% Plot time signals
window.flag = 1; % Cuts time signals into a given time window if true
window.borders = [1 5]*1e-6; % Time window interval [s]
window.indexes = []; % Time window indexes

filter.flag = 1; % Filters the time signals
filter.borders = [1 120]*1e6; % Passband filter range [Hz]
filter.order = 1; % Butterworth passband filter order

[hFigTime,window] = plotTimeSignals(RFdata,params.gage.acqInfo.SampleRate,2,window,filter,1);
[hFigTimeImage,window] = plotTimeSignals(RFdata,params.gage.acqInfo.SampleRate,2,window,filter,2,'image');
[hFigPtP,~] = plotTimeSignalsPeak2Peak(RFdata,params.gage.acqInfo.SampleRate,2,window,filter,params.scan.x,params.scan.y,3);


%% create receivers and beamforming objects

[X,Y]=meshgrid(params.scan.x-params.scan.xOffset,params.scan.y-params.scan.yOffset);
x=reshape(X,numel(X),1);
y=reshape(Y,numel(X),1);
receivers.pos_mm.x=x*1e3; 
receivers.pos_mm.y=y*1e3;
receivers.pos_mm.z=0*x;

% receivers.pos_mm.x=(params.scan.x-params.scan.xOffset)'*1e3; 
% receivers.pos_mm.y=(params.scan.y-params.scan.yOffset)'*1e3;
% receivers.pos_mm.z=0*receivers.pos_mm.x;

receivers.N=params.scan.Npts;
receivers.elevation_focus_mm=1; % check code to see where this is used and if we need to provide some accurate estimate of object depth

beamforming = Beamforming(receivers);
beamforming.mode='photoacoustic';
beamforming.c=1.480; % params.us.c; % mm/us
beamforming.sampling_freq_MHz=params.gage.acqInfo.SampleRate*1e-6;

US_Transducer_Sonaxis=US_Transducer_Sonaxis;
Dt=(US_Transducer_Sonaxis.time_pulse)*params.gage.acqInfo.SampleRate; % in samples, time-of-flight from transducer to FP cavity surface
delay=4*.1e-6*params.gage.acqInfo.SampleRate+Dt;
beamforming.t0_pulse_pt=delay; % or 1? t0_pulse*fs;

% beamforming.t0_pulse_pt=0; % or 1? t0_pulse*fs;

% beamforming.window=window;
% if window.flag
%     beamforming.acquisition_delay_pt=window.indexes(1); % or 1? t0_acq*fs;
%     beamforming.acquisition_duration_pt=window.indexes(end)-window.indexes(1);%size(RFdata.data.data,1); % make it smaller to kill reflections
% else
    beamforming.acquisition_delay_pt=0; % or 1? t0_acq*fs;
    beamforming.acquisition_duration_pt=size(RFdata.data.data,1); % make it smaller to kill reflections
% end

beamforming.number_of_elements=params.scan.Npts; 
% number of elements on ONE side; --> really?? does not make sense...,
% trying real nb --> works now

% reconstruction grid voxel size
% bf_step_mm=1*params.scan.dx*1e3;
% % bf_step_mm=0.05;
% 
% % extent of the reconstructed volume
% FOV_x_mm=params.scan.dx*(params.scan.nx-1)*1e3; % we could try to take a larger FOV that go es outside (in xy) the scanned area, todo
% FOV_y_mm=params.scan.dx*(params.scan.ny-1)*1e3;
% FOV_z_mm=1.5;
% 
% % center coordinates of reconstructed volume
% xc_mm=0; % params.scan.xOffset; % offset does not work for now
% yc_mm=0; % params.scan.yOffset;
% zc_mm=3;
% 
% beamforming.x_pixel_mm=(xc_mm-FOV_x_mm/2:bf_step_mm:xc_mm+FOV_x_mm/2);
% beamforming.y_pixel_mm=(yc_mm-FOV_y_mm/2:bf_step_mm:yc_mm+FOV_y_mm/2);
% beamforming.z_pixel_mm=(zc_mm-FOV_z_mm/2:bf_step_mm:zc_mm+FOV_z_mm/2);

step_mm = 0.01;
centers_mm = [0 0 3];
FOVs_mm = [params.scan.dx*(params.scan.nx-1)*1e3...
           params.scan.dx*(params.scan.ny-1)*1e3...
           1.5];
beamforming.ComputeGrids(step_mm,FOVs_mm,centers_mm)

beamforming.NA=1;
beamforming.apodization=0;

beamforming.LUTmaxChunkBytes = 20e6;
beamforming.Validate();

% expectedLUTSizeElt=length(beamforming.x_pixel_mm)*length(beamforming.y_pixel_mm)*length(beamforming.z_pixel_mm);
% expectedLUTSizeMB=8*1e-6*expectedLUTSizeElt;
% % nbytes = estimateMatrixSize(size(),varclass)
% disp(expectedLUTSizeMB)

%% compute LUT
clear LUT
tic
LUT=BF_compute_LUT(beamforming);
toc
% if isempty(LUT.delays_pt{1})
%     disp('There might be an issue with the LUT, please have a look')
% end
%  
%% save
if saveFlag
    tic
    save([filepath.processing filesep 'Beamforming'],'beamforming','LUT','-v7.3')
    toc
end
% try a hf5 version

%% Original version from Emmanuel Bossy
% 
% load('RFData.mat')
% 
% %
% beamforming.receivers=receivers;
% beamforming.mode='photoacoustic';
% beamforming.c=c; % mm/us
% beamforming.sampling_freq_MHz=fs;
% beamforming.t0_pulse_pt=t0_pulse*fs;
% beamforming.acquisition_delay_pt=t0_acq*fs;
% beamforming.acquisition_duration_pt=size(RF,1);
% beamforming.number_of_elements=size(RF,2);
% 
% %
% bf_step_mm=0.05;
% 
% xc_mm=0;
% yc_mm=0;
% zc_mm=25;
% 
% FOV_x_mm=6;
% FOV_y_mm=0;
% FOV_z_mm=8;
% 
% beamforming.x_pixel_mm=(xc_mm-FOV_x_mm/2:bf_step_mm:xc_mm+FOV_x_mm/2);
% beamforming.y_pixel_mm=(yc_mm-FOV_y_mm/2:bf_step_mm:yc_mm+FOV_y_mm/2);
% beamforming.z_pixel_mm=(zc_mm-FOV_z_mm/2:bf_step_mm:zc_mm+FOV_z_mm/2);
% 
% 
% beamforming.NA=1;
% beamforming.apodization=1;
% 
% %
% tic
% LUT=BF_compute_LUT(beamforming);
% toc
% 
% save Beamforming beamforming LUT
%
