clear all

%% Sources
sources = PASources(50,0.9); % f0 in MHz, BW is fractional
sources.SetSources([0 -0.2 -0.2 0.2 0.2 0.2],[0 -0.2 -0.2 0.2 0.2 0.1],[2 1.7 2.3 1.7 2.3 2]); % Positions in mm

%% Transducers
receivers = PAReceivers();
receivers.GenerateReceiverGrid(0.05,[1 1 0],[0 0 0]); % step_mm, FOVs_mm [x y z], centers_mm [x y z]

%% Acquisition parameters
rf = PASignals(1,1,500); % to_acq [us], acq_length [us], sampling freq [MHz]

%% Generate signals
rf.receivers = receivers;
rf.Generate(sources);

%% Plot
rf.PlotSourcesAndReceivers(1);
rf.PlotRF(2);


