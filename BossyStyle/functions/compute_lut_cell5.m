function [LUT] = compute_lut_cell5(beamforming)
    nx = numel(beamforming.x_pixel_mm);
    ny = numel(beamforming.y_pixel_mm);
    nz = numel(beamforming.z_pixel_mm);
    
    [LUT, delays_pt, idx_active_points] = compute_lut_vectorized(beamforming);
    LUT.delays_pt = cell(nx,ny,nz);
    LUT.apodization_coeff = cell(nx,ny,nz);
    
    for x = 1:nx
        for y = 1:ny
            for z = 1:nz
                idx = idx_active_points(:,x,y,z);
                LUT.delays_pt{x,y,z} = single(delays_pt(idx,x,y,z));
                
                if beamforming.apodization
                    LUT.apodization_coeff{x,y,z} = apodization_coeff(idx,x,y,z);
                end

            end
        end
    end
        
end

