function sz = estimate_lut_max_size(beamforming)

    sz = estimateMatrixSize([beamforming.sizes_px beamforming.acquisition_duration_pt],'single');
    disp(['LUT size (singles) = ' num2str(sz/1e3) ' Ko'])
    
    sz = estimateMatrixSize([beamforming.sizes_px beamforming.acquisition_duration_pt],'double');
    disp(['LUT size (doubles) = ' num2str(sz/1e3) ' Ko'])
end

