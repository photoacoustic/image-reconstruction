function [LUT] = compute_lut_cell4(beamforming)
    LUT.delays_pt = cell(size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2));
    LUT.apodization_coeff = cell(size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2));

    nx = size(LUT.delays_pt,1);
    ny = size(LUT.delays_pt,2);
    nz = size(LUT.delays_pt,3);
    
    delays_pt_constant_vec = (0:beamforming.acquisition_duration_pt:(beamforming.number_of_elements-1)*beamforming.acquisition_duration_pt)';

    x_distance_mm = abs(beamforming.receivers.pos_mm.x-beamforming.x_pixel_mm);
    y_distance_mm = abs(beamforming.receivers.pos_mm.y-beamforming.y_pixel_mm);
    z_distance_mm = abs(beamforming.receivers.pos_mm.z-beamforming.z_pixel_mm);
        
    for x_idx = 1:nx
        for y_idx = 1:ny
            xy_distance_mm2 = x_distance_mm(:,x_idx).^2 + y_distance_mm(:,y_idx).^2;
            xy_distance_mm = sqrt(xy_distance_mm2);
            
            for z_idx = 1:nz
                total_distance_mm = sqrt(xy_distance_mm2 + z_distance_mm(:,z_idx).^2);

                delays_us = 1/beamforming.c*total_distance_mm...
                    -beamforming.acquisition_delay_pt/beamforming.sampling_freq_MHz...
                    +beamforming.t0_pulse_pt/beamforming.sampling_freq_MHz;

                switch beamforming.mode
                    case 'photoacoustic'
                        LUT.type='photoacoustic';
                    case 'flat'
                        LUT.type='flat';
                        delays_us=delays_us+1/c*z_distance_mm;
                    case 'US_flat'
                        LUT.type='flat';
                        delays_us=delays_us+1/c*z_distance_mm;
                    otherwise
                        error('pb mode');
                end

                delays_pt = round(delays_us*beamforming.sampling_freq_MHz);


                %% NA limitation (NA=1 takes all the elements)
                local_NA=xy_distance_mm./total_distance_mm;
                max_local_NA=max(local_NA);

                NA_mask=(local_NA<beamforming.NA);


                apodization_coeff=1+0*NA_mask;

                if beamforming.apodization
                    apod_NA=min(beamforming.NA,max_local_NA);
                    apodization_coeff=0.54+0.46*cos(local_NA/apod_NA*pi);
                end

                apodization_coeff=apodization_coeff.*NA_mask;


                idx_active_points = (delays_pt>=1) & (delays_pt<=beamforming.acquisition_duration_pt) & (apodization_coeff>0);
                delays_pt = delays_pt + delays_pt_constant_vec;

                LUT.delays_pt{x_idx,y_idx,z_idx} = single(delays_pt(idx_active_points));
                
                if beamforming.apodization
                    LUT.apodization_coeff{x_idx,y_idx,z_idx} = apodization_coeff(idx_active_points);
                end
                
            end
            
        end
    end
end

