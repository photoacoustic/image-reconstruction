function tot = gpuarr_tot_dist(xy2,z)
    tot = sqrt(xy2 + z.^2);
end

