function [LUT, delays_pt, idx_active_points] = compute_lut_matrixv(beamforming)
    nx = numel(beamforming.x_pixel_mm);
    ny = numel(beamforming.y_pixel_mm);
    nz = numel(beamforming.z_pixel_mm);

    [LUT, delays_pt, idx_active_points] = compute_lut_vectorized(beamforming);
    LUT.delays_pt = nan(beamforming.N_receivers,nx,ny,nz);
    LUT.delays_pt(idx_active_points) = delays_pt(idx_active_points);
    
    LUT.idx_active_points = idx_active_points;
end

