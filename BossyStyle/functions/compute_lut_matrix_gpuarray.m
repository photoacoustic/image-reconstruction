function [LUT, delays_pt, idx_active_points] = compute_lut_matrix_gpuarray(beamforming)
    nx = numel(beamforming.x_pixel_mm);
    ny = numel(beamforming.y_pixel_mm);
    nz = numel(beamforming.z_pixel_mm);
    
    [LUT, delays_pt, idx_active_points] = compute_lut_gpuarray(beamforming);
    LUT.delays_pt = gpuArray(nan(beamforming.number_of_elements,nx,ny,nz,'single'));
    LUT.delays_pt(idx_active_points) = delays_pt(idx_active_points);
    LUT.delays_pt = gather(LUT.delays_pt);
end

