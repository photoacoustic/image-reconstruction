function [LUT] = compute_lut_matrix(beamforming,receivers)
    LUT.delays_pt = nan(size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2),beamforming.number_of_elements,'single');
    LUT.apodization_coeff = nan(size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2),beamforming.number_of_elements,'single');
    matSize = estimateMatrixSize([size(beamforming.x_pixel_mm,2),size(beamforming.y_pixel_mm,2),size(beamforming.z_pixel_mm,2),beamforming.number_of_elements],'single');
    disp(['Matrix size = ' num2str(matSize/1000) ' Ko'])
    
    for x_idx = 1:size(beamforming.x_pixel_mm,2)
        for y_idx = 1:size(beamforming.y_pixel_mm,2)
            for z_idx = 1:size(beamforming.z_pixel_mm,2)
                
                total_distance_mm=sqrt((receivers.pos_mm.x'-beamforming.x_pixel_mm(x_idx)).^2+...
                    (receivers.pos_mm.y'-beamforming.y_pixel_mm(y_idx)).^2+...
                    (receivers.pos_mm.z'-beamforming.z_pixel_mm(z_idx)).^2);

                xy_distance_mm=sqrt((receivers.pos_mm.x'-beamforming.x_pixel_mm(x_idx)).^2+...
                    (receivers.pos_mm.y'-beamforming.y_pixel_mm(y_idx)).^2);

                z_distance_mm=abs(receivers.pos_mm.z'-beamforming.z_pixel_mm(z_idx));

                delays_us = 1/beamforming.c*total_distance_mm...
                    -beamforming.acquisition_delay_pt/beamforming.sampling_freq_MHz...
                    +beamforming.t0_pulse_pt/beamforming.sampling_freq_MHz;

                %% delays computation

                switch beamforming.mode
                    case 'photoacoustic'
                        LUT.type='photoacoustic';
                    case 'flat'
                        LUT.type='flat';
                        delays_us=delays_us+1/c*z_distance_mm;
                    case 'US_flat'
                        LUT.type='flat';
                        delays_us=delays_us+1/c*z_distance_mm;
                    otherwise
                        error('pb mode');
                end

                delays_pt=round(delays_us*beamforming.sampling_freq_MHz);


                %% NA limitation (NA=1 takes all the elements)
                local_NA=xy_distance_mm./total_distance_mm;
                max_local_NA=max(local_NA);

                NA_mask=(local_NA<beamforming.NA);

                apodization_coeff=1+0*NA_mask;

                if beamforming.apodization
                    apod_NA=min(beamforming.NA,max_local_NA);
                    apodization_coeff=0.54+0.46*cos(local_NA/apod_NA*pi);
                end

                apodization_coeff=apodization_coeff.*NA_mask;


                idx_active_points= (delays_pt>=1) & (delays_pt<=beamforming.acquisition_duration_pt) & (apodization_coeff>0);
                delays_pt = delays_pt + (0:beamforming.acquisition_duration_pt:(beamforming.number_of_elements-1)*beamforming.acquisition_duration_pt);
                LUT.delays_pt(x_idx,y_idx,z_idx,idx_active_points) = delays_pt(idx_active_points);
                LUT.apodization_coeff(x_idx,y_idx,z_idx,idx_active_points)=apodization_coeff(idx_active_points);
            end
        end
    end
end

