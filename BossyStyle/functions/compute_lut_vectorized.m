function [LUT, delays_pt, idx_active_points] = compute_lut_vectorized(beamforming)
    x_distance_mm = single(abs(beamforming.receivers.pos_mm.x-beamforming.x_pixel_mm));
    y_distance_mm = single(abs(beamforming.receivers.pos_mm.y-beamforming.y_pixel_mm));
    z_distance_mm = single(abs(beamforming.receivers.pos_mm.z-beamforming.z_pixel_mm));
    
    x_distance_mm = reshape(x_distance_mm,[size(x_distance_mm,1) size(x_distance_mm,2) 1]);
    y_distance_mm = reshape(y_distance_mm,[size(y_distance_mm,1) 1 size(y_distance_mm,2)]);
    z_distance_mm = reshape(z_distance_mm,[size(z_distance_mm,1) 1 1 size(z_distance_mm,2)]);
    
    xy_distance_mm2 = x_distance_mm.^2 + y_distance_mm.^2;
    xy_distance_mm = sqrt(xy_distance_mm2);
    
    total_distance_mm = sqrt(xy_distance_mm2 + z_distance_mm.^2);
    clearvars xy_distance_mm2
    
    delays_us = 1/beamforming.sound_speed*total_distance_mm...
                    -beamforming.acq_delay_pt/beamforming.fs_MHz...
                    +beamforming.t0_pulse_pt/beamforming.fs_MHz;
                
    local_NA = xy_distance_mm./total_distance_mm;
    clearvars xy_distance_mm total_distance_mm
                
                
    switch beamforming.mode
        case 'photoacoustic'
            LUT.type = 'photoacoustic';
        case 'flat'
            LUT.type = 'flat';
            delays_us = delays_us+1/beamforming.sound_speed*z_distance_mm;
        case 'US_flat'
            LUT.type = 'flat';
            delays_us = delays_us+1/beamforming.sound_speed*z_distance_mm;
        otherwise
            error('pb mode');
    end
    
    
    delays_pt = round(delays_us*beamforming.fs_MHz);
    clearvars delays_us
    delays_pt_constant_vec = (0:beamforming.acq_duration_pt:(beamforming.N_receivers-1)*beamforming.acq_duration_pt)';

    
    max_local_NA = max(local_NA,[],1);
    NA_mask = (local_NA<beamforming.NA);
    apodization_coeff = 1+0*NA_mask;

    if beamforming.apodization
        apod_NA = min(beamforming.NA,max_local_NA);
        apodization_coeff = 0.54+0.46*cos(local_NA./apod_NA*pi);
    end
    clearvars local_NA

    apodization_coeff = apodization_coeff.*NA_mask;
    clearvars NA_mask
    
    idx_active_points = (delays_pt>=1) & (delays_pt<=beamforming.acq_duration_pt) & (apodization_coeff>0);
    delays_pt = delays_pt + delays_pt_constant_vec;
end

