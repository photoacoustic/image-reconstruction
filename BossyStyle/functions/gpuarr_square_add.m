function out = gpuarr_square_add(in1,in2)
    out = in1.^2 + in2.^2;
end

