clear all

FLAGS.GenRF = 1;
FLAGS.LoadRF = 0;
FLAGS.LoadedRFCorrection = 1;
FLAGS.PlotRF = 1;
FLAGS.BFobj = 1;
FLAGS.LUT = 1;
FLAGS.Rec = 1;
FLAGS.PlotBF = 1;


%% Signal generation
if FLAGS.GenRF
    sources = PASources(50,0.9); % f0 in MHz, BW is fractional
    % Some points
%     sources.SetSources([0 -0.2 -0.2 0.2 0.2 0.2],[0 -0.2 -0.2 0.2 0.2 0.1],[2 1.7 2.3 1.7 2.3 2]); % Positions in mm
    % Cross
    sources.SetSources([linspace(-0.5,0.5,100) linspace(0,0,100) linspace(-0.5,0.5,100)],[linspace(0,0,100) linspace(-0.5,0.5,100) linspace(-0.5,0.5,100)],[0.5*linspace(1,1,100) 0.3*linspace(1,1,100) linspace(0.2,1.2,100)]); % Positions in mm

    receivers = PAReceivers(); % step_mm, FOVs_mm [x y z], centers_mm [x y z]
    receivers.GenerateReceiverGrid(0.05,[2 2 0],[0 0 0]);
    rf = PASignals(0,3,500); % t0_acq [us], acq_length [us], sampling freq [MHz]
    rf.Generate(sources,receivers);

    % Alter generated perfect signal with various steps
%     rf.data = rf.RandomizePixelAmplitude(rf.data,'dim',2);
%     rf.data = rf.AddGaussianNoise(rf.data,0.2);
%     rf.data = rf.AddRandomOnOff(rf.data,'dim',2);

%     for i=1:size(rf.data,2)
%         BW = randi([2 25]);
%         rf.data(:,i) = rf.BandpassFilter(rf.data(:,i),BW*1e6,500e6,1);
%     end

    if FLAGS.PlotRF
        rf.PlotSourcesAndReceivers(1);
        rf.PlotRF(2);
    end
end


%% Load acquired signals
if FLAGS.LoadRF
	rf = PASignals([],[],[],'loadExperiment',true,'experimentPath','D:\Data\Jeremy\210824');

    rf.data = rf.SubstractMean(rf.data);
    rf.data = rf.BandpassFilter(rf.data,[1 150]*1e6,500e6,1);
    rf.data = rf.SmoothMovmean(rf.data,500);

%     rf.data(1:50,:) = 0;
%     rf.data(900:end,:) = 0;


    if FLAGS.PlotRF
%         rf.PlotSourcesAndReceivers(1);
        rf.PlotRF(2);

%         caxis([-0.005 0.005])
        ylim([0 2])
        colorbar

%         rf.PlotRF(2,'plot');
    end
end


%% Beamforming object
if FLAGS.BFobj
    beamforming = Beamforming(rf);
    beamforming.ComputeGrids(0.01,[2 2 1],[0 0 0.5]) % step_mm, FOVs_mm [x y z], centers_mm [x y z]
    beamforming.LUTmaxChunkBytes = 300e6;

    beamforming.Validate();
    beamforming.Show();
end


%% LUT computation
if FLAGS.LUT
    lut = LUT(beamforming);
    lut.Compute(beamforming,'type','matrixvgpu2','save',true);
end


%%
if FLAGS.Rec
    PAimg = PAImage(beamforming);
    PAimg.Compute(rf.data,lut,beamforming,'type','multiloops');

    if FLAGS.PlotBF
        hFigProjMIP = PAimg.PlotProj(beamforming,'mip',3);
        hFigProjSum = PAimg.PlotProj(beamforming,'sum',4);
%         PAimg.Dexhot();
%         PAimg.ViewVolume();
    end
end

%%
% hFigProjMIP = PAimg.PlotProj(beamforming,'mip',3);
% hFigProjSum = PAimg.PlotProj(beamforming,'sum',4);
% save([filepath.processing filesep 'reconstructed_img_norm_corr_filter=10-120MHz.mat'],'PAimg','lut','rf','params','beamforming','receivers','-v6')
% save([filepath.processing filesep 'reconstructed_img_norm_corr_nofilter.mat'],'PAimg','lut','rf','params','beamforming','receivers','-v6')
save([filepath.processing filesep 'reconstructed_img_norm_corr_filter=20-150MHz.mat'],'PAimg','lut','rf','params','beamforming','receivers','-v6')
