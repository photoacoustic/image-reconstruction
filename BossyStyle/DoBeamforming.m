startFromScratchFlag=0; % make it zero if you want to use data and LUT already in workspace

if startFromScratchFlag
    clearvars -except username folder LUT
    % folder=makeMyDay(username);
    % saveFlag=0;
    
    % load data and LUT
    [RFdata,params,filepath] = loadScanFPFile(folder); % from scan_fp
    load([filepath.processing filesep 'Beamforming']);
    sourceFlag=0;
end

%%
% if beamforming.window.flag
%     RF=squeeze(RFdata.data.data(beamforming.window.indexes(1):beamforming.window.indexes(end),2,:,:)); % be careful about channel
% else
    RF=squeeze(RFdata.data.data(:,2,:,:)); % be careful about channel
% end

% RF=reshape(RF,size(RF,1),params.scan.Npts);
[b,a] = butter(3,[1e6 120e6]/(params.gage.acqInfo.SampleRate/2));
PAsig2=double(single(filtfilt(b,a,double(RF)))); %./nrj_pulse(k,ik); --> normalisation for later
figure, subplot(121), imagesc(PAsig2(:,:))
subplot(122), plot(mean(PAsig2(:,:),2))

% RF=permute(RF,[1 3 2]);

%% normalise data to compensate for reflectivity fluctuations depending on cavity pixel
load('D:\Data\Jeremy\210507\calibscan_yoko_spinCoating_E210427_dielM7AR_newsample_FOV2mm_step25um.mat')
fpi = FPISpectraAnalysis('lambda',lambda,'spectra',spectra,'lambdadim',5);
fpi.Normalize('spectra');
fpi.Smooth(10,'spectra');
fpi.ComputeDerivative();
fpi.Smooth(10,'derivative');
fpi.ComputeCurvature();
fpi.ComputeOptimalWavelength('maxabsderivative',[1015e-9 1038e-9]);
fpi2 = FPISpectraAnalysis('lambda',lambda,'spectra',spectra,'lambdadim',5); % to use non normalised spectra
for kx=1:size(fpi.optimalWavelengthIdx,2)
    for ky=1:size(fpi.optimalWavelengthIdx,3)
        normCoef(kx,ky)=fpi2.spectra(fpi.optimalWavelengthIdx(1,kx,ky),kx,ky);
        PAsig3(:,kx,ky)=PAsig2(:,kx,ky)/normCoef(kx,ky);
    end
end
figure, imagesc(normCoef)

 


%% beamform image
tic 
% BF=BF_compute_beamformed_data(PAsig3,LUT,'singleloop');
% BF=BF_compute_beamformed_data(RF,LUT,'multiloops');
BF = BF_compute_beamformed_data(RF,LUT,beamforming,'looptype','singleloop');
% BF=BF_compute_beamformed_data(RF,LUT,'looptype','multiloops');
% BF=BF_compute_beamformed_data(RF,LUT,'parallel-x');
% BF=BF_compute_beamformed_data_par(RF,LUT);
% BF=BF_compute_beamformed_data_par(PAsig3,LUT);

% basic computation
% bfdata(ix,iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}).*LUT.apodization_coeff{ix,iy,iz});

toc

dexhot(abs(BF))
BF=permute(BF,[3 1 2]);
BFenv=abs(hilbert(BF));

%% plot image
% dexhot(BFenv(2:end,:,:),[1 1 1]);
mipIm3=mip(BFenv(2:end,:,:),3);
mipIm2=mip(BFenv(2:end,:,:),2);
mipIm1=mip(BFenv(2:end,:,:),1);

figure(10), subplot(131), imagesc(beamforming.y_pixel_mm,beamforming.z_pixel_mm,mipIm2)
xlabel('y (mm)'),ylabel('z (mm)'), axis image
subplot(132), imagesc(beamforming.x_pixel_mm,beamforming.z_pixel_mm,mipIm3)
xlabel('x (mm)'), axis image
subplot(133), imagesc(beamforming.x_pixel_mm,beamforming.y_pixel_mm,mipIm1)
xlabel('x (mm)'),ylabel('y (mm)'), axis image

figure(11), subplot(131), imagesc(beamforming.y_pixel_mm,beamforming.z_pixel_mm,squeeze(sum(BFenv(2:end,:,:),2)))
xlabel('y (mm)'),ylabel('z (mm)'), axis image
subplot(132), imagesc(beamforming.x_pixel_mm,beamforming.z_pixel_mm,squeeze(sum(BFenv(2:end,:,:),3)))
xlabel('x (mm)'), axis image
subplot(133), imagesc(beamforming.x_pixel_mm,beamforming.y_pixel_mm,squeeze(sum(BFenv(2:end,:,:),1)))
xlabel('x (mm)'),ylabel('y (mm)'), axis image

% x=beamforming.x_pixel_mm;
% z=beamforming.z_pixel_mm;
% imagesc(x,z,squeeze(abs(BF))');
% set(gca,'DataAspectRatio',[1 1 1]);
% shg
% 
% if sourceFlag
%     hold on
%     plot(sources.pos_mm.x,sources.pos_mm.z,'or')
%     hold off
% end





