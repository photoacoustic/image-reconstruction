function bfdata = BF_compute_beamformed_data(data,LUT,beamforming,varargin)
    % bfdata = BF_compute_beamformed_data(data,LUT)
    %
    % bfdata is RF-beamformed data (not enveloppe)
    % data.RF may be complex data (modulated...)
    %
    % No temporal interpolation, closest values are used.
    %
    % Emmanuel Bossy, 17/01/2020
    % 
    % Optional inputs:
    %
    %   varargin: 'singleloop' or 'multiloops'


    %% Checking optional input
    looptypes = {'singleloop','multiloops','parallel-x','parallel-x-single'};
    looptype = 'multiloops';
%     saveflag = false;

    if ~isempty(varargin)
        while ~isempty(varargin)
            switch lower(varargin{1})
                case 'looptype'
                    if any(strcmpi(varargin{2},looptypes))
                        looptype = lower(varargin{2});
                    else
                        error('Invalid looptype string.')
                    end
%                 case 'save'
%                     if isscalar(varargin{2})
%                         saveflag = varargin{2};
%                     else
%                         error('Invalid save flag value.')
%                     end
                otherwise
                    error(['Unknown optional parameter ' varargin{1}])
            end
            varargin(1:2) = [];
        end
    end
    
    
    %% Pre-allocating data
    nx = beamforming.sizes_px(1);
    ny = beamforming.sizes_px(2);
    nz = beamforming.sizes_px(3);
    ntot = nx*ny*nz;
    bfdata = zeros(nx,ny,nz);
    
    
    %% Looping
    switch looptype
        case 'multiloops'
            if contains(LUT.type,'chunked')
                progressbar('chunks')
                for c=1:beamforming.nChunks
                    load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
                    for ix=1:size(LUT.delays_pt,1)
                        kx = beamforming.chunks.idx{c};
                        for iy=1:ny
                            for iz=1:nz
                                bfdata(kx(ix),iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
                            end
                        end
                    end
                    progressbar(c/beamforming.nChunks)
                end
            elseif contains(LUT.type,'gpu2')
                data = gpuArray(data);
                d = gpuArray(nan(size(LUT.delays_pt),'single'));
                d(LUT.idx_active_points) = data(LUT.delays_pt(LUT.idx_active_points));
                bfdata = gather(squeeze(sum(d)));
            elseif contains(LUT.type,'gpuarray')
                data = gpuArray(data);
                d = gpuArray(nan(size(LUT.delays_pt),'single'));
                d(LUT.idx_active_points) = data(LUT.delays_pt(LUT.idx_active_points));
                bfdata = gather(squeeze(sum(d)));
            elseif contains(LUT.type,'matrix')
%                 for ix=1:nx
%                     for iy=1:ny
%                         for iz=1:nz
%                             pts = LUT.delays_pt(:,ix,iy,iz);
%                             pts(isnan(pts)) = [];
%                             bfdata(ix,iy,iz) = sum(data(pts));%.*LUT.apodization_coeff{ix,iy,iz});
%                         end
%                     end
%                 end
                d=nan(size(LUT.delays_pt));
                d(LUT.idx_active_points)=data(LUT.delays_pt(LUT.idx_active_points));
                bfdata=squeeze(sum(d));
            
            else
                for ix=1:nx
                    for iy=1:ny
                        for iz=1:nz
                            bfdata(ix,iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
                        end
                    end
                end
            end
            
        case 'singleloop'
            if contains(LUT.type,'chunked')
                for c=1:beamforming.nChunks
                    load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
                    if c==1, k=1; end
                    for i=1:length(beamforming.chunks.x{c})
                        bfdata(k-1+i) = sum(data(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
                    end
                    k = k+length(beamforming.chunks.x{c});
                    
%                     if saveflag, save(['LUT_chunk_' num2str(i) 'of' num2str(beamforming.nChunks) '.mat'],'-v6','LUT'); end  
                end
            
            else
                for i=1:ntot
                    bfdata(i) = sum(data(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
                end
            end
        
        case 'parallel-x'
            if contains(LUT.type,'chunked')
%                 for c=1:beamforming.nChunks
%                     loaded = load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
%                     LUT = loaded.LUT;
%                     kx = beamforming.chunks.idx{c};
%                     parfor ix=1:size(LUT.delays_pt,1)
%                         for iy=1:ny
%                             for iz=1:nz
%                                 bfdata(kx(ix),iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
%                             end
%                         end
%                     end
%                 end
            else
                parfor ix=1:nx
                    for iy=1:ny
                        for iz=1:nz
                            bfdata(ix,iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
                        end
                    end
                end
            end
            
            case 'parallel-x-single'
            if contains(LUT.type,'chunked')
%                 for c=1:beamforming.nChunks
%                     load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
%                     if c==1, k=1; end
%                     for i=1:length(beamforming.chunks.x{c})
%                         bfdata(k-1+i) = sum(data(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
%                     end
%                     k = k+length(beamforming.chunks.x{c});
%                     
% %                     if saveflag, save(['LUT_chunk_' num2str(i) 'of' num2str(beamforming.nChunks) '.mat'],'-v6','LUT'); end  
%                 end
%             
            else
                parfor i=1:ntot
                    bfdata(i) = sum(data(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
                end
            end
        
        otherwise
            error('Unknown loop type. Aborted.')
            
    end

    

end
