classdef PASources < handle
    % PASources handle class: contains all information related to
    %   Photo-Acoustic sources. Can be used to generate simulated
    %   Photo-Acoustic data

    properties
        N % Sources total number
        f0_MHz % Sources central frequency [MHz]
        BW % Sources fractional bandwidth
        pos_mm = struct('x',[],'y',[],'z',[]); % Sources 3D coordinates [mm] (should be row vectors)
    end
    
    methods (Access = public)
        
        function obj = PASources(f0_MHz,BW)
            % PASources constructor
            % 
            % Inputs:
            %   * f0_MHz: sources central frequency [MHz]
            %   * BW: sources fractional bandwidth
            
            obj.f0_MHz = f0_MHz;
            obj.BW = BW;
        end
        
        
        function SetSources(obj,x_mm,y_mm,z_mm)
            % Set sources 3D coordinates [mm]
            % 
            % Inputs:
            %   * x_mm: sources x coordinate [mm] (row vector)
            %   * y_mm: sources y coordinate [mm] (row vector)
            %   * z_mm: sources z coordinate [mm] (row vector)
            
            obj.CheckPosIntegrity(x_mm,y_mm,z_mm);
            
            obj.pos_mm.x = obj.CheckPosValidity(x_mm);
            obj.pos_mm.y = obj.CheckPosValidity(y_mm);
            obj.pos_mm.z = obj.CheckPosValidity(z_mm);
            
            obj.N = numel(obj.pos_mm.x);
        end
        
        
            
        function GenerateSourceGrid(obj,step_mm,FOVs_mm,centers_mm)
            % Generate a source grid based on step [mm], FOVs [mm] and centers [mm]
            % 
            % Inputs:
            %   * step_mm: grid x,y,z step [mm] (scalar)
            %   * FOVs_mm: grid x,y,z fields of view [mm] (1x3 vector)
            %   * centers_mm: grid x,y,z center coordinate [mm] (1x3 vector)
            
            x_mm = (-FOVs_mm(1)/2:step_mm:+FOVs_mm(1)/2) + centers_mm(1);
            y_mm = (-FOVs_mm(2)/2:step_mm:+FOVs_mm(2)/2) + centers_mm(2);
            z_mm = (-FOVs_mm(3)/2:step_mm:+FOVs_mm(3)/2) + centers_mm(3);
            
            [X,Y,Z] = meshgrid(x_mm,y_mm,z_mm);
            x = reshape(X,1,numel(X));
            y = reshape(Y,1,numel(Y));
            z = reshape(Z,1,numel(Z));
            
            obj.CheckPosIntegrity(x,y,z);
            obj.pos_mm.x = obj.CheckPosValidity(x); 
            obj.pos_mm.y = obj.CheckPosValidity(y);
            obj.pos_mm.z = obj.CheckPosValidity(z);

            obj.N = numel(obj.pos_mm.x);
        end
        
        
    end
    
    methods (Access = protected, Static = true)
        function out = CheckPosValidity(vector)
            % Ensure the input is a vector, returns the transposed vector
            % if not a row vector

            if ~isvector(vector)
                error('Please input a coordinate position vector.')
            else
                if ~isrow(vector)
                    warning('Changed position column vector into a row vector.')
                    out = transpose(vector);
                else
                    out = vector;
                end
            end
        end
        
        function CheckPosIntegrity(varargin)
            % Ensure inputs are numeric vectors with the same number of
            % elements
            
            numBool = cellfun(@isnumeric,varargin);
            vecBool = cellfun(@isvector,varargin);
            
            if ~prod(numBool) || ~prod(vecBool)
                error('Please enter numeric vector inputs.');
            else 
                % Compute input vector sizes
                vecNumel = cell2mat(cellfun(@numel,varargin,'UniformOutput',false));
                
                % Return error if the input vector have a different number of elements
                if length(unique(vecNumel))~=1
                    error('Input vectors should have the same number of elements');
                end
            end

        end
    end
end

