classdef Beamforming < handle
    % Beamforming handle class: contains all information related to the
    % beamforming properties needed for Photo-Acoustic image reconstruction
    
    properties
        receivers % PAReceivers instance
        mode = 'photoacoustic'; % Beamforming mode, can be either 'photoacoustic', or 'flat'
        sound_speed = 1.5; % Speed of sound [mm/us]
        fs_MHz = 500; % Photoacoustic signals sampling frequency [MHz]
        t0_pulse_pt = 0; % Center time of the pulse in the acquisition timebase. If >0, means that the pulse central time is AFTER the acquisiton trigger (t=0 by def) [samples]
        acq_delay_pt = 0; % Acquisition trigger time [samples]
        acq_duration_pt % Acquisition duration time [samples]
        N_receivers % Total number of receivers

        step_mm % Beamforming grid x,y,z step [mm] (scalar)
        FOVs_mm % Beamforming grid x,y,z fields of view [mm] (1x3 vector)
        centers_mm % Beamforming grid x,y,z center coordinate [mm] (1x3 vector)
        sizes_px % Beamforming grid sizes vector (1x3 vector)
        N_px % Total number of beamforming pixels
        
        % To be replaced as a struct pos_mm with x, y and z fields for homogenity
        x_pixel_mm % x pixels coordinates [mm]
        y_pixel_mm % y pixels coordinates [mm]
        z_pixel_mm % z pixels coordinates [mm]

        NA = 1; % Numerical aperture
        apodization = 0; % Use apodization if true
        
        LUTmaxChunkBytes = 100e6; % Maximum LUT size over which chunking will be offered
        chunks = struct('x',[],'idx',[]); % Structure containing points and indexes of each chunk into a cell array
    end
    
    properties (SetAccess = protected, GetAccess = public)
        isValidated = 0; % Valid object or not
        isChunked = []; % Chunked or not
        nChunks = []; % Number of chunks if LUT is chunked
        LUTmaxSize % Maximum size of the LUT, based on available memory
    end
    
    methods
        function obj = Beamforming(varargin)
            % Beamforming constructor
            % 
            % Optional input:
            %   * varargin{1}: should be a PASignals instance (manually
            %   assign properties otherwise)
            
            narginchk(0,1)
            
            if nargin
                if isa(varargin{1},'PASignals')
                    rf = varargin{1};
                    if strcmpi(rf.type,'simulated')
                        obj.receivers = rf.receivers;
                        obj.sound_speed = rf.sound_speed;
                        obj.mode = 'photoacoustic';
                        obj.fs_MHz = rf.fs_MHz;
                        obj.t0_pulse_pt = rf.t0_pulse*rf.fs_MHz;
                        obj.acq_delay_pt = rf.t0_acq*rf.fs_MHz;
                        obj.acq_duration_pt = rf.N_samples;
                        obj.N_receivers = rf.N_receivers;
                    else
                        obj.receivers = rf.receivers;
                        obj.sound_speed = rf.sound_speed;
                        obj.mode = 'photoacoustic';
                        obj.fs_MHz = rf.fs_MHz;
                        obj.t0_pulse_pt = rf.t0_pulse*rf.fs_MHz;
                        obj.acq_delay_pt = rf.t0_acq*rf.fs_MHz;
                        obj.acq_duration_pt = rf.N_samples;
                        obj.N_receivers = rf.N_receivers;
                        warning('Check beamforming object properties, or wrap them to fit into a PASignals object.')
                    end
                else
                    warning('Optional input argument should be a PASignals object. Assign properties manually.')
                end
            else
            end
        end
        
        function ComputeGrids(obj,step_mm,FOVs_mm,centers_mm)
            % Generate a beamforming grid based on step [mm], FOVs [mm] and centers [mm]
            % 
            % Inputs:
            %   * step_mm: grid x,y,z step [mm] (scalar)
            %   * FOVs_mm: grid x,y,z fields of view [mm] (1x3 vector)
            %   * centers_mm: grid x,y,z center coordinate [mm] (1x3 vector)
            
            obj.step_mm = step_mm;
            obj.centers_mm = centers_mm;
            obj.FOVs_mm = FOVs_mm;

            obj.x_pixel_mm = (-FOVs_mm(1)/2:step_mm:+FOVs_mm(1)/2) + centers_mm(1);
            obj.y_pixel_mm = (-FOVs_mm(2)/2:step_mm:+FOVs_mm(2)/2) + centers_mm(2);
            obj.z_pixel_mm = (-FOVs_mm(3)/2:step_mm:+FOVs_mm(3)/2) + centers_mm(3);
            
            obj.sizes_px = [length(obj.x_pixel_mm) length(obj.y_pixel_mm) length(obj.z_pixel_mm)];
            obj.N_px = prod(obj.sizes_px);
        end
        
        function Validate(obj)
            % Validates the current Beamforming object
            % Offering to chunk computations based on available memory
            
            % Get the maximum LUT size
            sz = obj.EstimateLUTmaxSize();
            
            % Get current memory and warn if it is more than max RAM
            mem = memory;
            if sz > 0.8*mem.MemAvailableAllArrays
                answer = input('Huge LUT, are you sure you wanna continue ? (y/n): ','s');
                if ~strcmpi(answer,'y')
                    disp('Aborted.')
                    obj.isValidated = 0;
                    return
                end
            end
            
            % Offer to chunk of the LUT size is gonna be over the specified
            % maxChunkBytes
            if obj.LUTmaxSize > obj.LUTmaxChunkBytes
                answer = input('LUT size > LUT max chunk bytes. Do you want to chunk ? (y/n): ','s');
                if ~strcmpi(answer,'y')
                    obj.isValidated = 1;
                    return
                else
                    obj.ComputeChunkPixels();
                    obj.isValidated = 1;
                end
            end
        end
        
        
        function LUTsz = EstimateLUTmaxSize(obj)
            % Estimates the maximum possible LUT size based on beamforming
            % grid size and on the number of receivers
            
            LUTsz = estimateMatrixSize([obj.sizes_px obj.N_receivers],'single');
            disp(['LUT size (singles) = ' num2str(LUTsz/1e6) ' Mo'])
            obj.LUTmaxSize = LUTsz;
        end
        
        
        function ComputeChunkPixels(obj)
            % Compute chunks pixels and indices
            
            obj.nChunks = ceil(obj.LUTmaxSize/obj.LUTmaxChunkBytes);
            [obj.chunks.x, obj.chunks.idx] = obj.ChunkVector(obj.x_pixel_mm);
           
            obj.isChunked = 1;
        end
        
        
        function [chunkcell,chunkidx] = ChunkVector(obj,vector)
            % Chunks an input vector into n parts
            %
            % Input:
            %   * vector: vector to be chunked
            %
            % Output:
            %   * chunkidx: Indexes of the chunks
            %   * chunkcell: values of the chunks
            
            % Restrict the function to the number of x pixels
            if obj.nChunks > numel(obj.x_pixel_mm)
                obj.nChunks = numel(obj.x_pixel_mm);
                chunkcell = cell(obj.nChunks,1);
                chunkidx = cell(obj.nChunks,1);
                
                % This case is simple, fill the cell array with the vector values, then return
                for i=1:obj.nChunks
                    chunkcell{i} = vector(i);
                    chunkidx{i} = i;
                end
                
                return
                
            end
            
            % Get the number of full size chunks and the remaining fragment
            vectorLength = numel(vector);
            chunkFullSize = ceil(vectorLength/obj.nChunks);
            chunkFragSize = rem(vectorLength,chunkFullSize);
            % Check how many chunkFullSize chunks can be extracted from the vector to split
            nFullSize = (vectorLength-chunkFragSize)/chunkFullSize;
                        
            if (nFullSize*chunkFullSize + chunkFragSize ~= vectorLength)
                error('Chunking dimensions problem. Aborting.')
            else
                % Adjust the number of chunks: the number of full size cuts + the remaining fragment
                % This can reduce the effective number of chunks, but is easier for integrity
                obj.nChunks = nFullSize+1;
            end

            % Initialize cell arrays
            chunkcell = cell(obj.nChunks,1);
            chunkidx = cell(obj.nChunks,1);
            
            for i=1:obj.nChunks
                if i<obj.nChunks
                    start = (i-1)*chunkFullSize+1;
                    stop = start+chunkFullSize-1;
                else
                    start = vectorLength - chunkFragSize + 1;
                    stop = vectorLength;
                end
                chunkcell{i} = vector(start:stop);
                chunkidx{i} = start:stop;
            end

        end
        
        function Show(obj)
            % Show details about the current Beamforming instance
            
            fprintf('\n\t Beamforming details:\n')
            fprintf('\t\t Reconstruction grid:\n')
            fprintf('\t\t\t x: FOV=%2.2fmm, min=%2.2fmm, max=%2.2fmm, step=%2.1fum (nx=%d)\n',obj.FOVs_mm(1), min(obj.x_pixel_mm), max(obj.x_pixel_mm), obj.step_mm*1e3, obj.sizes_px(1))
            fprintf('\t\t\t y: FOV=%2.2fmm, min=%2.2fmm, max=%2.2fmm, step=%2.1fum (ny=%d)\n',obj.FOVs_mm(2), min(obj.y_pixel_mm), max(obj.y_pixel_mm), obj.step_mm*1e3, obj.sizes_px(2))
            fprintf('\t\t\t z: FOV=%2.2fmm, min=%2.2fmm, max=%2.2fmm, step=%2.1fum (nz=%d)\n',obj.FOVs_mm(3), min(obj.z_pixel_mm), max(obj.z_pixel_mm), obj.step_mm*1e3, obj.sizes_px(3))
            fprintf('\t\t\t Total number of points = %d\n', obj.N_px)

            if isempty(obj.nChunks)
                str = 'no';
                val = 0;
            else
                str = 'yes';
                val = obj.nChunks;
            end
            fprintf('\t\t LUT computation:\n')
            fprintf('\t\t\t Chunked: %s, %d chunks\n\n',str,val)
        end
    end
    

    
end

