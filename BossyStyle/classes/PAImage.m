classdef PAImage < handle
    %RFDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        type = 'multiloops';
        data
    end
    
    properties (GetAccess = public, SetAccess = protected)
        sizes_px
        N_px
        types = {'multiloops','singleloop','parallel-x'};
    end
    
    methods
        function obj = PAImage(beamforming)
            obj.sizes_px = beamforming.sizes_px;
            obj.N_px = prod(obj.sizes_px);

            obj.data = zeros(obj.sizes_px,'single');
        end
        
        function Compute(obj,rfdata,LUT,beamforming,varargin)
            % Check optional argument
            if ~isempty(varargin)
                while ~isempty(varargin)
                    switch lower(varargin{1})
                        case 'type'
                            if any(strcmpi(varargin{2},obj.types))
                                obj.type = lower(varargin{2});
                            else
                                error('Invalid looptype string.')
                            end
                        otherwise
                            error(['Unknown optional parameter ' varargin{1}])
                    end
                    varargin(1:2) = [];
                end
            end
            
            
            switch obj.type
                case 'multiloops'
                    if contains(LUT.type,'cell')
                        if LUT.isChunked
                            upd_bar = textprogressbar2(beamforming.nChunks, 'barlength', 20, ...
                                 'updatestep', 1, ...
                                 'startmsg', 'Image chunk reconstruction:',...
                                 'endmsg', ' Done.', ...
                                 'showbar', false, ...
                                 'showremtime', true, ...
                                 'showactualnum', true, ...
                                 'barsymbol', '=', ...
                                 'emptybarsymbol', '-');
                            for c=1:beamforming.nChunks
                                load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
                                kx = beamforming.chunks.idx{c};
                                for ix=1:length(kx)
                                    for iy=1:obj.sizes_px(2)
                                        for iz=1:obj.sizes_px(3)
                                            obj.data(kx(ix),iy,iz) = sum(rfdata(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
                                        end
                                    end
                                end
                                upd_bar(c)
                            end
                        else
                            for ix=1:obj.sizes_px(1)
                                for iy=1:obj.sizes_px(2)
                                    for iz=1:obj.sizes_px(3)
                                        obj.data(ix,iy,iz) = sum(rfdata(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
                                    end
                                end
                            end
                        end

                    elseif contains(LUT.type,'gpu2')
                        obj.data = gpuArray(zeros(obj.sizes_px,'single'));
                        
                        if LUT.isChunked
                            upd_bar = textprogressbar2(beamforming.nChunks, 'barlength', 20, ...
                                 'updatestep', 1, ...
                                 'startmsg', 'Image chunk reconstruction:',...
                                 'endmsg', ' Done.', ...
                                 'showbar', false, ...
                                 'showremtime', true, ...
                                 'showactualnum', true, ...
                                 'barsymbol', '=', ...
                                 'emptybarsymbol', '-');
                            rfdata = gpuArray(rfdata);
                            
                            for c=1:beamforming.nChunks
                                load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
                                kx = beamforming.chunks.idx{c};
                                
                                d = gpuArray(nan(size(LUT.delays_pt),'single'));
                                d(LUT.idx_active_points) = rfdata(LUT.delays_pt(LUT.idx_active_points));
                                obj.data(kx,:,:) = squeeze(sum(d));
                                upd_bar(c)
                            end
                            obj.data = gather(obj.data);
                        else
                            rfdata = gpuArray(rfdata);
                            d = gpuArray(nan(size(LUT.delays_pt),'single'));
                            d(LUT.idx_active_points) = rfdata(LUT.delays_pt(LUT.idx_active_points));
                            obj.data = gather(squeeze(sum(d)));
                        end

                    elseif contains(LUT.type,'gpuarray')
                        rfdata = gpuArray(rfdata);
                        d = gpuArray(nan(size(LUT.delays_pt),'single'));
                        d(LUT.idx_active_points) = rfdata(LUT.delays_pt(LUT.idx_active_points));
                        obj.data = gather(squeeze(sum(d)));

                    elseif contains(LUT.type,'matrix')
                        for ix=1:obj.sizes_px(1)
                            for iy=1:obj.sizes_px(2)
                                for iz=1:obj.sizes_px(3)
                                    pts = LUT.delays_pt(:,ix,iy,iz);
                                    pts(isnan(pts)) = [];
                                    obj.data(ix,iy,iz) = sum(rfdata(pts));%.*LUT.apodization_coeff{ix,iy,iz});
                                end
                            end
                        end
%                         d=nan(size(LUT.delays_pt));
%                         d(LUT.idx_active_points)=rfdata(LUT.delays_pt(LUT.idx_active_points));
%                         obj.data=squeeze(sum(d));

                    else
                        
                    end

                case 'singleloop'
                    if contains(LUT.type,'chunked')
                        for c=1:beamforming.nChunks
                            load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
                            if c==1, k=1; end
                            for i=1:length(beamforming.chunks.x{c})
                                obj.data(k-1+i) = sum(rfdata(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
                            end
                            k = k+length(beamforming.chunks.x{c});

        %                     if saveflag, save(['LUT_chunk_' num2str(i) 'of' num2str(beamforming.nChunks) '.mat'],'-v6','LUT'); end  
                        end

                    else
                        for i=1:ntot
                            obj.img(i) = sum(rfdata(LUT.delays_pt{i}));%.*LUT.apodization_coeff{i});
                        end
                    end

                case 'parallel-x'
                    if contains(LUT.type,'chunked')
        %                 for c=1:beamforming.nChunks
        %                     loaded = load(['LUT_chunk_' num2str(c) 'of' num2str(beamforming.nChunks) '.mat'],'LUT');
        %                     LUT = loaded.LUT;
        %                     kx = beamforming.chunks.idx{c};
        %                     parfor ix=1:size(LUT.delays_pt,1)
        %                         for iy=1:ny
        %                             for iz=1:nz
        %                                 bfdata(kx(ix),iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
        %                             end
        %                         end
        %                     end
        %                 end
                    else
%                         parfor ix=1:nx
%                             for iy=1:ny
%                                 for iz=1:nz
%                                     obj.img(ix,iy,iz) = sum(data(LUT.delays_pt{ix,iy,iz}));%.*LUT.apodization_coeff{ix,iy,iz});
%                                 end
%                             end
%                         end
                    end



                otherwise
                    error('Unknown loop type. Aborted.')

            end
            
            
        end
        
        function Dexhot(obj)
            dexhot(abs(hilbert(obj.data)));
        end
        
        function ViewVolume(obj)
            volumeViewer(abs(hilbert(obj.data)));
        end
        
        function hFig = PlotProj(obj,beamforming,type,figNum)
            % Get the envelope of the reconstructed pressure
            BFenv = abs(hilbert(obj.data));
            
            % Compute projections according to type
            switch lower(type)
                case 'mip'
                    projYZ = mip(BFenv,1);
                    projXZ = mip(BFenv,2);
                    projXY = mip(BFenv,3);
                case 'sum'
                    projYZ = squeeze(sum(BFenv,1));
                    projXZ = squeeze(sum(BFenv,2));
                    projXY = squeeze(sum(BFenv,3));
                otherwise
                    error('Unknown projection type.')
            end
            
            % Plot projections and return figure handle
            hFig = figure(figNum);clf
                subplot(131), imagesc(beamforming.y_pixel_mm,beamforming.z_pixel_mm,projYZ.')
                    xlabel('y [mm]'),ylabel('z [mm]'), axis image
                subplot(132), imagesc(beamforming.x_pixel_mm,beamforming.z_pixel_mm,projXZ.')
                    xlabel('x [mm]'),ylabel('z [mm]'), axis image
                subplot(133), imagesc(beamforming.x_pixel_mm,beamforming.y_pixel_mm,projXY)
                    xlabel('x [mm]'),ylabel('y [mm]'), axis image
        end
        
        
    end

end

