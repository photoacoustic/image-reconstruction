classdef LUT < handle
    %RFDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        type = 'matrixv';
        saveflag = false;
        sizes
        idx_active_points
        delays_pt
        apodization_coeff
    end
    
    properties (GetAccess = public, SetAccess = protected)
        hasApodization
        isChunked
        nChunks
        types = {'cellBossy','cell4','cell5','matrix','matrixv','matrixvgpu','matrixvgpu2','matrixvgpuarrayfun'};
        funcHandles = {@compute_lut_cellBossy, @compute_lut_cell4, @compute_lut_cell5, @compute_lut_matrix, @compute_lut_matrixv, @compute_lut_matrix_gpuarray, @compute_lut_matrix_gpuarray2, @compute_lut_matrix_gpuarray_arrayfunc};
    end
    
    methods
        function obj = LUT(beamforming)
            if beamforming.apodization
                obj.hasApodization = true;
            else
                obj.hasApodization = false;
            end
            
            if beamforming.isChunked
                obj.isChunked = true;
                obj.nChunks = beamforming.nChunks;
            else
                obj.isChunked = false;
                obj.nChunks = 0;
            end
            
            obj.CheckGridsValidity(beamforming);
        end
        

        function Compute(obj,beamforming,opts)
            % Manage arguments
            arguments
                obj
                beamforming Beamforming
                opts.type char {mustBeMember(opts.type,{'cellBossy','cell4','cell5','matrix','matrixv','matrixvgpu','matrixvgpu2','matrixvgpuarrayfun'})} = 'matrixv'
                opts.save {mustBeNumericOrLogical} = false
            end
            
            obj.type = opts.type;
            obj.saveflag = opts.save;


            %%% Check if lut.type is allowed and return the function handle if valid
            tf = contains(obj.types,obj.type);
            fidx = find(tf==1);
            
            if isempty(fidx)
                error(['Unknown LUT type ' obj.type '. Aborting.'])
            else
                lutfunc = obj.funcHandles{fidx};
            end
            
            
            %%% Compute the lut, chunked or not
            if beamforming.isChunked
                % Delete the already existing file
                if obj.saveflag
                    if exist('LUT.mat','file'), delete 'LUT.mat', end
                end
                
                % Store the initial unchunked property
                store = beamforming.x_pixel_mm;

                % Loop on chunks
                upd_bar = textprogressbar2(beamforming.nChunks, 'barlength', 20, ...
                         'updatestep', 1, ...
                         'startmsg', 'LUT chunk computing:',...
                         'endmsg', ' Done.', ...
                         'showbar', false, ...
                         'showremtime', true, ...
                         'showactualnum', true, ...
                         'barsymbol', '=', ...
                         'emptybarsymbol', '-');
                
                for i=1:beamforming.nChunks
                    % Get the chunk values, compute the lut chunk, and save
                    beamforming.x_pixel_mm = beamforming.chunks.x{i};
                    LUT = lutfunc(beamforming);
                    if obj.saveflag, save(['LUT_chunk_' num2str(i) 'of' num2str(beamforming.nChunks) '.mat'],'-v6','LUT'); end
                    upd_bar(i)
                end
                beamforming.x_pixel_mm = store; % Restore the initial beamforming object
                LUT.type = [obj.type '_chunked'];
                obj.type = LUT.type;
                
            else
                LUT = lutfunc(beamforming);
                LUT.type = obj.type;
                obj.delays_pt = LUT.delays_pt;
                if isfield(LUT,'idx_active_points')
                    obj.idx_active_points = LUT.idx_active_points;
                end
                if obj.saveflag, save('LUT.mat','LUT'); end
            end    
            
        end
        
    end
    
    
    methods (Access = private)
        
    end
    
    
    methods (Access = protected, Static = true)
        function CheckGridsValidity(beamforming)
            receivers = beamforming.receivers;
            if ~isrow(beamforming.x_pixel_mm) || ~isrow(beamforming.y_pixel_mm) || ~isrow(beamforming.z_pixel_mm)
                error('Grid pixels should be line vectors')
            end
            if ~iscolumn(receivers.pos_mm.x) || ~iscolumn(receivers.pos_mm.y) || ~iscolumn(receivers.pos_mm.z)
                error('transducers positions should be column vectors')
            end
        end
    end
    
end

