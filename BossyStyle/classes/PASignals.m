classdef PASignals < handle
    % PASignals handle class: contains all information related to
    %   Photo-Acoustic signals.

    properties
        fs_MHz = 40; % Photoacoustic signals sampling frequency [MHz]
        t0_pulse = 0; % Center time of the pulse in the acquisition timebase. If >0, means that the pulse central time is AFTER the acquisiton trigger (t=0 by def) [us]
        t0_acq = 12; % Acquisition trigger time [us]
        acq_length = 14; % Acquisition length [us]
        acq_timebase_us % Acquisition timebase vector [us]
        sound_speed = 1.5; % Speed of sound [mm/us]

        data % Photo-Acoustic signals 2D matrix [N_samples x N_receivers]
        type % Photo-Acoustic signals type:   either 'simulated' or 'measured'
        N_samples % Total number of temporal samples
        N_receivers % Total number of receivers

        receivers % PAReceivers instance, containing Photo-Acoustic receivers related informations
        sources % PASources instance, containing Photo-Acoustic sources related informations (used only in simulation mode)
    end


    methods
        function obj = PASignals(t0_acq_us,acq_length_us,fs_MHz,optional)
            % PASignals constructor
            %
            % Inputs:
            %   * t0_acq_us: acquisition trigger time [us]
            %   * acq_length_us: acquisition length [us]
            %   * fs_MHz: sampling frequency [MHz]
            %   * optional: structure for options, used as a (string,value)
            %     input arguments couples. Options are the following:
            %       * 'loadExperiment': boolean (will call loadScanFPFile function)
            %       * 'experimentChannel': numeric, selects gage channel where PA signals are (either 1 or 2, default 2)
            %       * 'experimentPath': char, start path for uigetfile selection (default 'D:\Data\Jeremy')

            arguments
                t0_acq_us {mustBeNumeric} = 0
                acq_length_us {mustBeNumeric} = 10
                fs_MHz {mustBeNumeric} = 40

                optional.loadExperiment (1,1) {mustBeNumericOrLogical} = 0
                optional.experimentChannel (1,1) {mustBeNumeric} = 2
                optional.experimentPath char = 'D:\Data\Jeremy'
            end

            if ~optional.loadExperiment % Get ready for Generate method
                obj.t0_acq = t0_acq_us;
                obj.acq_length = acq_length_us;
                obj.fs_MHz = fs_MHz;
                obj.N_samples = round(obj.acq_length*obj.fs_MHz);
                obj.acq_timebase_us = (obj.t0_acq:1/obj.fs_MHz:obj.t0_acq+(obj.N_samples-1)/obj.fs_MHz)';

            else % Load user experiment
                [RFdata,params,~] = loadScanFPFile(optional.experimentPath); % from scan_fp
                data = RFdata.data.data;

                % Wrap experimental acquisition settings to class properties
                obj.fs_MHz = params.gage.acqInfo.SampleRate*1e-6;
                obj.t0_acq = params.gage.acqInfo.TriggerDelay/obj.fs_MHz;
                obj.acq_length = params.gage.acqInfo.SegmentSize/obj.fs_MHz;
                obj.N_samples = round(obj.acq_length*obj.fs_MHz);
                obj.acq_timebase_us = (obj.t0_acq:1/obj.fs_MHz:obj.t0_acq+(obj.N_samples-1)/obj.fs_MHz)';

                % Check PA signals data channel and add the corresponding data and settings
                if (optional.experimentChannel < 0 || optional.experimentChannel > 2 )
                    optional.experimentChannel = 2;
                end
                obj.AddGageData(data,optional.experimentChannel);
                obj.AddScanParams(params.scan);

                % Remove experimentally invalid points (where no peaks have been found in the calibration spectra)
                obj.data = obj.RemoveExperimentalInvalidPoints(obj.data,params.calib.validpoints);
                obj.data = single(reshape(obj.data,[obj.N_samples obj.N_receivers]));
            end
        end

        function Generate(obj,sources,receivers)
            % Generate Photo-Acoustic signals based on a PASignals and a
            % PAReceivers objects
            %
            % Inputs:
            %   * sources: PASignals instance
            %   * receivers: PAReceivers instance

            arguments
                obj
                sources PASources
                receivers PAReceivers
            end
            
            upd_bar = textprogressbar2(receivers.N*sources.N, ...
                                 'updatestep', 1, ...
                                 'startmsg', 'PA signals generation:',...
                                 'endmsg', ' Done.', ...
                                 'showbar', false, ...
                                 'showremtime', true, ...
                                 'showactualnum', true);

            obj.data = zeros(length(obj.acq_timebase_us),receivers.N);
            for k=1:receivers.N
                for n=1:sources.N
                    retard = 1/obj.sound_speed.*sqrt((receivers.pos_mm.x(k)-sources.pos_mm.x(n)).^2+(receivers.pos_mm.y(k)-sources.pos_mm.y(n)).^2+(receivers.pos_mm.z(k)-sources.pos_mm.z(n)).^2);
                    [yi,yq] = gauspuls((obj.acq_timebase_us-obj.t0_pulse-retard),sources.f0_MHz,sources.BW);
                    obj.data(:,k) = obj.data(:,k)+yi+1i*yq;
                    
                    upd_bar((k-1)*n+n)
                end
            end

            % Store the PASources and the PAReceivers objects into the sources and receivers properties
            obj.sources = sources;
            obj.receivers = receivers;
            obj.N_receivers = receivers.N;
            obj.type = 'simulated';
        end

        function AddGageData(obj,data,ch)
            % Loads experimentally measured PA signals usinge the Gage DAQ to the data property of the class
            %
            % Inputs:
            %   * data: 4D or 5D matrix
            %   * ch: DAQ channel containing the PA signals
            %   * varargin: not used yet

            arguments
                obj
                data {mustBeNumeric}
                ch (1,1) double = 2
            end


            if ndims(data) == 4
                obj.data = squeeze(data(:,ch,:,:));
            elseif ndims(data) == 5 % Average over repetitions
                obj.data = squeeze(mean(data(:,:,ch,:,:),2));
            end

            obj.type = 'measured';
        end

        function AddScanParams(obj,scan_struct)
            % Fills the receivers and N_receivers properties of the class based on experimental data
            %
            % Inputs:
            %   * scan: scan structure

            arguments
                obj
                scan_struct struct
            end

            r = PAReceivers();
            r.LoadReceiverGrid(scan_struct);

            obj.receivers = r;
            obj.N_receivers = r.N;
        end

        function CropSignals(obj,us_range)
            % Crops the PA signals in time domain within specified range
            %
            % Input:
            %   * us_range: cropping time range [us] (1x2 vector)

            arguments
                obj
                us_range (1,2) {mustBeNumeric}
            end

            % Find timebase within us range
            [t_ind,~] = find((obj.acq_timebase_us >= us_range(1)) & (obj.acq_timebase_us <= us_range(2)));

            % Update corresponding properties
            obj.t0_acq = us_range(1);
            obj.acq_length = abs(diff(us_range));
            obj.N_samples = round(obj.acq_length*obj.fs_MHz);
            obj.acq_timebase_us = (obj.t0_acq:1/obj.fs_MHz:obj.t0_acq+(obj.N_samples-1)/obj.fs_MHz)';

            % Update data
            obj.data = obj.data(t_ind,:);
        end



        function hfig = PlotRF(obj,fignum,opts)
            % Plots the Photo-Acoustic RF signal (imagesc)
            %
            % Input:
            %   * fignum: figure number
            %   * varargin: either 'image' or 'plot'
            %
            % Output:
            %   * hfig: figure handle

            arguments
                obj
                fignum (1,1) {mustBeNumeric}
                opts.type char {mustBeMember(opts.type,{'image','plot'})} = 'image'
            end

            hfig = figure(fignum); clf
            if strcmp(opts.type,'image')
                imagesc((1:obj.receivers.N),obj.acq_timebase_us,real(obj.data))
                xlabel('Receiver #'),ylabel('Time [us]')
                colormap(gray)
            else
                plot(obj.acq_timebase_us,real(obj.data))
                xlabel('Time [us]'),ylabel('Voltage [V]')
            end
            title('Real part of the acquired signals')

        end

        function hfig = PlotSourcesAndReceivers(obj,fignum)
            % Plots the positions of the source and the position of the receivers (plot3)
            %
            % Input:
            %   * fignum: figure number
            %
            % Output:
            %   * hfig: figure handle

            arguments
                obj
                fignum (1,1) {mustBeNumeric}
            end

            hfig = figure(fignum); clf,hold on
            if ~isempty(obj.receivers)
                plot3(obj.receivers.pos_mm.x,obj.receivers.pos_mm.y,obj.receivers.pos_mm.z,'Linestyle','none','Marker','.','MArkersize',25);
            end
            if ~isempty(obj.sources)
                plot3(obj.sources.pos_mm.x,obj.sources.pos_mm.y,obj.sources.pos_mm.z,'Linestyle','none','Marker','.','Markersize',25)
            end
            grid on, box on

            xlabel('x [mm]'),ylabel('y [mm]'),zlabel('z [mm]')
            title('Positions of the sources (red) and receivers (blue)')
        end

    end




    methods (Static)

        function data_out = RemoveExperimentalInvalidPoints(data_in,validity_matrix)
            % Sets the pixels in the ND data_in to zero based on the 2D validity matrix
            %
            % The sizes of the two dimensions of validity_matrix must be equal
            % to the sizes of the two first dimensions of data_in.
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * validity_matrix: 2D boolean matrix
            %
            % Output:
            %     * data_out: Filtered matrix (ND)

            arguments
                data_in {mustBeNumeric}
                validity_matrix {mustBeNumericOrLogical}
            end

            if ~isempty(validity_matrix)
                data_out = data_in.*reshape(validity_matrix,[1 size(validity_matrix)]);
            else
                data_out = data_in;
            end
        end

        function data_out = SubstractMean(data_in,opts)
            % Sets the pixels in the ND data_in to zero based on the 2D validity matrix
            %
            % The sizes of the two dimensions of validity_matrix must be equal
            % to the sizes of the two first dimensions of data_in

            arguments
                data_in (:,:) {mustBeNumeric} % Data matrix from which mean should be substracted
                opts.dim (1,1) {mustBeNumeric} = 1
            end

            % Check opts.dim validity
            if ~any(opts.dim,[1 2])
                opts.dim = 1; % Reassign default value
            end

            data_out = data_in - mean(data_in,opts.dim);
        end

        function data_out = BandpassFilter(data_in,cutFreqs,samplingFreq,order)
            % Zero phase-delay bandpass filtering of data.
            % The first dimension is filtered
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * cutFreqs: 2-elements vector, containing the cutoff frequencies
            %     * samplingFreq: sampling frequency
            %     * order: filter order
            %
            % Output:
            %     * data_out: Filtered matrix (ND)

            arguments
                data_in {mustBeNumeric}
                cutFreqs (1,2) {mustBeNumeric} = [1 250]*1e6
                samplingFreq (1,1) {mustBeNumeric} = 500e6
                order (1,1) {mustBeNumeric} = 1
            end

            [b,a] = butter(order,cutFreqs/(samplingFreq/2));
            data_out = single(filtfilt(b,a,double(data_in)));
        end

        function data_out = SmoothMovmean(data_in,nsmooth)
            % Smooths the data with the movmean function
            % The first dimension is smoothed
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * nsmooth: number of points used for moving smoothing window
            %
            % Output:
            %     * data_out: Filtered matrix (ND)

            arguments
                data_in {mustBeNumeric}
                nsmooth (1,1) {mustBeNumeric} = 500
            end

            data_out = data_in - movmean(data_in,nsmooth);
        end


        function data_out = RandomizePixelAmplitude(data_in,opts)
            % Returns data matrix multiplied by random factors between 0 and 1
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * ('dim',2): optional, set the dimension on which to apply random factors
            %
            % Output:
            %     * data_out: output matrix (ND)

            arguments
                data_in (:,:) {mustBeNumeric} % Data matrix from which amplitudes should be randomized
                opts.dim (1,1) {mustBeNumeric} = 2
            end

            % Check opts.dim validity
            if ~any(opts.dim,[1 2])
                opts.dim = 2; % Reassign default value
            end

            data_out = data_in.*rand(1,size(data_in,2));
        end

        function data_out = AddGaussianNoise(data_in,sigma)
            % Returns the data matrix with an added gaussian noise
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * sigma: noise std
            %
            % Output:
            %     * data_out: output matrix (ND)

            arguments
                data_in (:,:) {mustBeNumeric} % Data matrix on which to add noise
                sigma (1,1) {mustBeNumeric} = 0.1
            end

            data_out = data_in + sigma*randn(size(data_in));

        end

        function data_out = AddRandomOnOff(data_in,opts)
            % Returns data matrix multiplied by random binary matrix
            %
            % Inputs:
            %     * data_in: input matrix (ND)
            %     * ('dim',2): optional, set the dimension on which to apply random binary coefficients
            %
            % Output:
            %     * data_out: output matrix (ND)

            arguments
                data_in (:,:) {mustBeNumeric} % Data matrix from which amplitudes should be randomized
                opts.dim (1,1) {mustBeNumeric} = 2
            end

            % Check opts.dim validity
            if ~any(opts.dim,[1 2])
                opts.dim = 2; % Reassign default value
            end

            offpoints = randi([0 1],1,size(data_in,2));
            data_out = data_in.*offpoints;

        end

    end
end
