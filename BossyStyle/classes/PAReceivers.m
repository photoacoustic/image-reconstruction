classdef PAReceivers < handle
    % PAReceivers handle class: contains all information related to
    %   Photo-Acoustic receivers.
    
    properties
        N % Total number of receivers
        pos_mm = struct('x',[],'y',[],'z',[]); % Sources 3D coordinates [mm] (should be column vectors)
    end
    
    methods (Access = public)
        
        function obj = PAReceivers()
            % PAReceivers constructor, just creating an empty instance
        end

        function GenerateReceiverGrid(obj,step_mm,FOVs_mm,centers_mm)
            % Generate a receiver grid based on step [mm], FOVs [mm] and centers [mm]
            % 
            % Inputs:
            %   * step_mm: grid x,y,z step [mm] (scalar)
            %   * FOVs_mm: grid x,y,z fields of view [mm] (1x3 vector)
            %   * centers_mm: grid x,y,z center coordinate [mm] (1x3 vector)
            
            x_mm = (-FOVs_mm(1)/2:step_mm:+FOVs_mm(1)/2) + centers_mm(1);
            y_mm = (-FOVs_mm(2)/2:step_mm:+FOVs_mm(2)/2) + centers_mm(2);
            z_mm = (-FOVs_mm(3)/2:step_mm:+FOVs_mm(3)/2) + centers_mm(3);
            
            [X,Y,Z] = meshgrid(x_mm,y_mm,z_mm);
            x = reshape(X,numel(X),1);
            y = reshape(Y,numel(Y),1);
            z = reshape(Z,numel(Z),1);
            
            obj.CheckPosIntegrity(x,y,z);
            obj.pos_mm.x = obj.CheckPosValidity(x); 
            obj.pos_mm.y = obj.CheckPosValidity(y);
            obj.pos_mm.z = obj.CheckPosValidity(z);

            obj.N = length(obj.pos_mm.x);
        end
        
        function LoadReceiverGrid(obj,scan)
            % Loads a receiver grid based on scan structure
            % 
            % Inputs:
            %   * scan: structure with x,y and z fields, containing
            %   receiver coordinates [m]
            
            x_mm = scan.x*1e3;
            y_mm = scan.y*1e3;
            if isempty(scan.z)
                z_mm = 0;
            else
                z_mm = scan.z*1e3;
            end
            
            [X,Y,Z] = meshgrid(x_mm,y_mm,z_mm);
            x = reshape(X,numel(X),1);
            y = reshape(Y,numel(Y),1);
            z = reshape(Z,numel(Z),1);
            
            obj.CheckPosIntegrity(x,y,z);
            obj.pos_mm.x = obj.CheckPosValidity(x); 
            obj.pos_mm.y = obj.CheckPosValidity(y);
            obj.pos_mm.z = obj.CheckPosValidity(z);

            obj.N = length(obj.pos_mm.x);
        end
        
        
    end
    
    
    methods (Access = protected, Static = true)
        function out = CheckPosValidity(vector)
            % Ensure the input is a vector, returns the transposed vector
            % if not a column vector
            
            if ~isvector(vector)
                error('Please input a coordinate position vector.')
            else
                if ~iscolumn(vector)
                    warning('Changed position column vector into a row vector.')
                    out = transpose(vector);
                else
                    out = vector;
                end
            end
        end
        
        function CheckPosIntegrity(varargin)
            % Ensure inputs are numeric vectors with the same number of
            % elements
            
            numBool = cellfun(@isnumeric,varargin);
            vecBool = cellfun(@isvector,varargin);
            
            if ~prod(numBool) || ~prod(vecBool)
                error('Please enter numeric vector inputs.');
            else 
                % Compute input vector sizes
                vecNumel = cell2mat(cellfun(@numel,varargin,'UniformOutput',false));
                
                % Return error if the input vector have a different number of elements
                if length(unique(vecNumel))~=1
                    error('Input vectors should have the same number of elements');
                end
            end

        end
    end
    
    
    
end

