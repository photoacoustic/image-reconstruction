% clearvars -except username folder LUT

beamforming = Beamforming(rf);
beamforming.ComputeGrids(0.04,[2 2 1],[0 0.2 2]) % step_mm, FOVs_mm [x y z], centers_mm [x y z]
beamforming.Validate();


%%
lut = LUT(beamforming);
tic
lut.Compute(beamforming,'type','matrix','save',false);
toc

%%
tic
BF = BF_compute_beamformed_data(rf.data,lut,beamforming,'looptype','multiloops');
toc

dexhot(abs(BF))