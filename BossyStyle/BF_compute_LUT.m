function LUT=BF_compute_LUT(beamforming,varargin)

    % LUT=BF_computeLUT(beamforming)
    %
    % LUT has cell fields LUT.delays_pt and LUT.apodization_coeff
    %
    % beamforming has fields:
    %
    % beamforming.mode  % 'photoacoustic', 'flat'
    % beamforming.apodization % 'hamming', 'none'
    % beamforming.c
    % beamforming.sampling_freq_MHz
    % beamforming.t0_pulse_pt
    % beamforming.acquisition_delay_pt
    % beamforming.acquisition_duration_pt
    % beamforming.F_nb
    % beamforming.receivers  % position as column vectors...Historical
    % beamforming.x_pixel_mm % position as line vectors.... Historical...
    % beamforming.y_pixel_mm
    % beamforming.z_pixel_mm


    % je corrige l'endroit ou sont arrondi les d�lais...
    % j'ajoute une 1/2 largeur d'�l�ment pour positionnnement correct
    % x = 0 mm correspond au centre de la barrette. J'ai recentr�e la
    % focalisation dynamique au passage pour que �a marche
    %
    % Emmanuel Bossy, 17/01/2020
    
    
    %% Checking optional inputs
    outputTypes = {'cell','cell2','cell3','cell4','cell5','h5file','matrix'};
    outputType = 'cell';
    saveflag = false;

    if ~isempty(varargin)
        while ~isempty(varargin)
            switch lower(varargin{1})
                case 'type'
                    if any(strcmpi(varargin{2},outputTypes))
                       outputType = lower(varargin{2});
                    else
                        error('Invalid type string.')
                    end
                case 'save'
                    if isscalar(varargin{2})
                        saveflag = varargin{2};
                    else
                        error('Invalid save flag value.')
                    end
                otherwise
                    error(['Unknown optional parameter ' varargin{1}])
            end
            varargin(1:2) = [];
        end
    end
    

    %% Checking grid formats
    receivers = beamforming.receivers;
    if ~isrow(beamforming.x_pixel_mm) || ~isrow(beamforming.y_pixel_mm) || ~isrow(beamforming.z_pixel_mm)
        error('Grid pixels should be line vectors')
    end
    if ~iscolumn(receivers.pos_mm.x) || ~iscolumn(receivers.pos_mm.y) || ~iscolumn(receivers.pos_mm.z)
        error('transducers positions should be column vectors')
    end

    
    %% Preallocating LUT
    
    
    switch outputType
        case 'cell'
            LUT = compute_lut_cell(beamforming,receivers);
            LUT.type = 'cell';
            if saveflag, save('LUT.mat','LUT'); end
        
        case 'cell2'
            if beamforming.isChunked
                progressbar('chunks')
                for i=1:beamforming.nChunks
                    LUT = compute_lut_cell2_chunked(beamforming,receivers,i);
                    progressbar(i/beamforming.nChunks)
                end
                LUT.type = 'cell2_chunked';
                if saveflag, save('LUT.mat','LUT'); end  
            else
                LUT = compute_lut_cell2(beamforming,receivers);
                LUT.type = 'cell2';
                if saveflag, save('LUT.mat','LUT'); end  
            end
            
        case 'cell3'
            LUT = compute_lut_cell3(beamforming,receivers);
            LUT.type = 'cell3';
            if saveflag, save('LUT.mat','LUT'); end
            
        case 'cell4'
            LUT = compute_lut_cell4(beamforming,receivers);
            LUT.type = 'cell4';
            if saveflag, save('LUT.mat','LUT'); end
            
        case 'cell5'
            if beamforming.isChunked
                progressbar('chunks')
                for i=1:beamforming.nChunks
                    LUT = compute_lut_cell5_chunked(beamforming,i);
                    progressbar(i/beamforming.nChunks)
                    
                    if saveflag, save(['LUT_chunk_' num2str(i) 'of' num2str(beamforming.nChunks) '.mat'],'-v6','LUT'); end  
                end
                LUT.type = 'cell5_chunked';
            else
                LUT = compute_lut_cell5(beamforming);
                LUT.type = 'cell5';
                if saveflag, save('LUT.mat','LUT'); end  
            end
            
        case 'h5file'
            LUT = compute_lut_h5file(beamforming,receivers);
            LUT.type = 'h5file';
            
        case 'matrix'
            [LUT, delays_pt, idx_active_points] = compute_lut_matrix(beamforming,receivers);
            LUT.type = 'matrix';
            if saveflag, save('LUT.mat','LUT'); end
            
        otherwise
            error('Unknown LUT output type. Aborting.')
    end

end

